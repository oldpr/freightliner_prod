<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Llamado a consumidores | Freightliner Colombia';
$metaDescripcion = 'LLAMADO A CONSUMIDORES: CAMPAÑA SEGURIDAD CASCADIA PRODUCIDOS ENTRE 2007 Y 2018';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/Llamado_a_consumidores_campana_seguridad_cascadia.php';
$ogTitle = 'Freightliner en la Feria Expoconstrucción | Freightliner Colombia';
$ogDescription = 'Freightliner hizo presencia en la décimo cuarta edición de la Feria de Expoconstrucción y Expodiseño 2017';
$ogImage = 'http://freightliner.com.co/tags/expoconstruccion.jpg';
$twitterTitle = 'Freightliner en la Feria Expoconstrucción | Freightliner Colombia';
$twitterDescription = 'Freightliner hizo presencia en la décimo cuarta edición de la Feria de Expoconstrucción y Expodiseño 2017';
$twitterImage = 'http://freightliner.com.co/tags/expoconstruccion.jpg';


/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '11.05.2018';
$imagen = 'img/assets/detalle-noticias/6Noticia-photo.jpg';

$titulo = 'LLAMADO A CONSUMIDORES: CAMPAÑA SEGURIDAD CASCADIA PRODUCIDOS ENTRE 2007 Y 2018';

// --Contenido dividido por parrafos
$contenido[0] = 'Daimler Colombia  S. A informa  que, para determinados  vehículos FREIGHTLINER CASCADIA producidos entre 2007 y 2018, la fábrica ha lanzado una campaña de seguridad. Esta refiere a que, después de varias aplicaciones de freno duro, el interruptor de presión de la luz de freno puede no activarse con una aplicación ligera del pedal de freno. Tales luces no se iluminan cuando los frenos de servicio se están aplicando, por lo cual no indicaría a otros automovilistas la intención del conductor de reducir la velocidad.
';
$contenido[1] = 'Por lo anterior, solicitamos a todos los interesados consultar con el número VIN en las siguientes páginas web: www.freightliner.com.co; www.daimler.com.co, si esta campaña debe ser aplicada, para lo cual deberán comunicarse con nuestra red de concesionarios autorizados, identificar el concesionario más cercano a su ubicación y programar la cita. 
';
$contenido[2] = '<a href="https://www.freightliner.com.co/pdf/llamado_a_consumidores-cascadia.pdf" target="_blank">Listado de número de chasis</a>
';
$contenido[3] = 'Si lo desea, también puede ingresar su número VIN o FIN haciendo clic en <a href="http://www.freightliner.com.co/recall/" target="_blank">este enlace</a> y comprobar si su vehículo se encuentra dentro del listado de este llamado a seguridad. 
';
$contenido[4] = 'Encuentre <a href="https://www.freightliner.com.co/concesionarios.php" target="_blank">aquí</a> nuestra red de concesionarios autorizados para programar la cita en caso de que su vehículo se encuentre dentro del listado de este llamado a seguridad.
';
/*$contenido[14] = '•	Fabricado con acero Hardox 450 de alta resistencia a la abrasión y al impacto.<br>
•	Ideal para grandes y exigentes operaciones de carga de materiales duros y abrasivos.<br>
•	Resistente al desgaste y a los ataques químicos severos, no absorbe la humedad. Ideal para transportistas que manejan carga de materiales con temperaturas superiores a los 120 grados, situación que no afecta el acero de la carrocería.<br>
•	Especial para aplicaciones en Minería Pesada, transporte de materiales duros y abrasivos, procesos de carga del volco a gran altura (impactos). <br>
•	La escalera con su barandilla facilita la subida para la inspección de la carga en el volco. Los escalones tienen un diseño Antideslizante.<br>
•	Volco tipo góndola con piso plano para evitar que se pegue el material, brindando un desprendimiento y deslizamiento de la carga más práctico para procesos de descarga más seguros y rápidos.';*/


/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));