<?php
require_once './app.php';
$classBody = 'producto';

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = '';
$metaKeywords = '';
$ogUrl = '';
$ogTitle = '';
$ogDescription = '';
$ogImage = '';
$twitterTitle = '';
$twitterDescription = '';
$twitterImage = '';

$title = '';

/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = '';
$prodcutoNombre = '';
$productoPrecio = '';

/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = '';
$descTitulo = '';
$descTexto = '';


/**** SECCION ESPECIFICACIONES ****/

// --ITEM 1
$imgItem1 = '';
$tituloItem1 = '';
$subtituloItem1 = '';

// --ITEM 2
$imgItem2 = '';
$tituloItem2 = '';
$subtituloItem2 = '';

// --ITEM 3
$imgItem3 = '';
$tituloItem3 = '';
$subtituloItem3 = '';

// --ITEM 4
$imgItem4 = '';
$tituloItem4 = '';
$subtituloItem4 = '';

// --LINKS
$linkFichaTecnica = '';


/**** SECCIÓN IMÁGENES ****/
$linkImg1 = '';
$linkImg2 = '';
$linkImg3 = '';


/**** SECCIÓN DETALLES ****/

// --DETALLE 1
$imgDetalle1 = '';
$tituloDetalle1 = '';
$textoDetalle1 = '';

// --DETALLE 2
$imgDetalle2 = '';
$tituloDetalle2 = '';
$textoDetalle2 = '';

// --DETALLE 3
$imgDetalle3 = '';
$tituloDetalle3 = '';
$textoDetalle3 = '';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
    ));