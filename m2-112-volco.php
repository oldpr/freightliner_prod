<?php
require_once './app.php';
$classBody = 'producto';
/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = 'El volco diseñado para el trabajo extremo y las condiciones difíciles ofrece a sus operadores máxima resistencia y gran capacidad técnica de carga.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/m2-112-volco.php';
$ogTitle = 'M2 112 Volco | Camiones Freightliner Colombia';
$ogDescription = 'El volco diseñado para el trabajo extremo y las condiciones difíciles ofrece a sus operadores máxima resistencia y gran capacidad técnica de carga.';
$ogImage = 'http://freightliner.com.co/tags/m2-112-volco.jpg';
$twitterTitle = 'M2 112 Volco | Camiones Freightliner Colombia';
$twitterDescription = 'El volco diseñado para el trabajo extremo y las condiciones difíciles ofrece a sus operadores máxima resistencia y gran capacidad técnica de carga.';
$twitterImage = 'http://freightliner.com.co/tags/m2-112-volco.jpg';


$title = 'M2 112 Volco | Camiones Freightliner Colombia';
/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = 'img/assets/productos/m2-112-volco/1-Photo-Billboard-M2112.jpg';
$prodcutoNombre = 'M2 112 VOLCO';
$productoPrecio = '306.800.000';
/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = 'img/assets/productos/m2-112-volco/Logo-m2-112.jpg';
$descTitulo = 'La  volqueta diseñada para el trabajo extremo';
$descTexto = 'Si lo que se requiere es máxima resistencia y capacidad técnica de carga, en condiciones de operación con altas pendientes y sobre esfuerzos, Freightliner ofrece el M2 112 volco, un vehículo con plataforma reforzada, diseñado para trabajos extremos.';
/**** SECCION ESPECIFICACIONES ****/
// --ITEM 1
$imgItem1 = 'img/icon-seguridad.jpg';
$tituloItem1 = 'ABS / ATC';
$subtituloItem1 = 'Seguridad';
// --ITEM 2
$imgItem2 = 'img/icon-calidad.png';
$tituloItem2 = 'CABINA LIVIANA, LIBRE DE CORROSIÓN';
$subtituloItem2 = 'Calidad';
// --ITEM 3
$imgItem3 = 'img/icon-operaciones.png';
$tituloItem3 = ' MOTOR 350HP Y 1.350 LB/PIE';
$subtituloItem3 = 'Operaciones exigentes';
// --ITEM 4
$imgItem4 = 'img/icon-productividad.png';
$tituloItem4 = 'SUSPENSIÓN REFORZADA = MAYOR CAPACIDAD DE CARGA';
$subtituloItem4 = 'Productividad';
// --LINKS
$linkFichaTecnica = 'pdf/M2112-volco.pdf';
/**** SECCIÓN IMÁGENES ****/
$linkImg1 = 'img/assets/productos/m2-112-volco/M2-112-photo-1.jpg';
$linkImg2 = 'img/assets/productos/m2-112-volco/M2-112-photo-2.jpg';
$linkImg3 = 'img/assets/productos/m2-112-volco/M2-112-photo-3.jpg';
/**** SECCIÓN DETALLES ****/
// --DETALLE 1
$imgDetalle1 = 'img/assets/productos/m2-112-volco/M2-112-Trenmotriz-Photo.jpg';
$tituloDetalle1 = 'tren motriz';
$textoDetalle1 = 'Cuenta con un poderoso motor Detroit MBE 4000EPA 98, que le da a esta volqueta la capacidad de rendir en condiciones extremas de carga y terreno. Viene equipado con una caja de cambios Eaton RTO14908LL de 10 marchas, de alta relación y una última marcha overdrive, dándole al M2 112 volco, la versatilidad para operar en un amplio rango de terrenos y condiciones de carga sin dificultades. Sus ejes Meritor con gran capacidad de carga, además de su suspensión reforzada y sus robustos herrajes le permiten soportar las exigentes condiciones de carga. Sus ejes cuentan también con bloqueos de diferencial transversales y longitudinal, lo que le da al operador la posibilidad de contar con la máxima tracción disponible para tener la capacidad de maniobrar en terrenos difíciles, reduciendo el riesgo de quedar bloqueado en caminos llenos de lodo. Para aumentar la eficiencia en la operación, esta volqueta cuenta con sistemas electrónicos ABS (anti-bloqueo de frenos) y ATC (Control de tracción) que le brindan al operador las mejores condiciones de respuesta posibles del camión en fases de detención o de fuerza en la operación.';
// --DETALLE 2
$imgDetalle2 = 'img/assets/productos/m2-112-volco/M2-112-Productividad-photo.jpg';
$tituloDetalle2 = 'Máxima productividad del Conductor';
$textoDetalle2 = 'El M2 112 volco se ha creado para lograr que las operaciones de carga y descarga de materiales en zonas de construcción y canteras sean lo más agradables posibles. Cuenta con una cabina de suspensión neumática, tablero y controles ergonómicos, una silla del conductor con suspensión neumática y su aire acondicionado de fábrica, todo lo que se necesita para reducir el agotamiento y aumentar la productividad y satisfacción del operador.';
// --DETALLE 3
$imgDetalle3 = 'img/assets/productos/m2-112-volco/M2-112-desempeno-photo.jpg';
$tituloDetalle3 = 'Máximo desempeño del Camión';
$textoDetalle3 = 'El M2 112 volco cuenta con dos opciones de volco de 14 metros cúbicos, uno fabricado en acero H50, de mayor resistencia que el tradicional acero A36; y otro construido con Acero antidesgaste Hardox 450, de alto nivel de resistencia, para trabajos extremos con material muy duro o abrasivo y en condiciones de carga a altura (con altos niveles de impacto). Su diseño tipo góndola con piso liso, facilita el deslizamiento del material en procesos de descarga.<br>
Cuenta con un sistema hidráulico de tres vías, que aumenta la vida útil del aceite y reduce la necesidad de mantenimiento del mismo. En síntesis, el M2 112 volco ha sido creado para lograr máximo desempeño y rentabilidad en obras de infraestructura y explotación de agregados del país.';
/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));
