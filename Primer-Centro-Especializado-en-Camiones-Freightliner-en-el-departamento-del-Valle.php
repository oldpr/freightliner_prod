<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Primer centro especializado Freightliner en el Valle del Cauca | Freightliner Colombia';
$metaDescripcion = 'Así funciona el primer centro especializado en camiones Freightliner en el departamento del Valle del Cauca';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/Primer-Centro-Especializado-en-Camiones-Freightliner-en-el-departamento-del-Valle.php';
$ogTitle = 'Primer centro especializado Freightliner en el Valle del Cauca | Freightliner Colombia';
$ogDescription = 'Así funciona el primer centro especializado en camiones Freightliner en el departamento del Valle del Cauca';
$ogImage = 'http://freightliner.com.co/tags/centro-valle.jpg';
$twitterTitle = 'Primer centro especializado Freightliner en el Valle del Cauca | Freightliner Colombia';
$twitterDescription = 'Así funciona el primer centro especializado en camiones Freightliner en el departamento del Valle del Cauca';
$twitterImage = 'http://freightliner.com.co/tags/centro-valle.jpg';



/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '03.11. 2017';
$imagen = 'img/assets/detalle-noticias/5Noticia-photo.jpg';

$titulo = 'Primer Centro Especializado en Camiones Freightliner en el departamento del Valle';

// --Contenido dividido por parrafos
$contenido[0] = '•	La inversión fue de aproximadamente 2.800 millones de pesos y cuenta con un área de 15.000m2. <br>•	El nuevo concesionario ubicado en la autopista Cali-Yumbo, inició su operación en marzo de 2017. ';
$contenido[1] = 'La inauguración de la nueva vitrina de Andina Motors se encuentra ubicada en la autopista Cali-Yumbo Km 1.7, en un terreno construido total de 15.000m2 y una inversión de aproximadamente 2.800 millones de pesos. Pensada especialmente para atender una de las vías más transitadas de la región, se busca abarcar todas las necesidades del mercado de los Camiones Freightliner. ';
$contenido[2] = 'La nueva sede cuenta con el más completo portafolio de producto de la línea de Camiones Freightliner, líderes indiscutibles en el mercado americano, atendiendo las necesidades de aquellos sectores tradicionales como el de la construcción, infraestructura, entre otros con las referencias FL M2 106, FL M2 112. De igual manera, para el sector de transporte de carga, Freightliner continúa ofreciendo productos en el segmento de Tractocamiones, como la  Columbia y Cascadia. ';
$contenido[3] = '“La mayor ventaja es poder localizarnos en el corazón de la zona industrial de la ciudad (Acopi), de esta manera nuestros clientes amigos de la marca Freightliner no tendrán inconvenientes con la restricción por el tránsito al entrar a la ciudad. Por otra parte, contar con instalaciones dedicadas a Vehículos Comerciales con un gran patio de maniobras y un amplio número de puestos de trabajo”. Agregó Luis Enrique Sanclemente, Gerente Comercial de Vehículos de carga y pasajeros de Andina Motors.';
$contenido[4] = 'Con un área total de 15.000m2 y de vitrina 3.000 m2 aproximadamente,  el concesionario cuenta con servicio de venta y asesoría de vehículos nuevos de la marca Freightliner. Servicio posventa como venta de repuestos originales, mano de obra especializada (diagnóstico de fallas, reparación de motores, colisiones leves), Alineación y balanceo de Vehículos Comerciales, venta de llantas y lubricantes, entre otros. La nueva vitrina, inició su operación en marzo de 2017. El taller y almacén de repuestos abrió sus puertas el miércoles 17 de mayo y su gran lanzamiento oficial fue el martes 23 de mayo.  ';
/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));