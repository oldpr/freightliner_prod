<?php
require_once './app.php';
$classBody = 'producto';

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = 'Una volqueta liviana con una gran capacidad de carga, que permite el rendimiento, resistencia y productividad durante las largas jornadas de operación.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/m2-volco.php';
$ogTitle = 'M2 106 6X4 Volco | Camiones Freightliner Colombia';
$ogDescription = 'Una volqueta liviana con una gran capacidad de carga, que permite el rendimiento, resistencia y productividad durante las largas jornadas de operación.';
$ogImage = 'http://freightliner.com.co/tags/m2-106-volco-6X4.jpg';
$twitterTitle = 'M2 106 6X4 Volco | Camiones Freightliner Colombia';
$twitterDescription = 'Una volqueta liviana con una gran capacidad de carga, que permite el rendimiento, resistencia y productividad durante las largas jornadas de operación.';
$twitterImage = 'http://freightliner.com.co/tags/m2-106-volco-6X4.jpg';



$title = 'M2 106 6X4 Volco | Camiones Freightliner Colombia';

/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = 'img/assets/productos/m2-106-volco-6X4/1-Photo-Billboard-volco-6x4.jpg';
$prodcutoNombre = 'M2 106 VOLCO 6X4';
$productoPrecio = '266.800.000';

/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = 'img/assets/productos/m2-106-volco-6X4/Logo-m2-106.jpg';
$descTitulo = 'La volqueta liviana con gran capacidad de carga.';
$descTexto = 'Para aquellas operaciones en las que se transporta material en vías con control de peso, Freightliner ofrece  el M2 106 6x4 volco, construido en una plataforma muy liviana y resistente que permite sacarle todo el provecho a cada viaje del camión, incrementando la rentabilidad y productividad de la operación.';


/**** SECCION ESPECIFICACIONES ****/

/// --ITEM 1
$imgItem1 = 'img/icon-operaciones.png';
$tituloItem1 = 'SUSPENSIÓN REFORZADA = MAYOR RESISTENCIA';
$subtituloItem1 = 'Operaciones exigentes';
// --ITEM 2
$imgItem2 = 'img/icon-seguridad.jpg';
$tituloItem2 = 'CABINA = VISIBILIDAD Y CONFORT';
$subtituloItem2 = 'Seguridad';
// --ITEM 3
$imgItem3 = 'img/icon-calidad.png';
$tituloItem3 = ' CABINA LIVIANA, LIBRE DE CORROSIÓN';
$subtituloItem3 = 'Calidad';
//--ITEM 4
$imgItem4 = 'img/icon-productividad.png';
$tituloItem4 = 'GRAN CAPACIDAD DE CARGA';
$subtituloItem4 = 'Productividad';

// --LINKS
$linkFichaTecnica = 'pdf/m2106-6X4-volco.pdf';


/**** SECCIÓN IMÁGENES ****/
$linkImg1 = 'img/assets/productos/m2-106-volco-6X4/M2-106-photo-1.jpg';
$linkImg2 = 'img/assets/productos/m2-106-volco-6X4/M2-106-photo-2.jpg';
$linkImg3 = 'img/assets/productos/m2-106-volco-6X4/M2-106-Volco-photo-3.jpg';


/**** SECCIÓN DETALLES ****/

// --DETALLE 1
$imgDetalle1 = 'img/assets/productos/m2-106-volco-6X4/M2-106-Trenmotriz-Photo.jpg';
$tituloDetalle1 = 'tren motriz';
$textoDetalle1 = 'El M2 106 6x4 volco cuenta con un eficiente motor Detroit MBE 900 EPA 98, que le da a la volqueta el poder necesario para hacer el trabajo de manera confiable. Cuenta con una caja de cambios Eaton RT 8908LL de 10 marchas, dos de las cuales son de alta relación y una última marcha directa, brindándole la capacidad de afrontar de manera efectiva las agrestes condiciones de operación de estos vehículos. Sus ejes Meritor de gran capacidad de carga, su suspensión reforzada y sus robustos herrajes, le permiten soportar las exigentes condiciones de carga a las que se someten este tipo de camiones en las carreteras colombianas.

Para mejorar el desempeño en la operación de la volqueta, sus ejes cuentan con bloqueos de diferencia transversal y longitudinal, dándole al operador la posibilidad de contar con la máxima tracción disponible para maniobrar sobre terrenos en mal estado. De igual forma cuenta con un sistema anti-bloqueo de frenos, para aumentar la seguridad y respuesta en superficies con bajo nivel de agarre.';


// --DETALLE 2
$imgDetalle2 = 'img/assets/productos/m2-106-volco-6X4/M2-106-Volco-Productividad-photo.jpg';
$tituloDetalle2 = 'Máxima productividad del Conductor';

$textoDetalle2 = 'El M2 106 6X4 volco se ha creado para lograr que las largas jornadas de trabajo del conductor en las operaciones de carga y descarga en zonas de construcción y canteras sean lo más agradables posibles. Cuenta con una cabina de suspensión neumática, una silla para conductor con suspensión neumática, aire acondicionado de fábrica, vidrio panorámico con amplia visibilidad y tablero con controles ergonómicos que facilitan las maniobras seguras; todo lo que se necesita para reducir el agotamiento y aumentar la productividad y la satisfacción del operador.';


// --DETALLE 3
$imgDetalle3 = 'img/assets/productos/m2-106-volco-6X4/M2-106-desempeño-photo.jpg';
$tituloDetalle3 = 'Máximo desempeño del Camión';

$textoDetalle3 = 'El M2 106 6X4 volco tiene instalado un volco de 14 metros cúbicos fabricado en acero H50, de mayor resistencia que el tradicional acero A36. El diseño tipo góndola con piso liso, facilita el deslizamiento del material en procesos de descarga. Cuenta con un sistema hidráulico de tres vías, que aumentan la vida útil del aceite y reducen la necesidad de mantenimiento del mismo. <br>

El M2 106 es una volqueta diseñada especialmente para el transporte y distribución de agregados por las diferentes vías, canteras y zonas de construcción del país. Es un vehículo que brinda a los profesionales del volante, máximo desempeño, rentabilidad y comodidad para sus largas jornadas de trabajo.';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));
