<?php
require_once './app.php';
$classBody = 'producto';
/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = 'Nuestra Cascadia de Freightliner combina la calidad, elegancia, eficiencia y potencia que demandan los profesionales al volante.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/cascadia-dd15.php';
$ogTitle = 'Cascadia DD15 | Freightliner Colombia';
$ogDescription = 'Nuestra Cascadia de Freightliner combina la calidad, elegancia, eficiencia y potencia que demandan los profesionales al volante.';
$ogImage = 'http://freightliner.com.co/tags/cascadia-dd15.jpg';
$twitterTitle = 'Cascadia | Camiones Freightliner Colombia';
$twitterDescription = 'Nuestra Cascadia de Freightliner combina la calidad, elegancia, eficiencia y potencia que demandan los profesionales al volante.';
$twitterImage = 'http://freightliner.com.co/tags/cascadia-dd15.jpg';


$title = 'Cascadia | Camiones Freightliner Colombia';
/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = 'img/assets/productos/cascadia/1-Photo-Billboard-cascadia.jpg';
$prodcutoNombre = 'CASCADIA ISX EURO 5';
$productoPrecio = '398.500.000';
/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = 'img/assets/productos/cascadia/Logo-cascadia.png';
$descTitulo = 'CASCADIA AVANZA';
$descTexto = 'Con el mismo diseño elegante y aerodinámico validado en el exclusivo túnel de viento del Grupo Daimler en Portland, USA, Cascadia actualiza su tren motriz, logrando el nivel de emisiones Euro 5.  Gracias al nuevo motor Cummins ISX de 425 HP con tecnología Smart Torque, Cascadia alcanza otro gran hito en cuanto a su desempeño y prestaciones, reduciendo su impacto en el medio ambiente.';
/**** SECCION ESPECIFICACIONES ****/
// --ITEM 1
$imgItem1 = 'img/icon-eficiencia.jpg';
$tituloItem1 = 'AERODINÁMICA DISEÑADA EN TÚNEL DE VIENTO    ';
$subtituloItem1 = 'Eficiencia';
// --ITEM 2
$imgItem2 = 'img/icon-seguridad.png';
$tituloItem2 = 'ABS / ATC  ';
$subtituloItem2 = 'Seguridad';
// --ITEM 3
$imgItem3 = 'img/icon-calidad.png';
$tituloItem3 = ' CABINA LIVIANA, LIBRE DE CORROSIÓN';
$subtituloItem3 = 'Calidad';
//--ITEM 4
$imgItem4 = '';
$tituloItem4 = '';
$subtituloItem4 = '';

// --LINKS
$linkFichaTecnica = 'pdf/cascadia.pdf';
/**** SECCIÓN IMÁGENES ****/
$linkImg1 = 'img/assets/productos/cascadia/Cascadia-photo-1.jpg';
$linkImg2 = 'img/assets/productos/cascadia/Cascadia-photo-2.jpg';
$linkImg3 = 'img/assets/productos/cascadia/Cascadia-photo-3.jpg';

/**** SECCIÓN DETALLES ****/
// --DETALLE 1
$imgDetalle1 = 'img/assets/productos/cascadia/Cascadia-Trenmotriz-Photo.jpg';
$tituloDetalle1 = 'tren motriz';
$textoDetalle1 = 'Cascadia supera las expectativas. Ahora con motor Cummins ISX con estándar de emisiones Euro 5 gracias a su sistema de postratamiento de gases de escape (SCR), por encima de lo exigido por la norma de emisiones colombiana.  El desempeño del camión se ve favorecido con la tecnología Smart Torque de Cummins que permite sacarle el mejor provecho a los 425HP del motor, regulando de manera eficiente la entrega de torque según las condiciones de operación, para reducir el consumo de combustible y facilitar la conducción.<br>El motor se complementa con la conocida y muy confiable caja de cambios Eaton RTLO16918B, de 18 marchas. Su amplio rango de relaciones permite contar con la fuerza necesaria para avanzar con carga en condiciones de pendiente extrema y tener una buena velocidad final para lograr economía de combustible en terrenos planos.';
// --DETALLE 2
$imgDetalle2 = 'img/assets/productos/cascadia/Cascadia-Productividad-photo.jpg';
$tituloDetalle2 = 'Máxima productividad del Conductor';
$textoDetalle2 = 'Para aumentar la eficiencia en la operación, la Cascadia tiene sistemas electrónicos ABS (anti-bloqueo defrenos) y ATC (control de tracción) que le brindan al conductor las mejores condiciones de respuesta en fases de detención o de fuerza en la operación. Con su cabina de suspensión neumática, su amplia visibilidad en el vidrio frontal, su tablero y controles ergonómicos al alcance de la mano, la silla para el conductor con suspensión neumática y su aire acondicionado de fábrica, la Cascadia brinda todo lo necesario para facilitar la maniobrabilidad, reducir el agotamiento y aumentar la productividad y satisfacción del operador.';
// --DETALLE 3
$imgDetalle3 = 'img/assets/productos/cascadia/cascadia-desempeno-photo.jpg';
$tituloDetalle3 = 'Máximo desempeño del Camión';
$textoDetalle3 = 'La Cascadia es un tractocamión resistente que cuenta con una plataforma liviana,  bastidor reforzado y una cabina fabricada en aluminio, que logra maximizar la capacidad de carga dentro de los límites de peso establecidos en las normas colombianas, aumentando a su vez, la eficiencia y productividad en la operación. En síntesis, la Cascadia está diseñada para lograr máximo desempeño y rentabilidad en las carreteras del país.';
/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));