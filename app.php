<?php
require_once './vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./views');
$loader->addPath('./templates', 'templates');
$twig = new Twig_Environment($loader);
//$twig = new Twig_Environment($loader,array('cache' => './cache/prod'));