<?php
require_once './app.php';
$classBody = 'producto';
/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = 'Este camión ofrece operaciones más rentables, mayor rendimiento y un desempeño ágil  en las carreteras.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/m2106-6x4-largo.php';
$ogTitle = 'M2 106 6X4 Camión largo | Camiones Freightliner Colombia';
$ogDescription = 'Este camión ofrece operaciones más rentables, mayor rendimiento y un desempeño ágil  en las carreteras.';
$ogImage = 'http://freightliner.com.co/tags/m-2106-camion-6X4-largo.jpg';
$twitterTitle = 'M2 106 6X4 Camión largo | Camiones Freightliner Colombia';
$twitterDescription = 'Este camión ofrece operaciones más rentables, mayor rendimiento y un desempeño ágil  en las carreteras.';
$twitterImage = 'http://freightliner.com.co/tags/m-2106-camion-6X4-largo.jpg';


$title = 'M2 106 6X4 Camión largo | Camiones Freightliner Colombia';
/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = 'img/assets/productos/m-2106-camion-6X4-largo/1-M2-106-Camion-6X4-Billboard.jpg';
$prodcutoNombre = 'M2 106 6X4 LARGO';
$productoPrecio = '317.900.000';
/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = 'img/assets/productos/m-2106-camion-6X4-largo/Logo-m2-106.jpg';
$descTitulo = 'El poder en carretera';
$descTexto = 'El M2 106 6x4 largo ofrece lo mejor de una plataforma liviana (mayor capacidad de carga en vías con control de peso), con un potente motor Cummins ISL Euro 5 de 8.9 litros, 316HP y 996 Lb/pie, lo que permite afrontar    con el mejor rendimiento  las exigentes condiciones de las vías colombianas. El M2 106 camión largo 6X4 brinda todo lo necesario para lograr una operación rentable con un desempeño ágil.';
/**** SECCION ESPECIFICACIONES ****/
// --ITEM 1
$imgItem1 = 'img/icon-eficiencia.jpg';
$tituloItem1 = 'MÁXIMA CAPACIDAD DE CARGA';
$subtituloItem1 = 'Eficiencia';
// --ITEM 2
$imgItem2 = 'img/icon-seguridad.jpg';
$tituloItem2 = 'CABINA = VISIBILIDAD Y CONFORT';
$subtituloItem2 = 'Seguridad';
// --ITEM 3
$imgItem3 = 'img/icon-calidad.png';
$tituloItem3 = ' CABINA LIVIANA, LIBRE DE CORROSIÓN';
$subtituloItem3 = 'Calidad';
// --ITEM 4
$imgItem4 = '';
$tituloItem4 = '';
$subtituloItem4 = '';
// --LINKS
$linkFichaTecnica = 'pdf/m2106-6x4-camion-largo.pdf';
/**** SECCIÓN IMÁGENES ****/
$linkImg1 = 'img/assets/productos/m-2106-camion-6X4-largo/M2-106-Camion-6X4-photo-1.jpg';
$linkImg2 = 'img/assets/productos/m-2106-camion-6X4-largo/M2-106-Camion-6X4-photo-2.jpg';
$linkImg3 = 'img/assets/productos/m-2106-camion-6X4-largo/M2-106-Camion-6X4-photo-3.jpg';
/**** SECCIÓN DETALLES ****/
// --DETALLE 1
$imgDetalle1 = 'img/assets/productos/m-2106-camion-6X4-largo/M2-106-6x4-Trenmotriz-Photo.jpg';
$tituloDetalle1 = 'tren motriz';
$textoDetalle1 = 'Cuenta con una caja de cambios Eaton RTO14908LL de 10 marchas, dos de las cuales son de alta relación y una última marcha overdrive, brindandole al M2 106 Camión largo 6X4, la versatilidad para operar de manera eficiente en todos los terrenos. Pensando en el máximo desempeño, cuenta con un sistema de post-tratamiento de gases de escape (SCR), el cual reduce emisiones contaminantes, incluso más allá de lo requerido por las normas colombianas, sin sacrificar la eficiencia del motor, ni degradar de manera prematura el aceite de motor con cenizas y mayores temperaturas de trabajo. Esto significa un menor consumo de combustible, menores costos de mantenimiento preventivo y mayor disponibilidad del vehículo. Por otro lado, cuenta con un muy eficiente freno de motor Jacobs, cuya operación aumenta la seguridad en la operación del vehículo al momento de reducir y controlar la velocidad, sin tener que sobre exigir los frenos de servicio del camión.';
// --DETALLE 2
$imgDetalle2 = 'img/assets/productos/m-2106-camion-6X4-largo/M2-106-Camion-6X4-Productividad-photo.jpg';
$tituloDetalle2 = 'Máxima productividad del Conductor';
$textoDetalle2 = 'El M2 106 6x4 largo se ha creado para lograr que las largas jornadas de trabajo del operador sean más agradables, para ello, cuenta con una cabina de suspensión neumática, vidrio panorámico de amplia visibilidad, tablero y controles ergonómicos, además de aire acondicionado de fábrica que facilitan la maniobrabilidad en carretera, reducen el agotamiento y aumentan la productividad del operador.';
// --DETALLE 3
$imgDetalle3 = 'img/assets/productos/m-2106-camion-6X4-largo/M2-106-6x4-desempeno-photo.jpg';
$tituloDetalle3 = 'Máximo desempeño del Camión';
$textoDetalle3 = 'El M2 106 6x4 largo cuenta con un amplio largo carrozable, ideal para ser empleado con carrocerías tipo estacas, furgón, furgón refrigerado, planchón y grúa, así como en aplicaciones especiales como bombas concreteras.';
/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));