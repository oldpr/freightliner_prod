<?php
session_start();
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** NOTICIA ****/
$categoria = 'Sala de Presa';
$fecha = 'Ayer';
$imagen = 'img/noticia-01.jpg';

$titulo = 'Noticia de sala de prensa';

// --Contenido dividido por parrafos
$contenido[0] = 'Parrafo 1';
$contenido[1] = 'Parrafo 2';
$contenido[2] = 'Parrafo 3';

/**** NOTICIA RESTRINGIDA ****/

$restringir = true;

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

$datos = array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'restringir' => $restringir
);

if ( isset($_SESSION['usuario'])){
    $datos['usuario'] = $_SESSION['usuario'];
}

echo $twig->render('noticia.html.twig', $datos);