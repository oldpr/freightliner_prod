<?php
require('conexion.php');
if (!empty($_POST) && isset($_POST['correo'])) {
    $conexion = conectar();
    $correo = $_POST['correo'];
    $query = "INSERT INTO newsletter (correo) VALUES ('$correo')";
    $conexion->query($query);
    if ($conexion->error) {
        try {
            $res = array('code'=>400, 'msj'=>$conexion->error);
        } catch(Exception $e ) {
            $res = array('code'=>400, 'msj'=>$e->getMessage(), 'error'=>$e->getCode());
        }
    }else{
        $id = $conexion->insert_id;
        $res = array('code'=>200, 'id'=>$id);
    }
    desconectar($conexion);
    require_once './../vendor/autoload.php';

    $loader = new Twig_Loader_Filesystem('./../mails/');
    $twig = new Twig_Environment($loader);

    $to  = $correo;
    $message = $twig->render('bienvenido.html.twig');
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: Freightliner Colombia <no-reply@freightliner.com.co>' . "\r\n";
    mail($to, 'Suscrito a freightliner.com.co', $message, $headers);
}else {
    $res = array('code' => 400, 'msj' => 'Error: datos insuficientes');
}
echo json_encode($res);