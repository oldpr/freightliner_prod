<?php
ob_start();
?>
<?php
if(isset($_POST['correo'])) {
$email_to = "diana.montes@daimler.com";

$email_subject = "Contacto Freightliner";
function died($error) {
echo "Lo sentimos, hay un error en sus datos y el formulario no puede ser enviado. ";
echo "Detalle de los errores.<br /><br />";
echo $error."<br /><br />";
echo "Porfavor corrije los errores e inténtelo de nuevo.<br /><br />";
die();}
if(!isset($_POST['nombre']) ||
!isset($_POST['correo']) ||
!isset($_POST['telefono']) ||
!isset($_POST['producto']) ||
!isset($_POST['concesionario']) ||
!isset($_POST['ciudad']) ||
//!isset($_POST['mediointeres']) ||
!isset($_POST['comentarios']) ||
!isset($_POST['vehicle'])) {died('Lo sentimos pero parece haber un problema con los datos enviados.');}
$first_name = $_POST['nombre']; // requerido
$email_from = $_POST['correo']; // requerido
$telefono = $_POST['telefono']; // requerido
$producto = $_POST['producto']; // no requerido
$concesionario = $_POST['concesionario'];
$ciudad = $_POST['ciudad'];
$comentarios = $_POST['comentarios']; // no requerido
$vehicle = $_POST['vehicle']; // requerido
$error_message = "Error";

        //Verificar que la dirección de correo sea válida

        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

        if(!preg_match($email_exp,$email_from)) {

            $error_message .= 'La dirección de correo proporcionada no es válida.<br />';

        }

        //Validadacion de cadenas de texto

        $string_exp = "/^[A-Za-z .'-]+$/";

        if(!preg_match($string_exp,$first_name)) {

            $error_message .= 'El formato del nombre no es válido<br />';

        }

        if(strlen($error_message) < 0) {

            died($error_message);

        }

        //Plantilla de mensaje

        $email_message = "Contenido del Mensaje.\n\n";

        function clean_string($string) {

            $bad = array("content-type","bcc:","to:","cc:","href");

            return str_replace($bad,"",$string);

        }

        $email_message .= "Nombre: ".clean_string($first_name)."\n";

        $email_message .= "Celular: ".clean_string($telefono)."\n";

        $email_message .= "Email: ".clean_string($email_from)."\n";

        $email_message .= "Productos: ".clean_string($producto)."\n";

        $email_message .= "Ciudad: ".clean_string($ciudad)."\n";

        $email_message .= "Concesionario: ".clean_string($concesionario)."\n";

        $email_message .= "Comentarios: ".clean_string($comentarios)."\n";

        $email_message .= "Vehicle: ".clean_string($vehicle)."\n";

        //Encabezados

        $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($email_to, $email_subject, $email_message, $headers);
        echo'mensaje enviado';
        header('Location: ../contacto.php');
    } else{

        echo 'No llearon datos';

    }
?>
<?php
ob_end_flush();
?>
