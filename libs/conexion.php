<?php
function conectar(){
    $config = parse_ini_file('config.ini');
    $connection = mysqli_connect('localhost', $config['username'], $config['password'], $config['dbname']);
    if ($connection === false) {
        echo 'Ha habido un error <br>' . mysqli_connect_error();
    }
    return $connection;
}
function desconectar($connection){
    $connection->close();
}