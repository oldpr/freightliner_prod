<?php
if (!empty($_POST)){
    if (isset($_POST['nombre']) && isset($_POST['correo']) && isset($_POST['ciudad']) && isset($_POST['concesionario']) && isset($_POST['mediointeres']) && isset($_POST['telefono'])){
        $nombre = $_POST['nombre'];
        $correo = $_POST['correo'];
        $telefono = $_POST['telefono'];
        $ciudad = $_POST['ciudad'];
        $concesionario = $_POST['concesionario'];
        $mediointeres = $_POST['mediointeres'];
        $producto = $comentarios = '';
        if (isset($_POST['producto']))$producto = $_POST['producto'];
        if (isset($_POST['comentarios']))$comentarios = $_POST['comentarios'];

        $to  = 'diana.montes@daimler.com';
        $subject = 'Contacto freightliner';
        $message = "
            Mensaje de contacto<br>
            Nombre: $nombre<br>
            correo: $correo<br>
            Teléfono: $telefono<br>
            Ciudad: $ciudad<br>
            Concesionario: $concesionario<br>
            Medio: $mediointeres<br>
            Producto: $producto<br>
            Comentarios: $comentarios
        ";
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        //$headers .= 'From: no-reply <info@freightliner.com.co>' . "\r\n";

        if (mail($to, $subject, $message, $headers)){
            require_once './../vendor/autoload.php';

            $loader = new Twig_Loader_Filesystem('./../mails/');
            $twig = new Twig_Environment($loader);

            $to  = $correo;
            $message = $twig->render('gracias.html.twig', array('nombre' => $nombre));
            mail($to, $subject, $message, $headers);
            echo json_encode(array('code'=>200));
        }else{
            echo json_encode(array('code'=>400, 'msj'=>'Error: No se pudo enviar el correo'));
        }


    }else{
        echo json_encode(array('code'=>400, 'msj'=>'Error: No se enviaron los datos necesarios'));
    }
}else{
    echo json_encode(array('code'=>400, 'msj'=>'Error: No se enviaron datos'));
}
