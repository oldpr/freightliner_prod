<?php
require_once './app.php';
$classBody = 'info training';

echo $twig->render('drive-training.html.twig', array(
    'active' => 3,
    'classBody' => $classBody
));