<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Freightliner marca record histórico de participación | Freightliner Colombia';
$metaDescripcion = 'Freightliner, la marca líder de Norteamérica obtuvo un record histórico del 44% en la participación de mercado del segmento de carga.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/La-marca-Freightliner-obtiene-record-historico-del-44-porciento-de-participacion-de-mercado.php';
$ogTitle = 'Freightliner marca record histórico de participación | Freightliner Colombia';
$ogDescription = 'Freightliner, la marca líder de Norteamérica obtuvo un record histórico del 44% en la participación de mercado del segmento de carga.';
$ogImage = 'http://freightliner.com.co/tags/record-historico.jpg';
$twitterTitle = 'Freightliner marca record histórico de participación | Freightliner Colombia';
$twitterDescription = 'Freightliner, la marca líder de Norteamérica obtuvo un record histórico del 44% en la participación de mercado del segmento de carga.';
$twitterImage = 'http://freightliner.com.co/tags/record-historico.jpg';



/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '03.11. 2017';
$imagen = 'img/noticia-02.jpg';

$titulo = 'La marca Freightliner obtiene récord histórico del 44% de participación de mercado';

// --Contenido dividido por parrafos
$contenido[0] = 'Según el RUNT, el acumulado de matrículas a cierre del mes de septiembre de 2017, es de 517 unidades en el segmento compuesto por las marcas americanas. En el mes de Septiembre, Freightliner obtuvo una cifra histórica del 44% de participación de mercado en el segmento americano de carga. Se espera que para cierre del año 2017, Freightliner continúe consolidándose como la marca preferida por los transportadores colombianos. ';
$contenido[1] = 'PLa marca de camiones Freightliner obtuvo al cierre del mes de septiembre una cifra histórica de 44% de market share en el segmento de camión americano. 
';
$contenido[2] = '“La marca Freightliner ha dedicado múltiples esfuerzos en la configuración de un producto adecuado a las necesidades del transportador colombiano. Con un trabajo cercano con los diferentes actores del segmento de carga, se ha demostrado la idoneidad de nuestro portafolio de productos para las exigentes labores de nuestros clientes.  Un óptimo consumo de combustible al igual que unos gastos de operación rentables hacen la combinación perfecta para que seamos los escogidos”. Así lo afirmó Alexander Peña, Director de Camiones Daimler. 
';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));