var isBreakPoint = function (bp) {
    var bps = [320, 480, 768, 1024],
        w = $(window).width(),
        min, max
    for (var i = 0, l = bps.length; i < l; i++) {
      if (bps[i] === bp) {
        min = bps[i-1] || 0
        max = bps[i]
        break
      }
    }
    return w > min && w <= max
}

// $("#productos, .menu-expandido").hover(function(){
// 	if (parseInt($(window).width()) > 768) {
//     	$(".menu-expandido").toggle();
//       $(".overlay").fadeToggle("slow");
//     }
// });
$("#productos, .menu-expandido").hover(function(){
  if (parseInt($(window).width()) > 768) {
      $(".menu-expandido").toggle();
      $(".overlay").css("opacity", "0.7");
    }
});
$("#productos, .menu-expandido").mouseleave(function(){
      $(".overlay").css("opacity", "0");
});

$(".btn-productos").click(function(){
    $(".menu-expandido, .overlay").toggle();
});

$(".overlay, .menu-close").click(function(){
    $(".menu-expandido, .overlay").hide();
});

$("#menu-item, #menu-item a:before").click(function(){
    $(".menu-back").show();
});

$(".menu-back").click(function(){
    $(".menu-back").hide();
});

$(document).ready(function() {
   /*$("#carousel-example-generic").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#carousel-example-generic").swipeleft(function() {
      $(this).carousel('next');
   });*/
   var pathname = window.location.pathname;
   /*if (pathname == '/cascadia-dd15.php') {
    $(".btn-ficha").addClass('hidden');
   }*/
});