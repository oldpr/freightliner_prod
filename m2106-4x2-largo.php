<?php
require_once './app.php';
$classBody = 'producto';
/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = 'Un camión versátil creado para el demandante trabajo en carretera, cuenta con amplias configuraciones para diversas aplicaciones.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/m2106-4x2-largo.php';
$ogTitle = 'M2 106 4X2 Camión Largo | Camiones Freightliner Colombia';
$ogDescription = 'Un camión versátil creado para el demandante trabajo en carretera, cuenta con amplias configuraciones para diversas aplicaciones';
$ogImage = 'http://freightliner.com.co/tags/m2-106-camion-4x2-largo.jpg';
$twitterTitle = 'M2 106 4X2 Camión Largo | Camiones Freightliner Colombia';
$twitterDescription = 'Un camión versátil creado para el demandante trabajo en carretera, cuenta con amplias configuraciones para diversas aplicaciones';
$twitterImage = 'http://freightliner.com.co/tags/m2-106-camion-4x2-largo.jpg';


$title = 'M2 106 4X2 Camión Largo | Camiones Freightliner Colombia';
/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = 'img/assets/productos/m2-106-camion-4x2-largo/1-M2-106-Camion-Billboard.jpg';
$prodcutoNombre = 'M2 106 4X2 LARGO';
$productoPrecio = '274.900.000';
/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = 'img/assets/productos/m2-106-camion-4x2-largo/Logo-m2-106.jpg';
$descTitulo = 'El poder en carretera';
$descTexto = 'El M2 106 4X2 Tracto ha sido concebido para el demandante trabajo en carretera. Su poderoso motor Cummins ISL Euro 5,  le da la vitalidad y el desempeño que se requiere en las exigentes rutas colombianas.';
/**** SECCION ESPECIFICACIONES ****/
// --ITEM 1
$imgItem1 = 'img/icon-eficiencia.jpg';
$tituloItem1 = 'MOTOR POTENTE CON FRENO DE MOTOR JACOBS';
$subtituloItem1 = 'Eficiencia';
// --ITEM 2
$imgItem2 = 'img/icon-seguridad.jpg';
$tituloItem2 = 'CABINA = VISIBILIDAD Y CONFORT';
$subtituloItem2 = 'Seguridad';
// --ITEM 3
$imgItem3 = 'img/icon-calidad.png';
$tituloItem3 = ' CABINA LIVIANA, LIBRE DE CORROSIÓN';
$subtituloItem3 = 'Calidad';
// --ITEM 4
$imgItem4 = '';
$tituloItem4 = '';
$subtituloItem4 = '';
// --LINKS
$linkFichaTecnica = 'pdf/m21064x2-camion-largo.pdf';
/**** SECCIÓN IMÁGENES ****/
$linkImg1 = 'img/assets/productos/m2-106-camion-4x2-largo/M2-106-Camion-photo-1.jpg';
$linkImg2 = 'img/assets/productos/m2-106-camion-4x2-largo/M2-106-Camion-photo-2.jpg';
$linkImg3 = 'img/assets/productos/m2-106-camion-4x2-largo/M2-106-Camion-photo-3.jpg';
/**** SECCIÓN DETALLES ****/
// --DETALLE 1
$imgDetalle1 = 'img/assets/productos/m2-106-camion-4x2-largo/M2-106-Camion-Trenmotriz-Photo.jpg';
$tituloDetalle1 = 'tren motriz';
$textoDetalle1 = 'El M2 106 4x2 tracto cuenta con una caja Eaton FRO14210C, de 10 marchas. Su amplio rango de relaciones permite contar con la fuerza para avanzar con carga en condiciones de pendiente extrema. Sus ejes Meritor le brindan la posibilidad de llegar hasta las 17 toneladas de PBV, lo que le permite contar con el máximo de capacidad de carga admisible en las carreteras colombianas. Su motor Cummins ISL de 8.9 Litros cuenta con un sistema de pos-tratamiento de gases de escape (SCR) que logra reducir las emisiones contaminantes, incluso más allá de lo requerido por las normas colombianas y sin sacrificar la eficiencia del motor, ni degradar el aceite de motor con cenizas y mayores temperaturas de trabajo. Esto significa un menor consumo de combustible, menores costos de mantenimiento preventivo y mayor disponibilidad del vehículo. 
    <br>
    Por otro lado, cuenta con un muy eficiente freno de motor Jacobs, cuya operación aumenta la seguridad en el trabajo del vehículo al momento de reducir y controlar la velocidad y sin tener que sobre exigir los frenos de servicio del camión.';
// --DETALLE 2
$imgDetalle2 = 'img/assets/productos/m2-106-camion-4x2-largo/M2-106-Camion-Productividad-photo.jpg';
$tituloDetalle2 = 'Máxima productividad del conductor';
$textoDetalle2 = 'El M2 106 4x2 tracto ha sido creado para lograr que las largas jornadas de trabajo del operador sean más agradables, para ello cuenta con una cabina de suspensión neumática, vidrio panorámico de amplia visibilidad, tablero y controles ergonómicos, además de aire acondicionado de fábrica que facilitan la maniobrabilidad en carretera, reducen el agotamiento y aumentan la productividad del operador.';
// --DETALLE 3
$imgDetalle3 = 'img/assets/productos/m2-106-camion-4x2-largo/M2-106-Camion-desempeno-photo.jpg';
$tituloDetalle3 = 'Máximo desempeño del camión';
$textoDetalle3 = 'El M2 106 4x2 largo cuenta con un amplio largo carrozable, ideal para ser empleado con carrocerías tipo estacas, furgón, furgón refrigerado, planchón y grúa, enfocado en lograr la máxima productividad de la operación a la que se destine.';
/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));