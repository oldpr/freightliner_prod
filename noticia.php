<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = '';
$metaDescripcion = '';
$metaKeywords = '';
$ogUrl = '';
$ogTitle = '';
$ogDescription = '';
$ogImage = '';
$twitterTitle = '';
$twitterDescription = '';
$twitterImage = '';

/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '';
$imagen = '';

$titulo = '';

// --Contenido dividido por parrafos
$contenido[0] = 'Parrafo 1';
$contenido[1] = 'Parrafo 2';
$contenido[2] = 'Parrafo 3';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));