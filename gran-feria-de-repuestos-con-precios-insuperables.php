<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Noticias Freightliner Colombia - Gran Feria de Respuestos a Precios Insuperables';
$metaDescripcion = 'Visite nuestra red de concesionarios y distribuidores autorizados a nivel nacional y adquiera ya sus repuestos con precios insuperables';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles, precios, insuperables, feria';
$ogUrl = 'https://www.freightliner.com.co/gran-feria-de-repuestos-con-precios-insuperables.php';
$ogTitle = 'Noticias Freightliner Colombia - Gran Feria de Respuestos a Precios Insuperables';
$ogDescription = 'Visite nuestra red de concesionarios y distribuidores autorizados a nivel nacional y adquiera ya sus repuestos con precios insuperables';
$ogImage = 'https://www.freightliner.com.co/img/gran-feria-repuestos.jpg';
$twitterTitle = 'Noticias Freightliner Colombia - Gran Feria de Respuestos a Precios Insuperables';
$twitterDescription = 'Visite nuestra red de concesionarios y distribuidores autorizados a nivel nacional y adquiera ya sus repuestos con precios insuperables';
$twitterImage = 'https://www.freightliner.com.co/img/gran-feria-repuestos.jpg';

/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '30.11.2017';
$imagen = 'img/gran-feria-repuestos.jpg';

$titulo = 'LLEGA LA GRAN FERIA DE REPUESTOS CON PRECIOS INSUPERABLES.';

// --Contenido dividido por parrafos
$contenido[0] = 'Gracias a nuestro éxito extendimos nuestra gran Feria de Repuestos, la red de concesionarios y
distribuidores de repuestos autorizados a nivel nacional de Daimler Colombia, abren sus puertas hasta el 31 de marzo del presente año
para recibir a todos sus clientes interesados en adquirir repuestos de las marcas Mercedes-Benz,
Freightliner y FUSO con precios insuperables. Durante la feria los clientes tendrán la
oportunidad de adquirir paquetes de repuestos con hasta el 90% de descuento.';
$contenido[1] = 'Se espera que alrededor 11 mil piezas sean comercializadas durante la feria. Daimler Colombia
recomienda el uso de repuestos originales para garantizar el buen funcionamiento del vehículo.';
$contenido[2] = '<b>Bogotá, Febrero/ 2018</b>. Daimler Colombia representante de las marcas Mercedes-Benz,
Freightliner y FUSO, nuevamente estará presente hasta el 31 de marzo o hasta agotar existencias, en toda su red de concesionarios y distribuidores autorizados a nivel nacional con
la <b>Gran Feria de Repuestos para Vehículos Comerciales.</b>';
$contenido[3] = 'Durante la feria, los clientes tendrán la oportunidad de adquirir repuestos genuinos, con una
oferta insuperable de descuentos y precios de repuestos de la marca <b>Mercedes-Benz</b> con su línea
de <b>Camiones, vans y buses</b>, así como también la marca de <b>camión americano Freightliner</b> y
de igual manera, podrán adquirir repuestos de la marca de <b>camión liviano FUSO.</b>';
$contenido[4] = '<i>“Nuevamente estamos realizando nuestra gran feria de repuestos de vehículos comerciales. El
año pasado obtuvimos un cierre exitoso, sobrepasando la meta propuesta. Este año vamos por
un objetivo más ambicioso, teniendo en cuenta que nunca antes habíamos tenido descuentos
hasta del 90%.”</i>. Así lo afirmó Lorenzo Zanon, Gerente Comercial para la Dirección de
Posventa de Vehículos Comerciales de Daimler Colombia.';
$contenido[5] = 'El objetivo de la feria se centra en entregar por tiempo limitado, “Paquetes de Repuestos” que
incluyen grandes descuentos, pues son repuestos de alta y mediana rotación, siendo piezas para
colisión, reparaciones de motor, mantenimiento general, entre otros.';
$contenido[6] = 'Más de 11 mil referencias de alta rotación y a precios nunca antes vistos, estarán disponibles
durante la feria.';
$contenido[7] = '<i>“Con esta Feria, queremos llegar a nuestros clientes finales con disponibilidad y precios
competitivos, dándoles a conocer nuestra red de concesionarios a nivel nacional, donde estas se
encuentran a su disposición para los requerimientos de su negocio”</i>. Concluyó Zanon.
El servicio Posventa de Mercedes-Benz ha estado siempre comprometido con entregar repuestos
genuinos, en el sitio que se requiera, con precios justos y con la calidad que siempre ha
destacado a la marca. Cuenta con 16 puntos de servicio a lo largo del país, de los cuales 11 están
ubicados en las sedes de los concesionarios y talleres autorizados, un punto de servicio posventa
y 4 distribuidores autorizados de repuestos, además de contar con asesores debidamente
capacitados, brindando las herramientas especializadas para garantizar cualquier intervención en
su vehículo.';
$contenido[8] = 'Cabe resaltar que ese 90% de descuento es sobre el precio de venta sugerido al público antes de
IVA en referencias seleccionadas por la vigencia de la feria o hasta agotar existencias.';
$contenido[9] = '<b>¡¡¡Visite nuestra red de concesionarios y distribuidores autorizados a nivel nacional y
adquiera ya sus repuestos con precios insuperables!!!</b>';
$contenido[10] = '<b style="text-decoration: underline">Sobre Daimler Colombia:</b>';
$contenido[11] = 'Desde 1948 Mercedes-Benz en Colombia ha sido representada, inicialmente por la empresa
<b>Automercantil Ltda</b>. En 1998 se transformó en <b>Mercedes-Benz Colombia S.A</b>, pasando en ese
momento a ser parte de la gran familia Daimler. Actualmente y desde 2007, somos <b>Daimler
Colombia S.A</b>, representantes de las marcas <b>Mercedes-Benz, Thomas Built Buses, Detroit,
Freightliner y FUSO</b> apostando fuertemente a su crecimiento en territorio colombiano. Bajo la
dirección de la casa matriz, Daimler AG,  Daimler Colombia está en constante expansión de su
red de concesionarios y distribuidores autorizados, presente en más de diez ciudades del país.';
$contenido[12] = '<b>Contacto:</b>';
$contenido[13] = 'María Alejandra Cortés Oramas<br>
Comunicaciones &amp; PR at Vehículos comerciales Daimler Colombia<br>
(1) 423 6700 ext. 1646<br>
<a href="mailto:maria.cortes@daimler.com">maria.cortes@daimler.com</a>';
$contenido[14] = 'Natalia Gómez U.<br>
Communications &amp; PR at Daimler Colombia<br>
Tel: +57 (1) 423 67 00 ext 1635<br>
<a href="mailto:natalia.gomez@daimler.com">natalia.gomez@daimler.com</a>';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));