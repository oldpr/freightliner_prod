<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = '';
$metaKeywords = '';
$ogUrl = '';
$ogTitle = '';
$ogDescription = '';
$ogImage = '';
$twitterTitle = '';
$twitterDescription = '';
$twitterImage = '';

/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '20.09. 2017';
$imagen = 'img/noticia-01.jpg';

$titulo = 'LA MARCA DE CAMIONES FREIGHTLINER LE APUESTAN AL PLAN DE INFRAESTRUCTURA VIAL EN EL DEPARTAMENTO DE ANTIOQUIA.';

// --Contenido dividido por parrafos
$contenido[0] = 'La ejecución de proyectos del plan de infraestructura en marcha en Colombia se convierte en una oportunidad para la dinamización de diferentes sectores, que ven en esta apuesta su posibilidad de reactivación como es el caso del sector transportador. La marca de Camiones Freightliner está preparada con los más altos desarrollos en eficiencia, para hacer su aporte en la oferta de productos rentables, que incidan positivamente en la movilidad, accesibilidad, transporte, movimiento y demás servicios requeridos para sacar este proyecto de renovación nacional adelante. ';
$contenido[1] = 'Por lo anterior, Alemautos, ubicado en la ciudad de Medellín realiza una primera entrega de 10 volquetas Freightliner M2 112 Volco Hardox, las cuales hacen parte de una flota total de 30 Volquetas, destinadas a uno de los proyectos de infraestructura vial más grandes del departamento de Antioquia, el cual está en cabeza de la Concesionaria Vial del Pacifico, Covipacífico S.A.S con el proyecto Autopista Conexión Pacífico1. Este proyecto traerá con la construcción de la nueva autopista, grandes beneficios a diferentes escalas, una de ellas de índole regional, ya que mejorará las condiciones de comunicación de Medellín con los municipios del Suroeste de Antioquia, localidades que se caracterizan por su potencial en producción agrícola. ';
$contenido[2] = '“Estamos muy contentos de realizar esta primera entrega de Volquetas Freightliner M2 112 a nuestros clientes que pertenecen a las diferentes empresas privadas que operan en el Departamento de Antioquia y que le prestan el servicio a Covipacífico. Sabemos de la importancia de este proyecto de infraestructura vial, siendo uno de los más grandes realizados en la región antioqueña y esperamos que se convierta en un modelo de referencia en el resto del país. Así lo afirmó (Vocero Alemautos). ';
$contenido[3] = 'Esta obra de infraestructura vial comprende la construcción, mantenimiento y operación de 32.2 km de vía nueva en doble calzada, Bolombolo - Camilo C, sector "Cuatro Palos" y la operación y mantenimiento de 18 km de vía de "Cuatro Palos" - Ancón Sur, para un total de 50.2 km de vía concesionada.';
$contenido[4] = 'Esta conexión permitirá que los municipios del área de influencia obtengan ventajas para la localización de empresas, centros de acopio y procesamiento e intercambio regional de bienes y servicios, al convertirse en nodo donde se cruzan varias vías o concesiones de altas especificaciones. Asimismo, se facilitará el turismo y los servicios de recreación en los municipios del Suroeste.';
$contenido[5] = 'Además, la nueva autopista permitirá que los municipios estén mejor conectados con los principales centros económicos, dentro y fuera de Antioquia, y posibilitará la conquista de mercados internacionales.';
$contenido[6] = 'Durante la construcción del proyecto se estimulará el empleo y se dinamizará la economía en la región. Se estima la generación de 2.100 empleos directos e indirectos a corto, mediano y largo plazo. Adicionalmente, gracias a las mejores especificaciones técnicas de la vía, se reducirán los tiempos de comunicación y por ende los costos de transporte.';
$contenido[7] = 'Por su parte Alexander Peña, director de Camiones Daimler, asegura que la marca siempre ha buscado adaptar el producto a las situaciones particulares del país y así optimizar los atributos en configuración y desempeño, siendo estos los beneficios en rentabilidad de la operación, que hacen de un vehículo el verdadero camión colombiano para el trabajo.';
$contenido[8] = '“Con las volquetas Freightliner siempre nos hemos destacado en el mercado por su configuración con motor Mercedes-Benz y con las mejores prestaciones en terreno y eficiencia de combustible. Con esta entrega, nuestros clientes obtienen un camión Freightliner M2 112 con caja de volteo en su versión de máxima resistencia Hardox, considerado en el mercado como indestructible, lo que garantiza una durabilidad de la caja entre 10 a 15 años que automáticamente se traduce en una solución rentable para el propietario en la operación de vías de infraestructura”. Comentó Peña. ';
$contenido[9] = 'Este proyecto que se estima sea finalizado y entregado en el año 2020 es construido por empresas de origen privado de la región Antioqueña, entre las cuales se destaca Lugon Ingeniería, empresa dedicada a la construcción de carreteras/vías y quienes adquirieron una gran flota de volquetas Freightliner M2 112 para la consecución de las mismas. “Estos vehículos representan para nosotros calidad, economía de combustible, rendimiento en costo por kilómetro y gran capacitad de carga, aspectos clave a la hora de elegir un camión para la operación de obras de infraestructura vial como lo son las 4G”. Así lo afirmó Luis Gonzalez de la empresa Lugon Ingeniería S.AS.';
$contenido[10] = '<i>“Con este producto robusto y confiable, estamos seguros de satisfacer los exigentes requerimientos de nuestros clientes en los proyectos de infraestructura 4G y vías terciarias que se están desarrollando en el Departamento de Antioquia y a lo largo del país”.</i> Concluyó Peña.<br>';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));