<?php
require_once './app.php';
$classBody = 'info';

echo $twig->render('timeline.html.twig', array(
    'active' => 5,
    'classBody' => $classBody
));