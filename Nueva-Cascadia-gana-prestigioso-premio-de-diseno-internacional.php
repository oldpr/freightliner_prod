<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Nueva Cascadia gana prestigioso premio de diseño internacional. | Freightliner Colombia';
$metaDescripcion = 'Nueva Cascadia gana prestigioso premio de diseño internacional..';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/Nueva-Cascadia-gana-prestigioso-premio-de-diseno-internacional.php';
$ogTitle = 'Nueva Cascadia gana prestigioso premio de diseño internacional. | Freightliner Colombia';
$ogDescription = 'Nueva Cascadia gana prestigioso premio de diseño internacional.';
$ogImage = 'http://freightliner.com.co/tags/alemautos.jpg';
$twitterTitle = 'Nueva Cascadia gana prestigioso premio de diseño internacional.| Freightliner Colombia';
$twitterDescription = 'Nueva Cascadia gana prestigioso premio de diseño internacional.';
$twitterImage = 'http://freightliner.com.co/tags/alemautos.jpg';



/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '12.03. 2018';
$imagen = 'img/assets/detalle-noticias/nuevacascadia-1.jpg';

$titulo = 'Nueva Cascadia gana prestigioso premio de diseño internacional.';

// --Contenido dividido por parrafos
$contenido[0] = 'En el primer semestre del año 2017 Daimler Estados Unidos impuso nuevos estándares en contectividad, eficiencia de combustible y seguridad, con el lanzamiento de la Nueva Cascadia. Desde entonces, el entusiasmo no se ha mantenido únicamente en conductores y sector empresarial, el jurado de los Good Design Awards, uno de los concursos más prestigiosos de diseño industrial, premió el atractivo y funcional diseño del novedoso camión americano.

';
$contenido[1] = 'El jurado, quien elige al ganador entre los diseños de producto de algunas marcas de todas las industrias más exitosas del mundo, otorgó el premio al camión insignia de carga pesada por su diseño de interior de su cabina.

';
$contenido[2] = 'El nuevo interior de la Cascadia combina una tecnología innovadora con diseño ergonómico, ofrece un tablero optimizado y una multitud de configuraciones en sus asientos. Además, la "sala de estar" de los conductores fue diseñada para ofrecer más espacio de almacenamiento y hasta espacio para un microondas de tamaño estándar y televisores de pantalla plana.

';
$contenido[3] = 'Desde el comienzo de las ventas, la división de vehículos comerciales norteamericana de Daimler vendió más de 20,000 unidades de esta nueva Cascadia. Con una participación de mercado más reciente de 39.2% en las categorías de 6 a 8 toneladas Daimler Trucks sigue siendo el líder indiscutible del mercado en segmentos de camiones de servicio mediano y pesado en Norteamérica por un amplio margen. En la región del TLCAN, Daimler Trucks continúa creciendo en número de pedidos.
';




/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));