<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Codigos;
use AppBundle\Entity\Recalls;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(){
        return $this->render('AdminBundle:Default:index.html.twig');
    }

    public function subirCodigosRecallAction($idRecall, Request $request){

        $recall = $this->getDoctrine()->getRepository('AppBundle:Recalls')->find($idRecall);

        if ($request->request->get('caracter')) {
            $caracter = $request->request->get('caracter');
        }
        if ($request->request->get('codigos')) {
            $listaCodigos = $request->request->get('codigos');
        }

        if (isset($caracter) && ($caracter == ',' || $caracter == ';')) {

            $file = $request->files->get('fichero_usuario');
            if ($file) {
                $codigos = array();
                if (($gestor = fopen($file, "r")) !== FALSE) {
                    $fila = $cont = 0;
                    while (($datos = fgetcsv($gestor, 1500, $caracter)) !== FALSE) {
                        $numero = count($datos);
                        $fila++;
                        foreach ($datos as $codigo){
                            $codigos[$cont] = $codigo;
                            $cont++;
                        }
                    }
                    fclose($gestor);
                    return $this->render('AdminBundle:Default:subirCodigosRecall.html.twig', array('paso2' => true, 'entidad'=>$recall, 'codigos' => $codigos));
                }

                return $this->render('AdminBundle:Default:archivoCursosAdultos.html.twig', array('error' => 'Formato del archivo no valido', 'entidad'=>$recall));
            }
            return $this->render('AdminBundle:Default:archivoCursosAdultos.html.twig', array('error' => 'Archivo no valido', 'entidad'=>$recall));
        }elseif (isset($listaCodigos) && $listaCodigos != '') {

                $codigos = json_decode($listaCodigos);
                if ($this->eliminarCodigosAnteriores($recall)){
                    foreach ($codigos as $codigo ){
                        $entity = new Codigos();
                        $entity->setCodigo($codigo);
                        $entity->setFecha(new DateTime(date('Y-m-d')));
                        $entity->setRecall($recall);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($entity);
                        $em->flush();
                    }

                    return $this->redirectToRoute('admin_recalls_edit', array('id' => $idRecall));
                }
                return $this->render('AdminBundle:Default:archivoCursosAdultos.html.twig', array('error' => 'No se pudo borrar los archivos actuales', 'entidad'=>$recall));

        }else{
            return $this->render('AdminBundle:Default:subirCodigosRecall.html.twig', array('entidad'=>$recall));
        }
    }
    private function eliminarCodigosAnteriores(Recalls $recall){
        $codigos = $recall->getCodigos();
        if (!empty($codigos)){
            try {
                foreach ($recall->getCodigos() as $codigo) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($codigo);
                    $em->flush();
                }
            }catch (\Exception $e){
                return false;
            }
        }
        return true;

    }

    public function registrosAction(){

        $registros = $this->getDoctrine()->getRepository('AppBundle:Registros')->findAll();

        return $this->render('AdminBundle:Default:registros.html.twig', array('registros'=>$registros));
    }

    public function registroAction($idRegistro){

        $registro = $this->getDoctrine()->getRepository('AppBundle:Registros')->find($idRegistro);

        return $this->render('AdminBundle:Default:registro.html.twig', array('registro'=>$registro));
    }
}
