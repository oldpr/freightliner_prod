<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Codigos
 */
class Codigos
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var \AppBundle\Entity\Recalls
     */
    private $recall;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Codigos
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Codigos
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set recall
     *
     * @param \AppBundle\Entity\Recalls $recall
     * @return Codigos
     */
    public function setRecall(\AppBundle\Entity\Recalls $recall = null)
    {
        $this->recall = $recall;

        return $this;
    }

    /**
     * Get recall
     *
     * @return \AppBundle\Entity\Recalls 
     */
    public function getRecall()
    {
        return $this->recall;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $registros;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->registros = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add registros
     *
     * @param \AppBundle\Entity\Registros $registros
     * @return Codigos
     */
    public function addRegistro(\AppBundle\Entity\Registros $registros)
    {
        $this->registros[] = $registros;

        return $this;
    }

    /**
     * Remove registros
     *
     * @param \AppBundle\Entity\Registros $registros
     */
    public function removeRegistro(\AppBundle\Entity\Registros $registros)
    {
        $this->registros->removeElement($registros);
    }

    /**
     * Get registros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRegistros()
    {
        return $this->registros;
    }
}
