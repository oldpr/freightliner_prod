<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ciudades
 */
class Ciudades
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Ciudades
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Ciudades
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $concesionarios;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->concesionarios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add concesionarios
     *
     * @param \AppBundle\Entity\Concesionarios $concesionarios
     * @return Ciudades
     */
    public function addConcesionario(\AppBundle\Entity\Concesionarios $concesionarios)
    {
        $this->concesionarios[] = $concesionarios;

        return $this;
    }

    /**
     * Remove concesionarios
     *
     * @param \AppBundle\Entity\Concesionarios $concesionarios
     */
    public function removeConcesionario(\AppBundle\Entity\Concesionarios $concesionarios)
    {
        $this->concesionarios->removeElement($concesionarios);
    }

    /**
     * Get concesionarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConcesionarios()
    {
        return $this->concesionarios;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}
