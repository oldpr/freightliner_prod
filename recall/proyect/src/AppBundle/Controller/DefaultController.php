<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Registros;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller{

    function indexAction(){
        return $this->render('AppBundle:Default:index.html.twig');

    }

    function queEsCodigoVinAction(){
        return $this->render('AppBundle:Default:queEsCodigoVin.html.twig');

    }
    function buscarCodigoAction(Request $request){
        if ($request->request->get('codigo')) {
            $codigo = $request->request->get('codigo');
        }
        if (!isset($codigo) || $codigo == ''){
            return $this->render('AppBundle:Default:queEsCodigoVin.html.twig');
        }

        $listaCodigos = $this->getDoctrine()->getRepository('AppBundle:Codigos')->findBy(array('codigo'=>$codigo));
        if (isset($listaCodigos) && sizeof($listaCodigos)>0){

            $misRegistros = array();
            $cont = 0;
            foreach ($listaCodigos as $miCodigo){
                $entity = new Registros();
                $entity->setCodigo($miCodigo);
                $entity->setFecha(new DateTime(date('Y-m-d H:i:s')));
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $misRegistros[$cont]=$entity->getId();
                $cont ++;
            }

            return $this->render('AppBundle:Default:buscarCodigo.html.twig', array('codigos'=>$listaCodigos, 'codigo'=>$codigo, 'misRegistros' => $misRegistros));
        }

        return $this->render('AppBundle:Default:buscarCodigo.html.twig', array('codigo'=>$codigo));

    }

    function guardarRegistroAction(Request $request){
        if ($request->request->get('misRegistros')) {
            $misRegistros = $request->request->get('misRegistros');
        }
        if (!isset($misRegistros) || sizeof($misRegistros)<=0){
            return $this->redirectToRoute('homepage');
        }
        $nombres = $request->request->get('nombres');
        $apellidos = $request->request->get('apellidos');
        $correo = $request->request->get('correo');
        $telefono = $request->request->get('telefono');


        $registros = json_decode($misRegistros);
        if ($nombres !='' || $apellidos !='' || $correo !='' || $telefono !=''){
            foreach ($registros as $idRegistro){
                $em = $this->getDoctrine()->getManager();
                $registro = $em->getRepository('AppBundle:Registros')->find($idRegistro);
                $registro->setNombres($nombres);
                $registro->setApellidos($apellidos);
                $registro->setCorreo($correo);
                $registro->setTelefono($telefono);
                $em->flush();
            }
        }
        $codigo = $request->request->get('codigo');
        $miCodigo = $this->getDoctrine()->getRepository('AppBundle:Codigos')->findOneBy(array('codigo'=>$codigo));

        return $this->render('AppBundle:Default:datosEnviados.html.twig', array('codigo'=>$miCodigo));

    }

    function concesionariosAction(){
        $ciudades = $this->getDoctrine()->getManager()->getRepository('AppBundle:Ciudades')->findAll();
        return $this->render('AppBundle:Default:concesionarios.html.twig', array('ciudades'=> $ciudades));
    }
}
