<?php

/* AdminBundle:Default:registro.html.twig */
class __TwigTemplate_6e8fb8dc2ca117c5f31b93617712fe9789db1d8a909653a2c349d5a22ae8be38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:registro.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e851e577e30878a540b1a7f00687235901569ae735e53d0f3464fc9dfd6b114f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e851e577e30878a540b1a7f00687235901569ae735e53d0f3464fc9dfd6b114f->enter($__internal_e851e577e30878a540b1a7f00687235901569ae735e53d0f3464fc9dfd6b114f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Default:registro.html.twig"));

        // line 3
        $context["nomPagina"] = ((("VIN: " . $this->getAttribute($this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "codigo", array()), "codigo", array())) . " - fecha: ") . twig_date_format_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "fecha", array())));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e851e577e30878a540b1a7f00687235901569ae735e53d0f3464fc9dfd6b114f->leave($__internal_e851e577e30878a540b1a7f00687235901569ae735e53d0f3464fc9dfd6b114f_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_1b15ec6c4914da3924aebfbc8663f366aca06e82805c2a8b99129586caa561e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b15ec6c4914da3924aebfbc8663f366aca06e82805c2a8b99129586caa561e7->enter($__internal_1b15ec6c4914da3924aebfbc8663f366aca06e82805c2a8b99129586caa561e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_1b15ec6c4914da3924aebfbc8663f366aca06e82805c2a8b99129586caa561e7->leave($__internal_1b15ec6c4914da3924aebfbc8663f366aca06e82805c2a8b99129586caa561e7_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_7aee9ec1b5e15616cac33ca0b12ea0fe3ebecbdee9410d3b83a410eab6616862 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aee9ec1b5e15616cac33ca0b12ea0fe3ebecbdee9410d3b83a410eab6616862->enter($__internal_7aee9ec1b5e15616cac33ca0b12ea0fe3ebecbdee9410d3b83a410eab6616862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_registros");
        echo "\">Registros</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_7aee9ec1b5e15616cac33ca0b12ea0fe3ebecbdee9410d3b83a410eab6616862->leave($__internal_7aee9ec1b5e15616cac33ca0b12ea0fe3ebecbdee9410d3b83a410eab6616862_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_9bd0304ace98b0141f5c066dc67a6c76fd971142996511935d50c1da20da4ff2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9bd0304ace98b0141f5c066dc67a6c76fd971142996511935d50c1da20da4ff2->enter($__internal_9bd0304ace98b0141f5c066dc67a6c76fd971142996511935d50c1da20da4ff2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "
    ";
        // line 12
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 13
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 15
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 18
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Id</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "id", array()), "html", null, true);
        echo "</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Fecha</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 30
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "fecha", array())), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Nombres</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "nombres", array()), "html", null, true);
        echo "</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Apellidos</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "apellidos", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Correo</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "correo", array()), "html", null, true);
        echo "</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Telefono</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? $this->getContext($context, "registro")), "telefono", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


";
        
        $__internal_9bd0304ace98b0141f5c066dc67a6c76fd971142996511935d50c1da20da4ff2->leave($__internal_9bd0304ace98b0141f5c066dc67a6c76fd971142996511935d50c1da20da4ff2_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 52,  136 => 48,  126 => 41,  119 => 37,  109 => 30,  102 => 26,  92 => 18,  86 => 15,  82 => 13,  80 => 12,  77 => 11,  71 => 10,  62 => 7,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'VIN: '~registro.codigo.codigo~' - fecha: '~registro.fecha|date %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_registros') }}\">Registros</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> {{ error }}.
        </div>
    {% endif %}
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Id</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">{{ registro.id }}</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Fecha</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">{{ registro.fecha|date }}</p>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Nombres</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">{{ registro.nombres }}</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Apellidos</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">{{ registro.apellidos }}</p>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Correo</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">{{ registro.correo }}</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Telefono</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">{{ registro.telefono }}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


{% endblock %}", "AdminBundle:Default:registro.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/registro.html.twig");
    }
}
