<?php

/* AppBundle:Default:queEsCodigoVin.html.twig */
class __TwigTemplate_c3d9b7f0d013749f35679681e9ba81355cc8c978c4d3d5e7dde24adbb1a5f9a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:queEsCodigoVin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40a5697e029d66f8d233898b3f79d628299513bacff7f993338ecee2e34e43d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40a5697e029d66f8d233898b3f79d628299513bacff7f993338ecee2e34e43d5->enter($__internal_40a5697e029d66f8d233898b3f79d628299513bacff7f993338ecee2e34e43d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:queEsCodigoVin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_40a5697e029d66f8d233898b3f79d628299513bacff7f993338ecee2e34e43d5->leave($__internal_40a5697e029d66f8d233898b3f79d628299513bacff7f993338ecee2e34e43d5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6e0bfb8d5aa2be5dbb41c91bf4be833f330e1dd8b175109955a8008f05f773aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e0bfb8d5aa2be5dbb41c91bf4be833f330e1dd8b175109955a8008f05f773aa->enter($__internal_6e0bfb8d5aa2be5dbb41c91bf4be833f330e1dd8b175109955a8008f05f773aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3>¿Qué es un código VIN o FIN?</h3>
";
        
        $__internal_6e0bfb8d5aa2be5dbb41c91bf4be833f330e1dd8b175109955a8008f05f773aa->leave($__internal_6e0bfb8d5aa2be5dbb41c91bf4be833f330e1dd8b175109955a8008f05f773aa_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:queEsCodigoVin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h3>¿Qué es un código VIN o FIN?</h3>
{% endblock %}", "AppBundle:Default:queEsCodigoVin.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/queEsCodigoVin.html.twig");
    }
}
