<?php

/* base.html.twig */
class __TwigTemplate_ed0772c371af262ad4adc309b64005aa793504ea3ac8b803a33b2f77605f7496 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'metas' => array($this, 'block_metas'),
            'links' => array($this, 'block_links'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'modals' => array($this, 'block_modals'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_795a0b9a0bd3c0aba6108c28e5b351d38978c5b2a628583b8b4861b15ee8d062 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_795a0b9a0bd3c0aba6108c28e5b351d38978c5b2a628583b8b4861b15ee8d062->enter($__internal_795a0b9a0bd3c0aba6108c28e5b351d38978c5b2a628583b8b4861b15ee8d062_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"es\">
<head>
    ";
        // line 4
        $this->displayBlock('metas', $context, $blocks);
        // line 10
        echo "    ";
        $this->displayBlock('links', $context, $blocks);
        // line 16
        echo "    <link href=\"https://fonts.googleapis.com/css?family=Rajdhani:500,600,700\" rel=\"stylesheet\">
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"), "html", null, true);
        echo "\"></script>

    <title>Freightliner Colombia - Bienvenido al Sitio oficial</title>
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../img/favicon/favicon-16x16.png"), "html", null, true);
        echo "\">

</head>
";
        // line 23
        if (array_key_exists("classBody", $context)) {
            // line 24
            echo "<body class=\"";
            echo twig_escape_filter($this->env, ($context["classBody"] ?? $this->getContext($context, "classBody")), "html", null, true);
            echo "\">
";
        } else {
            // line 26
            echo "<body>
";
        }
        // line 28
        echo "<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-47958253-1\"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-47958253-1');
</script>
<!--[if lt IE 8]>
<p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
";
        // line 40
        $this->displayBlock('header', $context, $blocks);
        // line 183
        $this->displayBlock('content', $context, $blocks);
        // line 184
        $this->displayBlock('footer', $context, $blocks);
        // line 231
        $this->displayBlock('modals', $context, $blocks);
        // line 232
        $this->displayBlock('javascripts', $context, $blocks);
        // line 241
        echo "<script type=\"text/javascript\">
    var correoNewsletter = \$('#correoNewsletter');
    var submitNewsletter = \$('#submitNewsletter');
    var error = \"Ha habido un error al procesar la solicitud<br>por favor vuelva a intentarlo\";
    \$('form#newsletter').submit(function(event) {
        var correoN = correoNewsletter.val();
        var labelMsjNewsletter = \$('#labelMsjNewsletter');
        submitNewsletter.val('ENVIANDO...');
        \$.ajax({
            type        : 'POST',
            url         : './libs/addNewsletter.php',
            data        : {correo:correoN},
            success: function(respose){
                console.log(respose);
                respose = JSON.parse(respose);
                if (respose.code==200) {
                    correoNewsletter.val(\"\");
                    submitNewsletter.val('SUSCRITO');
                    labelMsjNewsletter.html('');
                }else{
                    console.log(respose.error);
                    labelMsjNewsletter.html(respose.msj);
                    submitNewsletter.val('SUSCRIBIRME');
                }
                labelMsjNewsletter.show(250);
            },
            error: function (response) {
                console.log(response);
                labelMsjNewsletter.html(error);
                labelMsjNewsletter.show(250);
                submitNewsletter.val('SUSCRIBIRME');
            }

        });
        event.preventDefault();
    });
</script>
</body>
</html>";
        
        $__internal_795a0b9a0bd3c0aba6108c28e5b351d38978c5b2a628583b8b4861b15ee8d062->leave($__internal_795a0b9a0bd3c0aba6108c28e5b351d38978c5b2a628583b8b4861b15ee8d062_prof);

    }

    // line 4
    public function block_metas($context, array $blocks = array())
    {
        $__internal_5d96eff44e05f67d7ef8e9ef08cceb7bc27a1f9ca0f055a102ddd28a53797f46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d96eff44e05f67d7ef8e9ef08cceb7bc27a1f9ca0f055a102ddd28a53797f46->enter($__internal_5d96eff44e05f67d7ef8e9ef08cceb7bc27a1f9ca0f055a102ddd28a53797f46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "metas"));

        // line 5
        echo "        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    ";
        
        $__internal_5d96eff44e05f67d7ef8e9ef08cceb7bc27a1f9ca0f055a102ddd28a53797f46->leave($__internal_5d96eff44e05f67d7ef8e9ef08cceb7bc27a1f9ca0f055a102ddd28a53797f46_prof);

    }

    // line 10
    public function block_links($context, array $blocks = array())
    {
        $__internal_d8d2d9407d95686703d230f1e463e3f79d7486025417a16f2bce4e38f3b8c5d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8d2d9407d95686703d230f1e463e3f79d7486025417a16f2bce4e38f3b8c5d2->enter($__internal_d8d2d9407d95686703d230f1e463e3f79d7486025417a16f2bce4e38f3b8c5d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "links"));

        // line 11
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../css/bootstrap-theme.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../css/main.css"), "html", null, true);
        echo "\">

    ";
        
        $__internal_d8d2d9407d95686703d230f1e463e3f79d7486025417a16f2bce4e38f3b8c5d2->leave($__internal_d8d2d9407d95686703d230f1e463e3f79d7486025417a16f2bce4e38f3b8c5d2_prof);

    }

    // line 40
    public function block_header($context, array $blocks = array())
    {
        $__internal_6d8ead9244c84a8ac481f839d5d70cf5cd74904993f97af2fbebcb44c8ff5eca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d8ead9244c84a8ac481f839d5d70cf5cd74904993f97af2fbebcb44c8ff5eca->enter($__internal_6d8ead9244c84a8ac481f839d5d70cf5cd74904993f97af2fbebcb44c8ff5eca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 41
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"contenedor\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"freightliner\" href=\"http://freightliner.com.co/index.php\"></a>
                <div class=\"info-header hidden-xs\">
                    <p>Servicio al cliente: <b>+57 1 742 6850</b></p>
                    <!-- <a href=\"mailto:info@freightliner.com.co\">info@freightliner.com.co</a> -->
                </div>
                <a class=\"visible-xs btn-productos\">Productos</a>
            </div>
            <div id=\"navbar\" class=\"navbar-collapse collapse\">
                <ul>
                    ";
        // line 59
        if ( !array_key_exists("active", $context)) {
            // line 60
            echo "                        ";
            $context["active"] = 0;
            // line 61
            echo "                    ";
        }
        // line 62
        echo "                    <li><a href=\"http://freightliner.com.co/index.php\" class=\"";
        if ((($context["active"] ?? $this->getContext($context, "active")) == 1)) {
            echo "active";
        }
        echo "\">Inicio</a></li>
                    <li class=\"expandible\"><a class=\"";
        // line 63
        if ((($context["active"] ?? $this->getContext($context, "active")) == 2)) {
            echo "active";
        }
        echo "\" id=\"productos\">Productos</a></li>
                    <li><a href=\"http://freightliner.com.co/posventa.php\" class=\"";
        // line 64
        if ((($context["active"] ?? $this->getContext($context, "active")) == 3)) {
            echo "active";
        }
        echo "\">Posventa</a></li>
                    <li><a href=\"http://freightliner.com.co/organizacion.php\" class=\"";
        // line 65
        if ((($context["active"] ?? $this->getContext($context, "active")) == 4)) {
            echo "active";
        }
        echo "\">¿por qué freightliner?</a></li>
                    <li><a href=\"http://freightliner.com.co/timeline.php\" class=\"";
        // line 66
        if ((($context["active"] ?? $this->getContext($context, "active")) == 5)) {
            echo "active";
        }
        echo "\">75 años de innovación</a></li>

                    <li><a href=\"http://freightliner.com.co/concesionarios.php\" class=\"";
        // line 68
        if ((($context["active"] ?? $this->getContext($context, "active")) == 8)) {
            echo "active";
        }
        echo "\">Concesionarios</a></li>
                    <li><a href=\"http://freightliner.com.co/noticias.php\" class=\"";
        // line 69
        if ((($context["active"] ?? $this->getContext($context, "active")) == 6)) {
            echo "active";
        }
        echo "\">Noticias</a></li>
                    <!-- <li><a href=\"http://freightliner.com.co/\" class=\"";
        // line 70
        if ((($context["active"] ?? $this->getContext($context, "active")) == 7)) {
            echo "active";
        }
        echo "\">Recall</a></li> -->
                    <li><a href=\"http://freightliner.com.co/contacto.php\" class=\"";
        // line 71
        if ((($context["active"] ?? $this->getContext($context, "active")) == 9)) {
            echo "active";
        }
        echo "\">Contacto</a></li>
                </ul>
                <div class=\"redes\">
                    <a class=\"facebook\" target=\"_blank\" href=\"https://www.facebook.com/FreightlinerCol/\"></a>
                    <a class=\"instagram\" target=\"_blank\" href=\"https://www.instagram.com/freightlinercol/\"></a>
                    <!-- <a class=\"youtube\" href=\"http://freightliner.com.co/\"></a> -->
                </div>
            </div><!--/.navbar-collapse -->
        </div>
        <div class=\"menu-expandido\">
            <ul class=\"tipo\">

                <li>
                    <a style=\"background-image: url(../img/opc-carga.jpg);\"><p>Movimiento en carretera</p></a>
                    <ul class=\"opciones\">
                        <li>
                            <a>CASCADIA</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-Cascadia.jpg);\"></div>
                                <p class=\"det-nombre\">CASCADIA</p>
                                <p class=\"det-motor\">Detroit DD15</span></p>
                                <p class=\"det-potencia\">475HP</span></p>
                                <p class=\"det-precio\"><span>\$</span>323.200.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/cascadia-dd15.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/cascadia.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 MINIMULA</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-Minimula.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 MINIMULA</p>
                                <p class=\"det-pbv\">5.321<span>kg</span></p>
                                <p class=\"det-combinado\">40.500kg</p>
                                <p class=\"det-precio\"><span>\$</span>297.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-minimula.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m2106-minimula.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 4x2 LARGO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Camion-4X2.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 4x2 LARGO</p>
                                <p class=\"det-pbv\">5.620<span>kg</span></p>
                                <p class=\"det-distancia\">6.121mm</p>
                                <p class=\"det-precio\"><span>\$</span>283.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-4x2-largo.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m21064x2-camion-largo.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 6x4 LARGO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Camion-6X4.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 6x4 LARGO</p>
                                <p class=\"det-pbv\">7.661<span>kg</span></p>
                                <p class=\"det-distancia\">6.400mm</p>
                                <p class=\"det-precio\"><span>\$</span>330.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-6x4-largo.php\">Conozca más</a>
                                    <a target=\"_blank\"  href=\"http://freightliner.com.co/pdf/m2106-6x4-camion-largo.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>

                <li>
                    <a style=\"background-image: url(../img/opc-construccion.jpg);\"><p>Movimiento en todo terreno</p></a>
                    <ul class=\"opciones\">
                        <li>
                            <a>M2 106 VOLCO 6x4</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Volco.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 VOLCO 6x4</p>
                                <p class=\"det-vol-vac\">9.504<span>kg</span></p>
                                <p class=\"det-capacidad\">14mts<span>3</span></p>
                                <p class=\"det-precio\"><span>\$</span>266.800.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2-volco.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m2106-6X4-volco.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 112 VOLCO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-112-Volco.jpg);\"></div>
                                <p class=\"det-nombre\">M2 112 VOLCO</p>
                                <p class=\"det-motor\">MBE 4000</span></p>
                                <p class=\"det-potencia\">350HP</span></p>
                                <p class=\"det-precio-volco-hoper\"><span>\$</span>296.500.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2-112-volco.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/M2112-volco.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div class=\"overlay\"></div>
";
        
        $__internal_6d8ead9244c84a8ac481f839d5d70cf5cd74904993f97af2fbebcb44c8ff5eca->leave($__internal_6d8ead9244c84a8ac481f839d5d70cf5cd74904993f97af2fbebcb44c8ff5eca_prof);

    }

    // line 183
    public function block_content($context, array $blocks = array())
    {
        $__internal_85c628f5224687757c800dc5fc9d285b4ee9a47f4588cf60101798cf04fe47b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85c628f5224687757c800dc5fc9d285b4ee9a47f4588cf60101798cf04fe47b9->enter($__internal_85c628f5224687757c800dc5fc9d285b4ee9a47f4588cf60101798cf04fe47b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_85c628f5224687757c800dc5fc9d285b4ee9a47f4588cf60101798cf04fe47b9->leave($__internal_85c628f5224687757c800dc5fc9d285b4ee9a47f4588cf60101798cf04fe47b9_prof);

    }

    // line 184
    public function block_footer($context, array $blocks = array())
    {
        $__internal_2aae51a563c69a996715f6c63395ad02007adf4bc6729f7b37bfb45b45589f61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2aae51a563c69a996715f6c63395ad02007adf4bc6729f7b37bfb45b45589f61->enter($__internal_2aae51a563c69a996715f6c63395ad02007adf4bc6729f7b37bfb45b45589f61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 185
        echo "    <footer>
        <div class=\"content\">
            <div class=\"col-md-8\">
                <a class=\"freightliner\" href=\"http://freightliner.com.co/index.php\"></a>
                <div class=\"boletin\">
                    <h3>Boletín virtual</h3>
                    <p>Suscríbase y entérese de las últimas noticias, novedades y ofertas de la marca.</p>
                    <form id=\"newsletter\">
                        <input type=\"email\" name=\"mail-boletin\" id=\"correoNewsletter\" placeholder=\"Escriba su correo electrónico\" required>
                        <input class=\"boton\" id=\"submitNewsletter\" type=\"submit\" value=\"suscribirme\">
                        <p id=\"labelMsjNewsletter\" class=\"msj-form\"></p>
                    </form>
                </div>
            </div>
            <div class=\"col-md-4\">
                <ul>
                    <li><a href=\"http://freightliner.com.co/index.php\">Inicio</a></li>
                    <li><a href=\"http://freightliner.com.co/organizacion.php\">Por qué Freightliner</a></li>
                    <li><a href=\"http://freightliner.com.co/posventa.php\">Posventa</a></li>
                    <li><a href=\"http://freightliner.com.co/noticias.php\">Noticias</a></li>
                    <li><a target=\"_blank\" href=\"http://daimler.com.co/freightliner.php\">Garantía</a></li>

                </ul>
                <ul>
                    <!-- <li><a href=\"http://freightliner.com.co/\">Sala de prensa</a></li> -->
                    <li><a href=\"http://freightliner.com.co/recall.php\">Campaña de seguridad</a></li>
                    <li><a href=\"http://freightliner.com.co/concesionarios.php\">Concesionarios</a></li>
                    <li><a href=\"http://freightliner.com.co/contacto.php\">Contacto</a></li>
                    <li><a href=\"http://freightliner.com.co/terminos-condiciones.php\">Términos y condiciones</a></li>
                </ul>
                <div class=\"redes\">
                    <a class=\"facebook\" target=\"_blank\" href=\"https://www.facebook.com/FreightlinerCol/\"></a>
                    <a class=\"instagram\" target=\"_blank\" href=\"https://www.instagram.com/freightlinercol/\"></a>
                    <!-- <a class=\"youtube\" href=\"http://freightliner.com.co/\"></a> -->
                </div>
            </div>
        </div>

        <hr>

        <div class=\"content legales\">
            <a class=\"daimler\" target=\"_blank\" href=\"http://daimler.com.co/\"></a>
            <p>Todos los derechos reservados - Daimler Colombia S.A. 2017 - <a href=\"http://freightliner.com.co/pdf/terminos-y-condiciones.pdf\" target=\"_blank\">Términos y condiciones</a></p>
        </div>
    </footer>
";
        
        $__internal_2aae51a563c69a996715f6c63395ad02007adf4bc6729f7b37bfb45b45589f61->leave($__internal_2aae51a563c69a996715f6c63395ad02007adf4bc6729f7b37bfb45b45589f61_prof);

    }

    // line 231
    public function block_modals($context, array $blocks = array())
    {
        $__internal_902af673165757effd925dd1fab1ce8365e7fde7d0352d7139e468b4790d2753 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_902af673165757effd925dd1fab1ce8365e7fde7d0352d7139e468b4790d2753->enter($__internal_902af673165757effd925dd1fab1ce8365e7fde7d0352d7139e468b4790d2753_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modals"));

        
        $__internal_902af673165757effd925dd1fab1ce8365e7fde7d0352d7139e468b4790d2753->leave($__internal_902af673165757effd925dd1fab1ce8365e7fde7d0352d7139e468b4790d2753_prof);

    }

    // line 232
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_6bda70d6b6429e2e0190b615888cbc8923e7accc9573c325951be3e6ea90769b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bda70d6b6429e2e0190b615888cbc8923e7accc9573c325951be3e6ea90769b->enter($__internal_6bda70d6b6429e2e0190b615888cbc8923e7accc9573c325951be3e6ea90769b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 233
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
    <script>window.jQuery || document.write('<script src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/vendor/jquery-1.11.2.min.js"), "html", null, true);
        echo "\"><\\/script>')</script>

    <script src=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/vendor/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 238
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/main.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_6bda70d6b6429e2e0190b615888cbc8923e7accc9573c325951be3e6ea90769b->leave($__internal_6bda70d6b6429e2e0190b615888cbc8923e7accc9573c325951be3e6ea90769b_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 238,  476 => 236,  471 => 234,  468 => 233,  462 => 232,  451 => 231,  399 => 185,  393 => 184,  382 => 183,  261 => 71,  255 => 70,  249 => 69,  243 => 68,  236 => 66,  230 => 65,  224 => 64,  218 => 63,  211 => 62,  208 => 61,  205 => 60,  203 => 59,  183 => 41,  177 => 40,  167 => 13,  163 => 12,  158 => 11,  152 => 10,  141 => 5,  135 => 4,  90 => 241,  88 => 232,  86 => 231,  84 => 184,  82 => 183,  80 => 40,  66 => 28,  62 => 26,  56 => 24,  54 => 23,  48 => 20,  42 => 17,  39 => 16,  36 => 10,  34 => 4,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!doctype html>
<html lang=\"es\">
<head>
    {% block metas %}
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    {% endblock %}
    {% block links %}
        <link rel=\"stylesheet\" href=\"{{ asset('../css/bootstrap.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{ asset('../css/bootstrap-theme.min.css')}}\">
        <link rel=\"stylesheet\" href=\"{{ asset('../css/main.css')}}\">

    {% endblock %}
    <link href=\"https://fonts.googleapis.com/css?family=Rajdhani:500,600,700\" rel=\"stylesheet\">
    <script src=\"{{ asset('../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}\"></script>

    <title>Freightliner Colombia - Bienvenido al Sitio oficial</title>
    <link rel=\"icon\" type=\"image/png\" href=\"{{ asset('../img/favicon/favicon-16x16.png')}}\">

</head>
{% if classBody is defined %}
<body class=\"{{ classBody }}\">
{% else %}
<body>
{% endif %}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-47958253-1\"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-47958253-1');
</script>
<!--[if lt IE 8]>
<p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
{% block header %}
    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"contenedor\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"freightliner\" href=\"http://freightliner.com.co/index.php\"></a>
                <div class=\"info-header hidden-xs\">
                    <p>Servicio al cliente: <b>+57 1 742 6850</b></p>
                    <!-- <a href=\"mailto:info@freightliner.com.co\">info@freightliner.com.co</a> -->
                </div>
                <a class=\"visible-xs btn-productos\">Productos</a>
            </div>
            <div id=\"navbar\" class=\"navbar-collapse collapse\">
                <ul>
                    {% if active is not defined %}
                        {% set active = 0 %}
                    {% endif %}
                    <li><a href=\"http://freightliner.com.co/index.php\" class=\"{% if active == 1 %}active{% endif %}\">Inicio</a></li>
                    <li class=\"expandible\"><a class=\"{% if active == 2 %}active{% endif %}\" id=\"productos\">Productos</a></li>
                    <li><a href=\"http://freightliner.com.co/posventa.php\" class=\"{% if active == 3 %}active{% endif %}\">Posventa</a></li>
                    <li><a href=\"http://freightliner.com.co/organizacion.php\" class=\"{% if active == 4 %}active{% endif %}\">¿por qué freightliner?</a></li>
                    <li><a href=\"http://freightliner.com.co/timeline.php\" class=\"{% if active == 5 %}active{% endif %}\">75 años de innovación</a></li>

                    <li><a href=\"http://freightliner.com.co/concesionarios.php\" class=\"{% if active == 8 %}active{% endif %}\">Concesionarios</a></li>
                    <li><a href=\"http://freightliner.com.co/noticias.php\" class=\"{% if active == 6 %}active{% endif %}\">Noticias</a></li>
                    <!-- <li><a href=\"http://freightliner.com.co/\" class=\"{% if active == 7 %}active{% endif %}\">Recall</a></li> -->
                    <li><a href=\"http://freightliner.com.co/contacto.php\" class=\"{% if active == 9 %}active{% endif %}\">Contacto</a></li>
                </ul>
                <div class=\"redes\">
                    <a class=\"facebook\" target=\"_blank\" href=\"https://www.facebook.com/FreightlinerCol/\"></a>
                    <a class=\"instagram\" target=\"_blank\" href=\"https://www.instagram.com/freightlinercol/\"></a>
                    <!-- <a class=\"youtube\" href=\"http://freightliner.com.co/\"></a> -->
                </div>
            </div><!--/.navbar-collapse -->
        </div>
        <div class=\"menu-expandido\">
            <ul class=\"tipo\">

                <li>
                    <a style=\"background-image: url(../img/opc-carga.jpg);\"><p>Movimiento en carretera</p></a>
                    <ul class=\"opciones\">
                        <li>
                            <a>CASCADIA</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-Cascadia.jpg);\"></div>
                                <p class=\"det-nombre\">CASCADIA</p>
                                <p class=\"det-motor\">Detroit DD15</span></p>
                                <p class=\"det-potencia\">475HP</span></p>
                                <p class=\"det-precio\"><span>\$</span>323.200.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/cascadia-dd15.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/cascadia.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 MINIMULA</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-Minimula.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 MINIMULA</p>
                                <p class=\"det-pbv\">5.321<span>kg</span></p>
                                <p class=\"det-combinado\">40.500kg</p>
                                <p class=\"det-precio\"><span>\$</span>297.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-minimula.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m2106-minimula.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 4x2 LARGO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Camion-4X2.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 4x2 LARGO</p>
                                <p class=\"det-pbv\">5.620<span>kg</span></p>
                                <p class=\"det-distancia\">6.121mm</p>
                                <p class=\"det-precio\"><span>\$</span>283.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-4x2-largo.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m21064x2-camion-largo.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 6x4 LARGO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Camion-6X4.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 6x4 LARGO</p>
                                <p class=\"det-pbv\">7.661<span>kg</span></p>
                                <p class=\"det-distancia\">6.400mm</p>
                                <p class=\"det-precio\"><span>\$</span>330.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-6x4-largo.php\">Conozca más</a>
                                    <a target=\"_blank\"  href=\"http://freightliner.com.co/pdf/m2106-6x4-camion-largo.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>

                <li>
                    <a style=\"background-image: url(../img/opc-construccion.jpg);\"><p>Movimiento en todo terreno</p></a>
                    <ul class=\"opciones\">
                        <li>
                            <a>M2 106 VOLCO 6x4</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Volco.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 VOLCO 6x4</p>
                                <p class=\"det-vol-vac\">9.504<span>kg</span></p>
                                <p class=\"det-capacidad\">14mts<span>3</span></p>
                                <p class=\"det-precio\"><span>\$</span>266.800.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2-volco.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m2106-6X4-volco.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 112 VOLCO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-112-Volco.jpg);\"></div>
                                <p class=\"det-nombre\">M2 112 VOLCO</p>
                                <p class=\"det-motor\">MBE 4000</span></p>
                                <p class=\"det-potencia\">350HP</span></p>
                                <p class=\"det-precio-volco-hoper\"><span>\$</span>296.500.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2-112-volco.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/M2112-volco.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div class=\"overlay\"></div>
{% endblock %}
{% block content %}{% endblock %}
{% block footer %}
    <footer>
        <div class=\"content\">
            <div class=\"col-md-8\">
                <a class=\"freightliner\" href=\"http://freightliner.com.co/index.php\"></a>
                <div class=\"boletin\">
                    <h3>Boletín virtual</h3>
                    <p>Suscríbase y entérese de las últimas noticias, novedades y ofertas de la marca.</p>
                    <form id=\"newsletter\">
                        <input type=\"email\" name=\"mail-boletin\" id=\"correoNewsletter\" placeholder=\"Escriba su correo electrónico\" required>
                        <input class=\"boton\" id=\"submitNewsletter\" type=\"submit\" value=\"suscribirme\">
                        <p id=\"labelMsjNewsletter\" class=\"msj-form\"></p>
                    </form>
                </div>
            </div>
            <div class=\"col-md-4\">
                <ul>
                    <li><a href=\"http://freightliner.com.co/index.php\">Inicio</a></li>
                    <li><a href=\"http://freightliner.com.co/organizacion.php\">Por qué Freightliner</a></li>
                    <li><a href=\"http://freightliner.com.co/posventa.php\">Posventa</a></li>
                    <li><a href=\"http://freightliner.com.co/noticias.php\">Noticias</a></li>
                    <li><a target=\"_blank\" href=\"http://daimler.com.co/freightliner.php\">Garantía</a></li>

                </ul>
                <ul>
                    <!-- <li><a href=\"http://freightliner.com.co/\">Sala de prensa</a></li> -->
                    <li><a href=\"http://freightliner.com.co/recall.php\">Campaña de seguridad</a></li>
                    <li><a href=\"http://freightliner.com.co/concesionarios.php\">Concesionarios</a></li>
                    <li><a href=\"http://freightliner.com.co/contacto.php\">Contacto</a></li>
                    <li><a href=\"http://freightliner.com.co/terminos-condiciones.php\">Términos y condiciones</a></li>
                </ul>
                <div class=\"redes\">
                    <a class=\"facebook\" target=\"_blank\" href=\"https://www.facebook.com/FreightlinerCol/\"></a>
                    <a class=\"instagram\" target=\"_blank\" href=\"https://www.instagram.com/freightlinercol/\"></a>
                    <!-- <a class=\"youtube\" href=\"http://freightliner.com.co/\"></a> -->
                </div>
            </div>
        </div>

        <hr>

        <div class=\"content legales\">
            <a class=\"daimler\" target=\"_blank\" href=\"http://daimler.com.co/\"></a>
            <p>Todos los derechos reservados - Daimler Colombia S.A. 2017 - <a href=\"http://freightliner.com.co/pdf/terminos-y-condiciones.pdf\" target=\"_blank\">Términos y condiciones</a></p>
        </div>
    </footer>
{% endblock %}
{% block modals %}{% endblock %}
{% block javascripts %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
    <script>window.jQuery || document.write('<script src=\"{{ asset('../js/vendor/jquery-1.11.2.min.js') }}\"><\\/script>')</script>

    <script src=\"{{ asset('../js/vendor/bootstrap.min.js')}}\"></script>
    {#<script src=\"{{ asset('../js/jquery.mobile.custom.js')}}\"></script>#}
    <script src=\"{{ asset('../js/main.js')}}\"></script>

{% endblock %}
<script type=\"text/javascript\">
    var correoNewsletter = \$('#correoNewsletter');
    var submitNewsletter = \$('#submitNewsletter');
    var error = \"Ha habido un error al procesar la solicitud<br>por favor vuelva a intentarlo\";
    \$('form#newsletter').submit(function(event) {
        var correoN = correoNewsletter.val();
        var labelMsjNewsletter = \$('#labelMsjNewsletter');
        submitNewsletter.val('ENVIANDO...');
        \$.ajax({
            type        : 'POST',
            url         : './libs/addNewsletter.php',
            data        : {correo:correoN},
            success: function(respose){
                console.log(respose);
                respose = JSON.parse(respose);
                if (respose.code==200) {
                    correoNewsletter.val(\"\");
                    submitNewsletter.val('SUSCRITO');
                    labelMsjNewsletter.html('');
                }else{
                    console.log(respose.error);
                    labelMsjNewsletter.html(respose.msj);
                    submitNewsletter.val('SUSCRIBIRME');
                }
                labelMsjNewsletter.show(250);
            },
            error: function (response) {
                console.log(response);
                labelMsjNewsletter.html(error);
                labelMsjNewsletter.show(250);
                submitNewsletter.val('SUSCRIBIRME');
            }

        });
        event.preventDefault();
    });
</script>
</body>
</html>", "base.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/base.html.twig");
    }
}
