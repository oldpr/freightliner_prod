<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_5e4e9a83b2ea77068cabb368dc10c6d6e8b8439be00d1ec205cbf2aa672e29e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ccb4a40f75606a8a005d951cddc2a101cc011fc0b7fb4396d40fd09877be9ff9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ccb4a40f75606a8a005d951cddc2a101cc011fc0b7fb4396d40fd09877be9ff9->enter($__internal_ccb4a40f75606a8a005d951cddc2a101cc011fc0b7fb4396d40fd09877be9ff9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ccb4a40f75606a8a005d951cddc2a101cc011fc0b7fb4396d40fd09877be9ff9->leave($__internal_ccb4a40f75606a8a005d951cddc2a101cc011fc0b7fb4396d40fd09877be9ff9_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_338aa1c1b486711fee52f994d695f33a2d7d2cbfda3bb5b94dd9daac86ac0862 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_338aa1c1b486711fee52f994d695f33a2d7d2cbfda3bb5b94dd9daac86ac0862->enter($__internal_338aa1c1b486711fee52f994d695f33a2d7d2cbfda3bb5b94dd9daac86ac0862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_338aa1c1b486711fee52f994d695f33a2d7d2cbfda3bb5b94dd9daac86ac0862->leave($__internal_338aa1c1b486711fee52f994d695f33a2d7d2cbfda3bb5b94dd9daac86ac0862_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_88c4fbcc3b8c465dd4b0970276c47815a6fdbd63defa5dc92f09b86ae345a9e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88c4fbcc3b8c465dd4b0970276c47815a6fdbd63defa5dc92f09b86ae345a9e0->enter($__internal_88c4fbcc3b8c465dd4b0970276c47815a6fdbd63defa5dc92f09b86ae345a9e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_88c4fbcc3b8c465dd4b0970276c47815a6fdbd63defa5dc92f09b86ae345a9e0->leave($__internal_88c4fbcc3b8c465dd4b0970276c47815a6fdbd63defa5dc92f09b86ae345a9e0_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_50b6f65a83752543bd731a61d4aa02058e4254d5b863f544706815fd4b2eb8d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50b6f65a83752543bd731a61d4aa02058e4254d5b863f544706815fd4b2eb8d3->enter($__internal_50b6f65a83752543bd731a61d4aa02058e4254d5b863f544706815fd4b2eb8d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_50b6f65a83752543bd731a61d4aa02058e4254d5b863f544706815fd4b2eb8d3->leave($__internal_50b6f65a83752543bd731a61d4aa02058e4254d5b863f544706815fd4b2eb8d3_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
