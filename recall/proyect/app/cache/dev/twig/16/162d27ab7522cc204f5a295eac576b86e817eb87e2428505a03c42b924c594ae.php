<?php

/* :concesionarios:show.html.twig */
class __TwigTemplate_a632b9f2486673741aaf49885cf19b7f554826d19ae0a89dd5d333d60c6d6fd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":concesionarios:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_27acdee1cffca85d374d21784b47d80ea51cd4e77f8375a592d2c28b45f8170c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27acdee1cffca85d374d21784b47d80ea51cd4e77f8375a592d2c28b45f8170c->enter($__internal_27acdee1cffca85d374d21784b47d80ea51cd4e77f8375a592d2c28b45f8170c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":concesionarios:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_27acdee1cffca85d374d21784b47d80ea51cd4e77f8375a592d2c28b45f8170c->leave($__internal_27acdee1cffca85d374d21784b47d80ea51cd4e77f8375a592d2c28b45f8170c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ffbbc471d4fb9968847552754530e80051c132ac1952e636b46b929c3d68e4a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ffbbc471d4fb9968847552754530e80051c132ac1952e636b46b929c3d68e4a1->enter($__internal_ffbbc471d4fb9968847552754530e80051c132ac1952e636b46b929c3d68e4a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Concesionario</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "nombre", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "descripcion", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_edit", array("id" => $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 31
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_ffbbc471d4fb9968847552754530e80051c132ac1952e636b46b929c3d68e4a1->leave($__internal_ffbbc471d4fb9968847552754530e80051c132ac1952e636b46b929c3d68e4a1_prof);

    }

    public function getTemplateName()
    {
        return ":concesionarios:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 33,  84 => 31,  78 => 28,  72 => 25,  62 => 18,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Concesionario</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ concesionario.id }}</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>{{ concesionario.nombre }}</td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td>{{ concesionario.descripcion }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_concesionarios_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_concesionarios_edit', { 'id': concesionario.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":concesionarios:show.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/concesionarios/show.html.twig");
    }
}
