<?php

/* AppBundle:Default:buscarCodigo.html.twig */
class __TwigTemplate_8b48e00fb0d625df06ae49fa1f6d3707bc057fd3c2fedea3b2d3c647850304b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:buscarCodigo.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_69b0d0f65d8b2017ad40777518c8bb741f9421b5f6d9189666db2473b46539ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69b0d0f65d8b2017ad40777518c8bb741f9421b5f6d9189666db2473b46539ff->enter($__internal_69b0d0f65d8b2017ad40777518c8bb741f9421b5f6d9189666db2473b46539ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:buscarCodigo.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_69b0d0f65d8b2017ad40777518c8bb741f9421b5f6d9189666db2473b46539ff->leave($__internal_69b0d0f65d8b2017ad40777518c8bb741f9421b5f6d9189666db2473b46539ff_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_75070598b17e7bb8bff49e5050d0f52061aacc9370ba9dc872547a24864760ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75070598b17e7bb8bff49e5050d0f52061aacc9370ba9dc872547a24864760ab->enter($__internal_75070598b17e7bb8bff49e5050d0f52061aacc9370ba9dc872547a24864760ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>
    ";
        // line 7
        if (array_key_exists("codigos", $context)) {
            // line 8
            echo "
        <div class=\"content\">
            <div class=\"form-contact\">
                <h2>EL CÓDIGO APLICA</h2>
                <p>El código <span>";
            // line 12
            echo twig_escape_filter($this->env, ($context["codigo"] ?? $this->getContext($context, "codigo")), "html", null, true);
            echo "</span>, aplica para la campaña de seguridad <span>";
            echo twig_replace_filter($this->getAttribute($this->getAttribute($this->getAttribute(($context["codigos"] ?? $this->getContext($context, "codigos")), 0, array(), "array"), "recall", array()), "descripcion", array()), array("<p>" => "", "</p>" => ""));
            echo "</span>, ingrese sus datos de contacto para obtener más información.</p>
                <form action=\"";
            // line 13
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("guardarRegistro");
            echo "\" method=\"post\">
                    <input type=\"hidden\" name=\"codigo\" value=\"";
            // line 14
            echo twig_escape_filter($this->env, ($context["codigo"] ?? $this->getContext($context, "codigo")), "html", null, true);
            echo "\">
                    <input type=\"text\" name=\"nombres\" placeholder=\"nombres\" required>
                    <input type=\"text\" name=\"apellidos\" placeholder=\"apellidos\" required>
                    <input type=\"text\" name=\"correo\" placeholder=\"correo electrónico\" required>
                    <input type=\"text\" name=\"telefono\" placeholder=\"teléfono\" required>
                    <input type=\"hidden\" name=\"misRegistros\" value=\"";
            // line 19
            echo twig_escape_filter($this->env, twig_jsonencode_filter(($context["misRegistros"] ?? $this->getContext($context, "misRegistros"))), "html", null, true);
            echo "\" >
                    <input type=\"submit\" value=\"Ver información\" class=\"boton\">
                </form>
            </div>

        </div>

    ";
        } else {
            // line 27
            echo "        <div class=\"content\">
            <div class=\"form-contact\">
                <h2>LLAMADOS DE SEGURIDAD</h2>
                <p>El código <span>";
            // line 30
            echo twig_escape_filter($this->env, ($context["codigo"] ?? $this->getContext($context, "codigo")), "html", null, true);
            echo "</span>, no aplica para alguna de las campañas de seguridad vigentes de Freightliner Colombia Rectifique el código o intente nuevamente.</p>
                <form action=\"";
            // line 31
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("buscarCodigo");
            echo "\" method=\"post\" class=\"two-buttons\">
                    <input class=\"red\" type=\"text\" name=\"codigo\" placeholder=\"Ingrese su número VIN o FIN\">
                    <input type=\"submit\" value=\"volver a buscar\" class=\"boton\">
                    <a href=\"http://freightliner.com.co\" class=\"boton boton-medio boton-negro\">IR AL SITIO WEB</a>
                    <a class=\"que-es\" role=\"button\" data-toggle=\"popover\" data-trigger=\"focus\" title=\"Dismissible popover\" data-content=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"><b>¿Qué es el código VIN o FIN?</b></a>
                </form>
            </div>
        </div>
    ";
        }
        
        $__internal_75070598b17e7bb8bff49e5050d0f52061aacc9370ba9dc872547a24864760ab->leave($__internal_75070598b17e7bb8bff49e5050d0f52061aacc9370ba9dc872547a24864760ab_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:buscarCodigo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 31,  87 => 30,  82 => 27,  71 => 19,  63 => 14,  59 => 13,  53 => 12,  47 => 8,  45 => 7,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block content %}
    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>
    {% if codigos is defined %}

        <div class=\"content\">
            <div class=\"form-contact\">
                <h2>EL CÓDIGO APLICA</h2>
                <p>El código <span>{{codigo}}</span>, aplica para la campaña de seguridad <span>{{codigos[0].recall.descripcion|replace({'<p>':'','</p>':''})|raw}}</span>, ingrese sus datos de contacto para obtener más información.</p>
                <form action=\"{{ path('guardarRegistro') }}\" method=\"post\">
                    <input type=\"hidden\" name=\"codigo\" value=\"{{ codigo }}\">
                    <input type=\"text\" name=\"nombres\" placeholder=\"nombres\" required>
                    <input type=\"text\" name=\"apellidos\" placeholder=\"apellidos\" required>
                    <input type=\"text\" name=\"correo\" placeholder=\"correo electrónico\" required>
                    <input type=\"text\" name=\"telefono\" placeholder=\"teléfono\" required>
                    <input type=\"hidden\" name=\"misRegistros\" value=\"{{ misRegistros|json_encode() }}\" >
                    <input type=\"submit\" value=\"Ver información\" class=\"boton\">
                </form>
            </div>

        </div>

    {% else %}
        <div class=\"content\">
            <div class=\"form-contact\">
                <h2>LLAMADOS DE SEGURIDAD</h2>
                <p>El código <span>{{codigo}}</span>, no aplica para alguna de las campañas de seguridad vigentes de Freightliner Colombia Rectifique el código o intente nuevamente.</p>
                <form action=\"{{ path('buscarCodigo') }}\" method=\"post\" class=\"two-buttons\">
                    <input class=\"red\" type=\"text\" name=\"codigo\" placeholder=\"Ingrese su número VIN o FIN\">
                    <input type=\"submit\" value=\"volver a buscar\" class=\"boton\">
                    <a href=\"http://freightliner.com.co\" class=\"boton boton-medio boton-negro\">IR AL SITIO WEB</a>
                    <a class=\"que-es\" role=\"button\" data-toggle=\"popover\" data-trigger=\"focus\" title=\"Dismissible popover\" data-content=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"><b>¿Qué es el código VIN o FIN?</b></a>
                </form>
            </div>
        </div>
    {% endif %}
{% endblock %}", "AppBundle:Default:buscarCodigo.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/buscarCodigo.html.twig");
    }
}
