<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_778aaa9a660e02fd4d953204048384d54a6951f38c19096ee649fbd657ba04fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c78009ca160bea27588e035cb3c96a06e4b19359f62e0d22e6183597f3c0b0d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c78009ca160bea27588e035cb3c96a06e4b19359f62e0d22e6183597f3c0b0d3->enter($__internal_c78009ca160bea27588e035cb3c96a06e4b19359f62e0d22e6183597f3c0b0d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_c78009ca160bea27588e035cb3c96a06e4b19359f62e0d22e6183597f3c0b0d3->leave($__internal_c78009ca160bea27588e035cb3c96a06e4b19359f62e0d22e6183597f3c0b0d3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
