<?php

/* :recalls:show.html.twig */
class __TwigTemplate_b16f22e50cca64da5a6deec8334f142b0270d1ec802b35194057cf694a82f666 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":recalls:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d60889fe8670f0c2dc8d78c626ac8d6ab810542294d9722996f86061832fd6e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d60889fe8670f0c2dc8d78c626ac8d6ab810542294d9722996f86061832fd6e1->enter($__internal_d60889fe8670f0c2dc8d78c626ac8d6ab810542294d9722996f86061832fd6e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":recalls:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d60889fe8670f0c2dc8d78c626ac8d6ab810542294d9722996f86061832fd6e1->leave($__internal_d60889fe8670f0c2dc8d78c626ac8d6ab810542294d9722996f86061832fd6e1_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_edaf13b0a9f656c93dabc748ef7c0df65b89cbb462ca052a7b0f7cb95823c0ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edaf13b0a9f656c93dabc748ef7c0df65b89cbb462ca052a7b0f7cb95823c0ad->enter($__internal_edaf13b0a9f656c93dabc748ef7c0df65b89cbb462ca052a7b0f7cb95823c0ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Recall</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Codigo</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "codigo", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "nombre", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "descripcion", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Imagen</th>
                <td><img src=\"";
        // line 26
        echo twig_escape_filter($this->env, ($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/images/") . $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "imagen", array())), "html", null, true);
        echo "\" alt=\"Imagen\"></td>
            </tr>
            <tr>
                <th>Fecha</th>
                <td>";
        // line 30
        if ($this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "fecha", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "fecha", array()), "Y-m-d"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Importancia</th>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "importancia", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Informacion</th>
                <td>";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "informacion", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_edit", array("id" => $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 51
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 53
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_edaf13b0a9f656c93dabc748ef7c0df65b89cbb462ca052a7b0f7cb95823c0ad->leave($__internal_edaf13b0a9f656c93dabc748ef7c0df65b89cbb462ca052a7b0f7cb95823c0ad_prof);

    }

    public function getTemplateName()
    {
        return ":recalls:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 53,  121 => 51,  115 => 48,  109 => 45,  99 => 38,  92 => 34,  83 => 30,  76 => 26,  69 => 22,  62 => 18,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Recall</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ recall.id }}</td>
            </tr>
            <tr>
                <th>Codigo</th>
                <td>{{ recall.codigo }}</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>{{ recall.nombre }}</td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td>{{ recall.descripcion }}</td>
            </tr>
            <tr>
                <th>Imagen</th>
                <td><img src=\"{{ asset('uploads/images/')~recall.imagen }}\" alt=\"Imagen\"></td>
            </tr>
            <tr>
                <th>Fecha</th>
                <td>{% if recall.fecha %}{{ recall.fecha|date('Y-m-d') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Importancia</th>
                <td>{{ recall.importancia }}</td>
            </tr>
            <tr>
                <th>Informacion</th>
                <td>{{ recall.informacion }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_recalls_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_recalls_edit', { 'id': recall.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":recalls:show.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/recalls/show.html.twig");
    }
}
