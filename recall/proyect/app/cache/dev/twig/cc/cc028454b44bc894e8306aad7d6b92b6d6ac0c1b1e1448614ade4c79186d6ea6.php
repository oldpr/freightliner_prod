<?php

/* SensioDistributionBundle::Configurator/layout.html.twig */
class __TwigTemplate_82834ef5798dcd44df523936e95880792909c28a63da04062c43eeb74329b231 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle::Configurator/layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f43fa27008b403e4b79ec999c20c647ca43f44970393b608007f7a3881273f74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f43fa27008b403e4b79ec999c20c647ca43f44970393b608007f7a3881273f74->enter($__internal_f43fa27008b403e4b79ec999c20c647ca43f44970393b608007f7a3881273f74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle::Configurator/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f43fa27008b403e4b79ec999c20c647ca43f44970393b608007f7a3881273f74->leave($__internal_f43fa27008b403e4b79ec999c20c647ca43f44970393b608007f7a3881273f74_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_31da37a990aaf8261537219910b42bf425428d645ee94b2c279d4eec47165c0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31da37a990aaf8261537219910b42bf425428d645ee94b2c279d4eec47165c0c->enter($__internal_31da37a990aaf8261537219910b42bf425428d645ee94b2c279d4eec47165c0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_31da37a990aaf8261537219910b42bf425428d645ee94b2c279d4eec47165c0c->leave($__internal_31da37a990aaf8261537219910b42bf425428d645ee94b2c279d4eec47165c0c_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_93af20d08d751b8e260bd6edfdfd4479679beffde831c5e02b61733a6e3f8d88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93af20d08d751b8e260bd6edfdfd4479679beffde831c5e02b61733a6e3f8d88->enter($__internal_93af20d08d751b8e260bd6edfdfd4479679beffde831c5e02b61733a6e3f8d88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_93af20d08d751b8e260bd6edfdfd4479679beffde831c5e02b61733a6e3f8d88->leave($__internal_93af20d08d751b8e260bd6edfdfd4479679beffde831c5e02b61733a6e3f8d88_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_5c37063ff1ed948f0fe88951f7dc3b831a33e6fc0d2638343c83cc261d0c0101 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c37063ff1ed948f0fe88951f7dc3b831a33e6fc0d2638343c83cc261d0c0101->enter($__internal_5c37063ff1ed948f0fe88951f7dc3b831a33e6fc0d2638343c83cc261d0c0101_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, ($context["version"] ?? $this->getContext($context, "version")), "html", null, true);
        echo "</div>
";
        
        $__internal_5c37063ff1ed948f0fe88951f7dc3b831a33e6fc0d2638343c83cc261d0c0101->leave($__internal_5c37063ff1ed948f0fe88951f7dc3b831a33e6fc0d2638343c83cc261d0c0101_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_3cd78333c95ebd5ab2fc0f050195c553cf2f2444c545e30bd12c858988363a2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cd78333c95ebd5ab2fc0f050195c553cf2f2444c545e30bd12c858988363a2c->enter($__internal_3cd78333c95ebd5ab2fc0f050195c553cf2f2444c545e30bd12c858988363a2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_3cd78333c95ebd5ab2fc0f050195c553cf2f2444c545e30bd12c858988363a2c->leave($__internal_3cd78333c95ebd5ab2fc0f050195c553cf2f2444c545e30bd12c858988363a2c_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle::Configurator/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"TwigBundle::layout.html.twig\" %}

{% block head %}
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}\" />
{% endblock %}

{% block title 'Web Configurator Bundle' %}

{% block body %}
    <div class=\"block\">
        {% block content %}{% endblock %}
    </div>
    <div class=\"version\">Symfony Standard Edition v.{{ version }}</div>
{% endblock %}
", "SensioDistributionBundle::Configurator/layout.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/views/Configurator/layout.html.twig");
    }
}
