<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_3671eea0cc27a16e7a4d80831423f4355ef5b85c368d08b84c19b5c54ce3547a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb802ddaaaec2ce98403da00c24fbd4b0447cf2c2dbc499a130cb56bc58ffa6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb802ddaaaec2ce98403da00c24fbd4b0447cf2c2dbc499a130cb56bc58ffa6a->enter($__internal_cb802ddaaaec2ce98403da00c24fbd4b0447cf2c2dbc499a130cb56bc58ffa6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_cb802ddaaaec2ce98403da00c24fbd4b0447cf2c2dbc499a130cb56bc58ffa6a->leave($__internal_cb802ddaaaec2ce98403da00c24fbd4b0447cf2c2dbc499a130cb56bc58ffa6a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
