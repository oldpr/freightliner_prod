<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_fdd4d5c521f62ae6d237d88c036373627b33abb093512ae78c954254be644134 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca19e587a7eb65dc7b2a6926ab1574ab8cbd4e0174e93c877228c010106b8ed4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca19e587a7eb65dc7b2a6926ab1574ab8cbd4e0174e93c877228c010106b8ed4->enter($__internal_ca19e587a7eb65dc7b2a6926ab1574ab8cbd4e0174e93c877228c010106b8ed4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_ca19e587a7eb65dc7b2a6926ab1574ab8cbd4e0174e93c877228c010106b8ed4->leave($__internal_ca19e587a7eb65dc7b2a6926ab1574ab8cbd4e0174e93c877228c010106b8ed4_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_06d4393cd35543449960572ce2f9162e8a95e085dd2de80d2f8e83e75d6ed4f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06d4393cd35543449960572ce2f9162e8a95e085dd2de80d2f8e83e75d6ed4f3->enter($__internal_06d4393cd35543449960572ce2f9162e8a95e085dd2de80d2f8e83e75d6ed4f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_06d4393cd35543449960572ce2f9162e8a95e085dd2de80d2f8e83e75d6ed4f3->leave($__internal_06d4393cd35543449960572ce2f9162e8a95e085dd2de80d2f8e83e75d6ed4f3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
