<?php

/* :recalls:new.html.twig */
class __TwigTemplate_7478a7217e573e46aaec5c8689bc3cf3ae776ca6a92a8946bca86c58ff92d1f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":recalls:new.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c849aba7ea3c093f51ff17e19bb3f860f3a1903dcded304a677bf99352fd7562 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c849aba7ea3c093f51ff17e19bb3f860f3a1903dcded304a677bf99352fd7562->enter($__internal_c849aba7ea3c093f51ff17e19bb3f860f3a1903dcded304a677bf99352fd7562_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":recalls:new.html.twig"));

        // line 3
        $context["nomPagina"] = "Recall Nuevo";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c849aba7ea3c093f51ff17e19bb3f860f3a1903dcded304a677bf99352fd7562->leave($__internal_c849aba7ea3c093f51ff17e19bb3f860f3a1903dcded304a677bf99352fd7562_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_3920e1b28cbf235715f5c922d811e04d02eea80a67e551dcd91ac2e43f9705c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3920e1b28cbf235715f5c922d811e04d02eea80a67e551dcd91ac2e43f9705c4->enter($__internal_3920e1b28cbf235715f5c922d811e04d02eea80a67e551dcd91ac2e43f9705c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_3920e1b28cbf235715f5c922d811e04d02eea80a67e551dcd91ac2e43f9705c4->leave($__internal_3920e1b28cbf235715f5c922d811e04d02eea80a67e551dcd91ac2e43f9705c4_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_77fa9f8087bb5431c0b940d00e2a0854e735a654c3cc684b3399a3cfce814916 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77fa9f8087bb5431c0b940d00e2a0854e735a654c3cc684b3399a3cfce814916->enter($__internal_77fa9f8087bb5431c0b940d00e2a0854e735a654c3cc684b3399a3cfce814916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Recalls</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_77fa9f8087bb5431c0b940d00e2a0854e735a654c3cc684b3399a3cfce814916->leave($__internal_77fa9f8087bb5431c0b940d00e2a0854e735a654c3cc684b3399a3cfce814916_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_110ff11183ab23d1c112f0f604583577ae1b1819ada5eadfa31dfecb0517b637 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_110ff11183ab23d1c112f0f604583577ae1b1819ada5eadfa31dfecb0517b637->enter($__internal_110ff11183ab23d1c112f0f604583577ae1b1819ada5eadfa31dfecb0517b637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    ";
        // line 18
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form")));
        echo "

                    ";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "imagen", array()), 'row');
        echo "

                    ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget', array("attr" => array("class" => "form-group")));
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    ";
        // line 28
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_110ff11183ab23d1c112f0f604583577ae1b1819ada5eadfa31dfecb0517b637->leave($__internal_110ff11183ab23d1c112f0f604583577ae1b1819ada5eadfa31dfecb0517b637_prof);

    }

    public function getTemplateName()
    {
        return ":recalls:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 28,  96 => 22,  91 => 20,  86 => 18,  77 => 11,  71 => 10,  62 => 7,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Recall Nuevo' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_recalls_index') }}\">Recalls</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    {{ form_start(form, { 'attr': {'class': 'form'} }) }}

                    {{ form_row(form.imagen) }}

                    {{ form_widget(form, {'attr': {'class': 'form-group'}}) }}
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    {{ form_end(form) }}

                </div>
            </div>
        </div>
    </div>
{% endblock %}
", ":recalls:new.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/recalls/new.html.twig");
    }
}
