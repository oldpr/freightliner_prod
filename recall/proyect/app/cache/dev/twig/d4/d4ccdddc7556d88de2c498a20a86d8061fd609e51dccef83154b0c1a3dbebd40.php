<?php

/* AdminBundle:Default:index.html.twig */
class __TwigTemplate_a9d0f8628a9b6840f76f404c441b2908bd1e2e06f9e3be7da4cfea4cb50719be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a39ba05731a797a1c3c06d18aaea56fe6e3ace3a523f4d91c210593a70869f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a39ba05731a797a1c3c06d18aaea56fe6e3ace3a523f4d91c210593a70869f4->enter($__internal_7a39ba05731a797a1c3c06d18aaea56fe6e3ace3a523f4d91c210593a70869f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7a39ba05731a797a1c3c06d18aaea56fe6e3ace3a523f4d91c210593a70869f4->leave($__internal_7a39ba05731a797a1c3c06d18aaea56fe6e3ace3a523f4d91c210593a70869f4_prof);

    }

    // line 3
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_8f522833da384f4330baee08a0f1b41d6a5ac18408490dbfcc6aab19c4a8310b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f522833da384f4330baee08a0f1b41d6a5ac18408490dbfcc6aab19c4a8310b->enter($__internal_8f522833da384f4330baee08a0f1b41d6a5ac18408490dbfcc6aab19c4a8310b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        // line 4
        echo "    Inicio
";
        
        $__internal_8f522833da384f4330baee08a0f1b41d6a5ac18408490dbfcc6aab19c4a8310b->leave($__internal_8f522833da384f4330baee08a0f1b41d6a5ac18408490dbfcc6aab19c4a8310b_prof);

    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        $__internal_e06fed6bedbcee0f22544d138966d445218f0ac5799819872e698ba52406694d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e06fed6bedbcee0f22544d138966d445218f0ac5799819872e698ba52406694d->enter($__internal_e06fed6bedbcee0f22544d138966d445218f0ac5799819872e698ba52406694d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 8
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-6 col-lg-3\">
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">
                        <div class=\"mini-stat clearfix bx-shadow bg-white\">
                            <span class=\"mini-stat-icon bg-info\"><i class=\"md md-web\"></i></span>
                            <div class=\"mini-stat-info text-right text-dark\">
                                <span class=\"text-dark\">Recall</span>
                            </div>
                        </div>
                    </a>
            </div>

        </div>
    </div> <!-- container -->
";
        
        $__internal_e06fed6bedbcee0f22544d138966d445218f0ac5799819872e698ba52406694d->leave($__internal_e06fed6bedbcee0f22544d138966d445218f0ac5799819872e698ba52406694d_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 11,  55 => 8,  49 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}

{% block pagina %}
    Inicio
{% endblock %}

{% block content %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-6 col-lg-3\">
                    <a href=\"{{ path ('admin_recalls_index') }}\">
                        <div class=\"mini-stat clearfix bx-shadow bg-white\">
                            <span class=\"mini-stat-icon bg-info\"><i class=\"md md-web\"></i></span>
                            <div class=\"mini-stat-info text-right text-dark\">
                                <span class=\"text-dark\">Recall</span>
                            </div>
                        </div>
                    </a>
            </div>

        </div>
    </div> <!-- container -->
{% endblock %}", "AdminBundle:Default:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/index.html.twig");
    }
}
