<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_991821cada5da5510ee5671362fc878f4cfc5467dc5851090dfdbf3a4129ac52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4bd1ac329edb72f239cb7d4c676dfbe17e99e4e2fa7838ac9842f302b551594 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4bd1ac329edb72f239cb7d4c676dfbe17e99e4e2fa7838ac9842f302b551594->enter($__internal_f4bd1ac329edb72f239cb7d4c676dfbe17e99e4e2fa7838ac9842f302b551594_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_f4bd1ac329edb72f239cb7d4c676dfbe17e99e4e2fa7838ac9842f302b551594->leave($__internal_f4bd1ac329edb72f239cb7d4c676dfbe17e99e4e2fa7838ac9842f302b551594_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
