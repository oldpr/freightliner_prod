<?php

/* :concesionarios:new.html.twig */
class __TwigTemplate_84d9b1bce758365574766e47841e231c3bba504081426e1c8949d0361e22064d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":concesionarios:new.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1dee234cc4c66be58b87adf54eb85c44105bc70abfc582722685269a769d0482 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dee234cc4c66be58b87adf54eb85c44105bc70abfc582722685269a769d0482->enter($__internal_1dee234cc4c66be58b87adf54eb85c44105bc70abfc582722685269a769d0482_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":concesionarios:new.html.twig"));

        // line 3
        $context["nomPagina"] = "Nuevo Concesionario";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1dee234cc4c66be58b87adf54eb85c44105bc70abfc582722685269a769d0482->leave($__internal_1dee234cc4c66be58b87adf54eb85c44105bc70abfc582722685269a769d0482_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_8f0ebc3e342124517e82098ea9f3c6c0b9d948b0e98e3b40df2147a1dab5eaa0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f0ebc3e342124517e82098ea9f3c6c0b9d948b0e98e3b40df2147a1dab5eaa0->enter($__internal_8f0ebc3e342124517e82098ea9f3c6c0b9d948b0e98e3b40df2147a1dab5eaa0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_8f0ebc3e342124517e82098ea9f3c6c0b9d948b0e98e3b40df2147a1dab5eaa0->leave($__internal_8f0ebc3e342124517e82098ea9f3c6c0b9d948b0e98e3b40df2147a1dab5eaa0_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_e010857ca6cb02680919bda9e8cbf5d05c0a8afedefc09f178380b9e516ea09b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e010857ca6cb02680919bda9e8cbf5d05c0a8afedefc09f178380b9e516ea09b->enter($__internal_e010857ca6cb02680919bda9e8cbf5d05c0a8afedefc09f178380b9e516ea09b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_index");
        echo "\">Ciudades</a></li>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_edit", array("id" => $this->getAttribute($this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "ciudad", array()), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "ciudad", array()), "html", null, true);
        echo "</a></li>
    <li class=\"active\">";
        // line 8
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_e010857ca6cb02680919bda9e8cbf5d05c0a8afedefc09f178380b9e516ea09b->leave($__internal_e010857ca6cb02680919bda9e8cbf5d05c0a8afedefc09f178380b9e516ea09b_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_827a648781d469d13242e845de20ae0ef84012a757010d43ad37bff49908ec64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_827a648781d469d13242e845de20ae0ef84012a757010d43ad37bff49908ec64->enter($__internal_827a648781d469d13242e845de20ae0ef84012a757010d43ad37bff49908ec64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    ";
        // line 19
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
                    ";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    ";
        // line 26
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_827a648781d469d13242e845de20ae0ef84012a757010d43ad37bff49908ec64->leave($__internal_827a648781d469d13242e845de20ae0ef84012a757010d43ad37bff49908ec64_prof);

    }

    public function getTemplateName()
    {
        return ":concesionarios:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 26,  96 => 20,  92 => 19,  83 => 12,  77 => 11,  68 => 8,  62 => 7,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Nuevo Concesionario' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_ciudades_index') }}\">Ciudades</a></li>
    <li><a href=\"{{ path('admin_ciudades_edit',{'id':concesionario.ciudad.id}) }}\">{{ concesionario.ciudad }}</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    {{ form_start(form) }}
                    {{ form_widget(form) }}
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    {{ form_end(form) }}

                </div>
            </div>
        </div>
    </div>

{% endblock %}
", ":concesionarios:new.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/concesionarios/new.html.twig");
    }
}
