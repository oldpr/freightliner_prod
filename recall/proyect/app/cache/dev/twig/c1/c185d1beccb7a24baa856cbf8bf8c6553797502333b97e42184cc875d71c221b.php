<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_d3574d5ce0629d1ece1b7eb0260d39842a1f8508c85824c79880728704823c43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b0c3364fc2a42b4cbe625d6d1164b2a962e6df00c1b70db9539f198240aca8f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0c3364fc2a42b4cbe625d6d1164b2a962e6df00c1b70db9539f198240aca8f4->enter($__internal_b0c3364fc2a42b4cbe625d6d1164b2a962e6df00c1b70db9539f198240aca8f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_b0c3364fc2a42b4cbe625d6d1164b2a962e6df00c1b70db9539f198240aca8f4->leave($__internal_b0c3364fc2a42b4cbe625d6d1164b2a962e6df00c1b70db9539f198240aca8f4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
