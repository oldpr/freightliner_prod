<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_9bd6dafde69e02ace645dafbd025fd86de061c123dd0ca1d276361ca3637569c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_adab75fab6f418edbcc651a56fa84c43f4b5282e801381fa42c11dbfce5e7c4e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adab75fab6f418edbcc651a56fa84c43f4b5282e801381fa42c11dbfce5e7c4e->enter($__internal_adab75fab6f418edbcc651a56fa84c43f4b5282e801381fa42c11dbfce5e7c4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_adab75fab6f418edbcc651a56fa84c43f4b5282e801381fa42c11dbfce5e7c4e->leave($__internal_adab75fab6f418edbcc651a56fa84c43f4b5282e801381fa42c11dbfce5e7c4e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
