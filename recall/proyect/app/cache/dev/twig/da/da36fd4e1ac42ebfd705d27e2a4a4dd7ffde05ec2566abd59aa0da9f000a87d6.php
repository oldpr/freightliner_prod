<?php

/* :ciudades:edit.html.twig */
class __TwigTemplate_e11da547050b8c9415684e33345bce71a3a08e0a2311728576ee08f85f4f0680 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":ciudades:edit.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45527a08fb325c16742f9f51ea046f500eba3efab8f3dcfa4e5d16537b72df98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45527a08fb325c16742f9f51ea046f500eba3efab8f3dcfa4e5d16537b72df98->enter($__internal_45527a08fb325c16742f9f51ea046f500eba3efab8f3dcfa4e5d16537b72df98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":ciudades:edit.html.twig"));

        // line 3
        $context["nomPagina"] = $this->getAttribute(($context["ciudad"] ?? $this->getContext($context, "ciudad")), "nombre", array());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_45527a08fb325c16742f9f51ea046f500eba3efab8f3dcfa4e5d16537b72df98->leave($__internal_45527a08fb325c16742f9f51ea046f500eba3efab8f3dcfa4e5d16537b72df98_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_98c947fa269bdd01b45da7dc9b41e6de13f4a251cc44279e1dc942c58095974a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98c947fa269bdd01b45da7dc9b41e6de13f4a251cc44279e1dc942c58095974a->enter($__internal_98c947fa269bdd01b45da7dc9b41e6de13f4a251cc44279e1dc942c58095974a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_98c947fa269bdd01b45da7dc9b41e6de13f4a251cc44279e1dc942c58095974a->leave($__internal_98c947fa269bdd01b45da7dc9b41e6de13f4a251cc44279e1dc942c58095974a_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_0223fc8d18f2062aca7ef09f89a505d80e151a048958e0a8cc9abda6ce61d2d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0223fc8d18f2062aca7ef09f89a505d80e151a048958e0a8cc9abda6ce61d2d4->enter($__internal_0223fc8d18f2062aca7ef09f89a505d80e151a048958e0a8cc9abda6ce61d2d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_index");
        echo "\">Ciudades</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_0223fc8d18f2062aca7ef09f89a505d80e151a048958e0a8cc9abda6ce61d2d4->leave($__internal_0223fc8d18f2062aca7ef09f89a505d80e151a048958e0a8cc9abda6ce61d2d4_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_96db3632909c038415811ad9d30616ace813e7d36eab57c9b1143a131055e82e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96db3632909c038415811ad9d30616ace813e7d36eab57c9b1143a131055e82e->enter($__internal_96db3632909c038415811ad9d30616ace813e7d36eab57c9b1143a131055e82e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "
    ";
        // line 12
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array(), "any", false, true), "get", array(0 => "error"), "method", true, true)) {
            // line 13
            echo "        ";
            $context["error"] = $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "error"), "method");
            // line 14
            echo "    ";
        }
        // line 15
        echo "    ";
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 16
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 18
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 21
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    ";
        // line 25
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start', array("attr" => array("class" => "")));
        echo "
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"submit\" class=\"btn btn-danger waves-effect waves-light pull-right\">Eliminar</button>
                    </h3>
                    ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
                </div>
                <div class=\"panel-body\">


                    ";
        // line 34
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "

                    ";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    ";
        // line 42
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Concesionarios <a href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_new", array("id" => $this->getAttribute(($context["ciudad"] ?? $this->getContext($context, "ciudad")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>
                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 68
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["ciudad"] ?? $this->getContext($context, "ciudad")), "concesionarios", array()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["entidad"]) {
            // line 69
            echo "                                    <tr>
                                        <td>";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "id", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_edit", array("id" => $this->getAttribute($context["entidad"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "nombre", array()), "html", null, true);
            echo "</a></td>
                                    </tr>
                                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 74
            echo "                                    <div class=\"alert alert-info\">
                                        Esta ciudad aún no tiene registrado concesionarios.
                                    </div>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_96db3632909c038415811ad9d30616ace813e7d36eab57c9b1143a131055e82e->leave($__internal_96db3632909c038415811ad9d30616ace813e7d36eab57c9b1143a131055e82e_prof);

    }

    public function getTemplateName()
    {
        return ":ciudades:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 78,  190 => 74,  180 => 71,  176 => 70,  173 => 69,  168 => 68,  150 => 53,  136 => 42,  127 => 36,  122 => 34,  114 => 29,  107 => 25,  101 => 21,  95 => 18,  91 => 16,  88 => 15,  85 => 14,  82 => 13,  80 => 12,  77 => 11,  71 => 10,  62 => 7,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = ciudad.nombre %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_ciudades_index') }}\">Ciudades</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    {% if app.request.get('error') is defined %}
        {% set error = app.request.get('error') %}
    {% endif %}
    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> {{ error }}.
        </div>
    {% endif %}
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    {{ form_start(delete_form, { 'attr': {'class': ''} }) }}
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"submit\" class=\"btn btn-danger waves-effect waves-light pull-right\">Eliminar</button>
                    </h3>
                    {{ form_end(delete_form) }}
                </div>
                <div class=\"panel-body\">


                    {{ form_start(edit_form) }}

                    {{ form_widget(edit_form) }}
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    {{ form_end(edit_form) }}

                </div>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Concesionarios <a href=\"{{ path('admin_concesionarios_new', {'id':ciudad.id}) }}\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>
                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                </tr>
                                </thead>


                                <tbody>
                                {% for entidad in ciudad.concesionarios %}
                                    <tr>
                                        <td>{{ entidad.id }}</td>
                                        <td><a href=\"{{ path('admin_concesionarios_edit', {'id':entidad.id}) }}\">{{ entidad.nombre }}</a></td>
                                    </tr>
                                {% else %}
                                    <div class=\"alert alert-info\">
                                        Esta ciudad aún no tiene registrado concesionarios.
                                    </div>
                                {% endfor %}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", ":ciudades:edit.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/ciudades/edit.html.twig");
    }
}
