<?php

/* :ciudades:index.html.twig */
class __TwigTemplate_ee8375f004e5056af583dec7549f55ccf1bb6a12b5b0b446c0043b3bd7911a14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":ciudades:index.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1d1fbc6afcad75728b8f8e62d9f1db5a850de2b3991e77a107c6363cd2951cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1d1fbc6afcad75728b8f8e62d9f1db5a850de2b3991e77a107c6363cd2951cb->enter($__internal_a1d1fbc6afcad75728b8f8e62d9f1db5a850de2b3991e77a107c6363cd2951cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":ciudades:index.html.twig"));

        // line 3
        $context["nomPagina"] = "Ciudades";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a1d1fbc6afcad75728b8f8e62d9f1db5a850de2b3991e77a107c6363cd2951cb->leave($__internal_a1d1fbc6afcad75728b8f8e62d9f1db5a850de2b3991e77a107c6363cd2951cb_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_16acb4360316b4d15e24dcbb74a17d774aed4d7ae283d1fede5274096a372906 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16acb4360316b4d15e24dcbb74a17d774aed4d7ae283d1fede5274096a372906->enter($__internal_16acb4360316b4d15e24dcbb74a17d774aed4d7ae283d1fede5274096a372906_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_16acb4360316b4d15e24dcbb74a17d774aed4d7ae283d1fede5274096a372906->leave($__internal_16acb4360316b4d15e24dcbb74a17d774aed4d7ae283d1fede5274096a372906_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_74d0361e09491118cc14018eda4701db0dd3e1d1eebcba81897239a6929722ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74d0361e09491118cc14018eda4701db0dd3e1d1eebcba81897239a6929722ff->enter($__internal_74d0361e09491118cc14018eda4701db0dd3e1d1eebcba81897239a6929722ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li class=\"active\">";
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_74d0361e09491118cc14018eda4701db0dd3e1d1eebcba81897239a6929722ff->leave($__internal_74d0361e09491118cc14018eda4701db0dd3e1d1eebcba81897239a6929722ff_prof);

    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        $__internal_727270c7fd20a4080fd13fb8d05c61d6d7152dc5bcaecffe49cf95459d33c170 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_727270c7fd20a4080fd13fb8d05c61d6d7152dc5bcaecffe49cf95459d33c170->enter($__internal_727270c7fd20a4080fd13fb8d05c61d6d7152dc5bcaecffe49cf95459d33c170_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 10
        echo "
    <!-- container -->
    ";
        // line 12
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 13
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: ";
            // line 15
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 18
        echo "
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Lista <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_new");
        echo "\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ciudades"] ?? $this->getContext($context, "ciudades")));
        foreach ($context['_seq'] as $context["_key"] => $context["entidad"]) {
            // line 40
            echo "                                    <tr>
                                        <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "id", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_edit", array("id" => $this->getAttribute($context["entidad"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "nombre", array()), "html", null, true);
            echo "</a></td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_727270c7fd20a4080fd13fb8d05c61d6d7152dc5bcaecffe49cf95459d33c170->leave($__internal_727270c7fd20a4080fd13fb8d05c61d6d7152dc5bcaecffe49cf95459d33c170_prof);

    }

    public function getTemplateName()
    {
        return ":ciudades:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 45,  126 => 42,  122 => 41,  119 => 40,  115 => 39,  96 => 23,  89 => 18,  83 => 15,  79 => 13,  77 => 12,  73 => 10,  67 => 9,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Ciudades' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    <!-- container -->
    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: {{ error }}.
        </div>
    {% endif %}

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Lista <a href=\"{{ path('admin_ciudades_new')}}\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                </tr>
                                </thead>


                                <tbody>
                                {% for entidad in ciudades %}
                                    <tr>
                                        <td>{{ entidad.id }}</td>
                                        <td><a href=\"{{ path('admin_ciudades_edit', { 'id': entidad.id }) }}\">{{ entidad.nombre }}</a></td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", ":ciudades:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/ciudades/index.html.twig");
    }
}
