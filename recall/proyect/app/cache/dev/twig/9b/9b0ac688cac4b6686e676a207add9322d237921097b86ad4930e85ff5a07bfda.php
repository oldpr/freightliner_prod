<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_9d6e0e43a80ac6232f0ae59b4c1cde1aec076d484bd466a6295e5d19d72d20df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d810b8e4617f7465ba23e3dba37c6aa397ef145f7aed2bf3949542392a1b6496 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d810b8e4617f7465ba23e3dba37c6aa397ef145f7aed2bf3949542392a1b6496->enter($__internal_d810b8e4617f7465ba23e3dba37c6aa397ef145f7aed2bf3949542392a1b6496_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_d810b8e4617f7465ba23e3dba37c6aa397ef145f7aed2bf3949542392a1b6496->leave($__internal_d810b8e4617f7465ba23e3dba37c6aa397ef145f7aed2bf3949542392a1b6496_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
