<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_9c485fb873d0e3dff742e3ca31c8b9336ed4887b8a2057b1aa2be2ce79906200 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c714f70bd1bd215148a5fc6b7d47dc5a2d1d51441eaeb5d93c2c257c892c765 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c714f70bd1bd215148a5fc6b7d47dc5a2d1d51441eaeb5d93c2c257c892c765->enter($__internal_3c714f70bd1bd215148a5fc6b7d47dc5a2d1d51441eaeb5d93c2c257c892c765_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_3c714f70bd1bd215148a5fc6b7d47dc5a2d1d51441eaeb5d93c2c257c892c765->leave($__internal_3c714f70bd1bd215148a5fc6b7d47dc5a2d1d51441eaeb5d93c2c257c892c765_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
