<?php

/* :recalls:edit.html.twig */
class __TwigTemplate_ab30f7847e563f2e271e6008b00e7d19035307c98dddd5cb2251b4918ce29dd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":recalls:edit.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6dc71b9598c6cc5bb74e2c7d765a7ecd4c568482c77ce0759c171c96e37753f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6dc71b9598c6cc5bb74e2c7d765a7ecd4c568482c77ce0759c171c96e37753f6->enter($__internal_6dc71b9598c6cc5bb74e2c7d765a7ecd4c568482c77ce0759c171c96e37753f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":recalls:edit.html.twig"));

        // line 3
        $context["nomPagina"] = $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "nombre", array());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6dc71b9598c6cc5bb74e2c7d765a7ecd4c568482c77ce0759c171c96e37753f6->leave($__internal_6dc71b9598c6cc5bb74e2c7d765a7ecd4c568482c77ce0759c171c96e37753f6_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_7b7459370c1976adeebf40f2179918dbe5737264abe9651ce64149241f91523c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b7459370c1976adeebf40f2179918dbe5737264abe9651ce64149241f91523c->enter($__internal_7b7459370c1976adeebf40f2179918dbe5737264abe9651ce64149241f91523c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_7b7459370c1976adeebf40f2179918dbe5737264abe9651ce64149241f91523c->leave($__internal_7b7459370c1976adeebf40f2179918dbe5737264abe9651ce64149241f91523c_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_ef3307ba3b9d979ca11f9abdde2a09ce90b637a444550b307c7acaa57ad5aab3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef3307ba3b9d979ca11f9abdde2a09ce90b637a444550b307c7acaa57ad5aab3->enter($__internal_ef3307ba3b9d979ca11f9abdde2a09ce90b637a444550b307c7acaa57ad5aab3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Recalls</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_ef3307ba3b9d979ca11f9abdde2a09ce90b637a444550b307c7acaa57ad5aab3->leave($__internal_ef3307ba3b9d979ca11f9abdde2a09ce90b637a444550b307c7acaa57ad5aab3_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_db669b099834dcc0fd451354baace691559473a0452cc3a588cb4928b2736d52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db669b099834dcc0fd451354baace691559473a0452cc3a588cb4928b2736d52->enter($__internal_db669b099834dcc0fd451354baace691559473a0452cc3a588cb4928b2736d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "
    ";
        // line 12
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array(), "any", false, true), "get", array(0 => "error"), "method", true, true)) {
            // line 13
            echo "        ";
            $context["error"] = $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "error"), "method");
            // line 14
            echo "    ";
        }
        // line 15
        echo "    ";
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 16
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 18
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 21
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    ";
        // line 25
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start', array("attr" => array("class" => "", "id" => "form_delete")));
        echo "
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"button\" class=\"btn btn-danger waves-effect waves-light btn-sm pull-right\" id=\"\" onclick=\"eliminar()\">Eliminar</button>
                    </h3>
                    ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
                </div>
                <div class=\"panel-body\">


                    ";
        // line 34
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start', array("attr" => array("class" => "form")));
        echo "

                    <label class=\"control-label\">Imagen</label>
                    <img src=\"";
        // line 37
        echo twig_escape_filter($this->env, ($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/images/") . $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "imagen", array())), "html", null, true);
        echo "\" class=\"responsive-img\" alt=\"Imagen\" style=\"max-width: 100%; max-height: 250px\">
                    ";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_form"] ?? $this->getContext($context, "edit_form")), "imagen", array()), 'row', array("label" => "Cambiar Imagen"));
        echo "

                    ";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_form"] ?? $this->getContext($context, "edit_form")), "linkPdf", array()), 'row', array("label" => "Cambiar Archivo"));
        echo "
                    <div class=\"col-md-12 control-label panel panel-default\">
                        <div class=\"panel-heading\">
                            <a href=\"";
        // line 43
        echo twig_escape_filter($this->env, ($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/docs/") . $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "linkPdf", array())), "html", null, true);
        echo "\" class=\"panel-title text-info\" target=\"_blank\">Ver Archivo Actual</a>
                        </div>
                    </div>

                    ";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget', array("attr" => array("class" => "form-group")));
        echo "

                    <input type=\"hidden\" name=\"img\" value=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "imagen", array()), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"pdf\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "linkPdf", array()), "html", null, true);
        echo "\">
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    ";
        // line 56
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Códigos VIN <a href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subirCodigosRecall", array("idRecall" => $this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-success pull-right\"><span class=\"md  md-file-upload\"></span> Subir Códigos VIN</a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 84
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["recall"] ?? $this->getContext($context, "recall")), "codigos", array()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["entidad"]) {
            // line 85
            echo "                                    <tr>
                                        <td>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "id", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "codigo", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 88
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entidad"], "fecha", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                                    </tr>
                                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 91
            echo "                                    <div class=\"alert alert-info\">
                                        No se han subido los VIN de este Recall.
                                    </div>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_db669b099834dcc0fd451354baace691559473a0452cc3a588cb4928b2736d52->leave($__internal_db669b099834dcc0fd451354baace691559473a0452cc3a588cb4928b2736d52_prof);

    }

    // line 105
    public function block_js($context, array $blocks = array())
    {
        $__internal_1a0655f921ce4180454655fd66e364a813fc8cb022615740dd72c71389365087 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a0655f921ce4180454655fd66e364a813fc8cb022615740dd72c71389365087->enter($__internal_1a0655f921ce4180454655fd66e364a813fc8cb022615740dd72c71389365087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        // line 106
        echo "    <script type=\"text/javascript\">
        function eliminar() {
            swal({
                    title: \"Desea eliminar este elemento?\",
                    text: \"Si se elimina, no se podrá recuperar!\",
                    type: \"warning\",
                    showCancelButton: true,
                    confirmButtonColor: \"#DD6B55\",
                    confirmButtonText: \"Sí, Eliminar!\",
                    closeOnConfirm: false
                },
                function () {
                    document.getElementById(\"form_delete\").submit();
                    swal(\"Eliminado!\", \"Este elemento se eliminó.\", \"success\");
                });
        }
    </script>
";
        
        $__internal_1a0655f921ce4180454655fd66e364a813fc8cb022615740dd72c71389365087->leave($__internal_1a0655f921ce4180454655fd66e364a813fc8cb022615740dd72c71389365087_prof);

    }

    public function getTemplateName()
    {
        return ":recalls:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 106,  252 => 105,  236 => 95,  227 => 91,  219 => 88,  215 => 87,  211 => 86,  208 => 85,  203 => 84,  183 => 67,  169 => 56,  160 => 50,  156 => 49,  151 => 47,  144 => 43,  138 => 40,  133 => 38,  129 => 37,  123 => 34,  115 => 29,  108 => 25,  102 => 21,  96 => 18,  92 => 16,  89 => 15,  86 => 14,  83 => 13,  81 => 12,  78 => 11,  72 => 10,  63 => 7,  58 => 6,  52 => 5,  40 => 4,  33 => 1,  31 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = recall.nombre %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_recalls_index') }}\">Recalls</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    {% if app.request.get('error') is defined %}
        {% set error = app.request.get('error') %}
    {% endif %}
    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> {{ error }}.
        </div>
    {% endif %}
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    {{ form_start(delete_form, { 'attr': {'class': '','id': 'form_delete'} }) }}
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"button\" class=\"btn btn-danger waves-effect waves-light btn-sm pull-right\" id=\"\" onclick=\"eliminar()\">Eliminar</button>
                    </h3>
                    {{ form_end(delete_form) }}
                </div>
                <div class=\"panel-body\">


                    {{ form_start(edit_form, { 'attr': {'class': 'form'} }) }}

                    <label class=\"control-label\">Imagen</label>
                    <img src=\"{{ asset('uploads/images/')~recall.imagen }}\" class=\"responsive-img\" alt=\"Imagen\" style=\"max-width: 100%; max-height: 250px\">
                    {{ form_row(edit_form.imagen, { 'label': 'Cambiar Imagen' }) }}

                    {{ form_row(edit_form.linkPdf, { 'label': 'Cambiar Archivo' }) }}
                    <div class=\"col-md-12 control-label panel panel-default\">
                        <div class=\"panel-heading\">
                            <a href=\"{{ asset('uploads/docs/')~recall.linkPdf }}\" class=\"panel-title text-info\" target=\"_blank\">Ver Archivo Actual</a>
                        </div>
                    </div>

                    {{ form_widget(edit_form, {'attr': {'class': 'form-group'}}) }}

                    <input type=\"hidden\" name=\"img\" value=\"{{ recall.imagen }}\">
                    <input type=\"hidden\" name=\"pdf\" value=\"{{ recall.linkPdf }}\">
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    {{ form_end(edit_form) }}

                </div>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Códigos VIN <a href=\"{{ path('subirCodigosRecall', {'idRecall':recall.id}) }}\" class=\"btn btn-success pull-right\"><span class=\"md  md-file-upload\"></span> Subir Códigos VIN</a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>


                                <tbody>
                                {% for entidad in recall.codigos %}
                                    <tr>
                                        <td>{{ entidad.id }}</td>
                                        <td>{{ entidad.codigo }}</td>
                                        <td>{{ entidad.fecha|date ('d/m/Y') }}</td>
                                    </tr>
                                {% else %}
                                    <div class=\"alert alert-info\">
                                        No se han subido los VIN de este Recall.
                                    </div>
                                {% endfor %}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
{% block js %}
    <script type=\"text/javascript\">
        function eliminar() {
            swal({
                    title: \"Desea eliminar este elemento?\",
                    text: \"Si se elimina, no se podrá recuperar!\",
                    type: \"warning\",
                    showCancelButton: true,
                    confirmButtonColor: \"#DD6B55\",
                    confirmButtonText: \"Sí, Eliminar!\",
                    closeOnConfirm: false
                },
                function () {
                    document.getElementById(\"form_delete\").submit();
                    swal(\"Eliminado!\", \"Este elemento se eliminó.\", \"success\");
                });
        }
    </script>
{% endblock %}", ":recalls:edit.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/recalls/edit.html.twig");
    }
}
