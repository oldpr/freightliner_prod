<?php

/* AppBundle:Default:index.html.twig */
class __TwigTemplate_aaed354247d12cfc880b766ec31da0fe6189551a2dba18337429d093b7ebc0c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c869a9ab437ad283760991436f4906fa3f2099b920fd9769f8d820048ef04197 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c869a9ab437ad283760991436f4906fa3f2099b920fd9769f8d820048ef04197->enter($__internal_c869a9ab437ad283760991436f4906fa3f2099b920fd9769f8d820048ef04197_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c869a9ab437ad283760991436f4906fa3f2099b920fd9769f8d820048ef04197->leave($__internal_c869a9ab437ad283760991436f4906fa3f2099b920fd9769f8d820048ef04197_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_0e67094cd2ee5ee24e622ce6f0532929432f3c3d37dfa9ea2d33ecafb662095a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e67094cd2ee5ee24e622ce6f0532929432f3c3d37dfa9ea2d33ecafb662095a->enter($__internal_0e67094cd2ee5ee24e622ce6f0532929432f3c3d37dfa9ea2d33ecafb662095a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>

    <div class=\"content\">
        <div class=\"form-contact\">
            <h2>LLAMADOS DE SEGURIDAD</h2>
            <p>Bienvenido a la sección especializada de Freightliner Colombia donde se le informará si su vehículo aplica para alguna de las campañas de seguridad vigentes.</p>
            <form action=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("buscarCodigo");
        echo "\" method=\"post\">
                <input type=\"text\" name=\"codigo\" placeholder=\"Ingrese su número VIN o FIN\">
                <input type=\"submit\" value=\"Buscar\" class=\"boton\">
                <a ";
        // line 15
        echo " class=\"que-es\" role=\"button\" data-toggle=\"popover\" data-trigger=\"focus\" title=\"Dismissible popover\" data-content=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"><b>¿Qué es el código VIN o FIN?</b></a>
            </form>
        </div>

    </div>
";
        
        $__internal_0e67094cd2ee5ee24e622ce6f0532929432f3c3d37dfa9ea2d33ecafb662095a->leave($__internal_0e67094cd2ee5ee24e622ce6f0532929432f3c3d37dfa9ea2d33ecafb662095a_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 15,  50 => 12,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block content %}
    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>

    <div class=\"content\">
        <div class=\"form-contact\">
            <h2>LLAMADOS DE SEGURIDAD</h2>
            <p>Bienvenido a la sección especializada de Freightliner Colombia donde se le informará si su vehículo aplica para alguna de las campañas de seguridad vigentes.</p>
            <form action=\"{{ path('buscarCodigo') }}\" method=\"post\">
                <input type=\"text\" name=\"codigo\" placeholder=\"Ingrese su número VIN o FIN\">
                <input type=\"submit\" value=\"Buscar\" class=\"boton\">
                <a {#href=\" {{ path('queEsCodigoVin') }} \"#} class=\"que-es\" role=\"button\" data-toggle=\"popover\" data-trigger=\"focus\" title=\"Dismissible popover\" data-content=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"><b>¿Qué es el código VIN o FIN?</b></a>
            </form>
        </div>

    </div>
{% endblock %}", "AppBundle:Default:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/index.html.twig");
    }
}
