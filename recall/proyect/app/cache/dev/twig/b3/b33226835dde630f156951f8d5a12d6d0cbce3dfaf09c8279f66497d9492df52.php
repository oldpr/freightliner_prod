<?php

/* :concesionarios:index.html.twig */
class __TwigTemplate_a60297ad8165948917dc062ae9eb01c78b3e90c6c243bc6aee94b4cffa25538d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":concesionarios:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3cb508b7b2626186cc41129db3e6c68291099238ac4667a190b0688ef1863854 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cb508b7b2626186cc41129db3e6c68291099238ac4667a190b0688ef1863854->enter($__internal_3cb508b7b2626186cc41129db3e6c68291099238ac4667a190b0688ef1863854_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":concesionarios:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3cb508b7b2626186cc41129db3e6c68291099238ac4667a190b0688ef1863854->leave($__internal_3cb508b7b2626186cc41129db3e6c68291099238ac4667a190b0688ef1863854_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dc8f9817005c50cc479fe7789aa10cb1e825d45814d5a2b570f12d22905679f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc8f9817005c50cc479fe7789aa10cb1e825d45814d5a2b570f12d22905679f9->enter($__internal_dc8f9817005c50cc479fe7789aa10cb1e825d45814d5a2b570f12d22905679f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Concesionarios list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["concesionarios"] ?? $this->getContext($context, "concesionarios")));
        foreach ($context['_seq'] as $context["_key"] => $context["concesionario"]) {
            // line 17
            echo "            <tr>
                <td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_show", array("id" => $this->getAttribute($context["concesionario"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "nombre", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "descripcion", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_show", array("id" => $this->getAttribute($context["concesionario"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_edit", array("id" => $this->getAttribute($context["concesionario"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concesionario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_concesionarios_new");
        echo "\">Create a new concesionario</a>
        </li>
    </ul>
";
        
        $__internal_dc8f9817005c50cc479fe7789aa10cb1e825d45814d5a2b570f12d22905679f9->leave($__internal_dc8f9817005c50cc479fe7789aa10cb1e825d45814d5a2b570f12d22905679f9_prof);

    }

    public function getTemplateName()
    {
        return ":concesionarios:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 38,  96 => 33,  84 => 27,  78 => 24,  71 => 20,  67 => 19,  61 => 18,  58 => 17,  54 => 16,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Concesionarios list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for concesionario in concesionarios %}
            <tr>
                <td><a href=\"{{ path('admin_concesionarios_show', { 'id': concesionario.id }) }}\">{{ concesionario.id }}</a></td>
                <td>{{ concesionario.nombre }}</td>
                <td>{{ concesionario.descripcion }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('admin_concesionarios_show', { 'id': concesionario.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('admin_concesionarios_edit', { 'id': concesionario.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_concesionarios_new') }}\">Create a new concesionario</a>
        </li>
    </ul>
{% endblock %}
", ":concesionarios:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/concesionarios/index.html.twig");
    }
}
