<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_f0cba76c5dbb2a250596e4adb7fb431e6bed1e193c76d48cb5ac372a6155ac8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_74c55351c86c152509546c3dd4b2d6e162600ae027a7223afae32f1cf3aff572 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74c55351c86c152509546c3dd4b2d6e162600ae027a7223afae32f1cf3aff572->enter($__internal_74c55351c86c152509546c3dd4b2d6e162600ae027a7223afae32f1cf3aff572_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_74c55351c86c152509546c3dd4b2d6e162600ae027a7223afae32f1cf3aff572->leave($__internal_74c55351c86c152509546c3dd4b2d6e162600ae027a7223afae32f1cf3aff572_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
