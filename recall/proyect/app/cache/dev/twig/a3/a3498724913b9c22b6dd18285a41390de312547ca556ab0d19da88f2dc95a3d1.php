<?php

/* :ciudades:new.html.twig */
class __TwigTemplate_621fb80d1963e33b33f7dec69883da3639fd8c504d829443efcd85d0f322e3a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":ciudades:new.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2e2dfac2e4622ffaee3a28cfb66220fd43135db22efa7da6d075e8617dd589a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2e2dfac2e4622ffaee3a28cfb66220fd43135db22efa7da6d075e8617dd589a->enter($__internal_c2e2dfac2e4622ffaee3a28cfb66220fd43135db22efa7da6d075e8617dd589a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":ciudades:new.html.twig"));

        // line 3
        $context["nomPagina"] = "Nueva Ciudad";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c2e2dfac2e4622ffaee3a28cfb66220fd43135db22efa7da6d075e8617dd589a->leave($__internal_c2e2dfac2e4622ffaee3a28cfb66220fd43135db22efa7da6d075e8617dd589a_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_ed766ef22807d0b5d34c04b7d9ceeca2c904a70a4f9abef98410ee42ae320300 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed766ef22807d0b5d34c04b7d9ceeca2c904a70a4f9abef98410ee42ae320300->enter($__internal_ed766ef22807d0b5d34c04b7d9ceeca2c904a70a4f9abef98410ee42ae320300_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_ed766ef22807d0b5d34c04b7d9ceeca2c904a70a4f9abef98410ee42ae320300->leave($__internal_ed766ef22807d0b5d34c04b7d9ceeca2c904a70a4f9abef98410ee42ae320300_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_e40b144ddefd5ec7fcf8fca5511f39ea17bcda0172654e7db008b7cba78c64a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e40b144ddefd5ec7fcf8fca5511f39ea17bcda0172654e7db008b7cba78c64a6->enter($__internal_e40b144ddefd5ec7fcf8fca5511f39ea17bcda0172654e7db008b7cba78c64a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_index");
        echo "\">Ciudades</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_e40b144ddefd5ec7fcf8fca5511f39ea17bcda0172654e7db008b7cba78c64a6->leave($__internal_e40b144ddefd5ec7fcf8fca5511f39ea17bcda0172654e7db008b7cba78c64a6_prof);

    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        $__internal_d29a2d1f8e03c4ed1196b4e269b647b440b23bef91bdab803556d29bb753ef19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d29a2d1f8e03c4ed1196b4e269b647b440b23bef91bdab803556d29bb753ef19->enter($__internal_d29a2d1f8e03c4ed1196b4e269b647b440b23bef91bdab803556d29bb753ef19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 11
        echo "
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    ";
        // line 18
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "

                    ";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    ";
        // line 26
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_d29a2d1f8e03c4ed1196b4e269b647b440b23bef91bdab803556d29bb753ef19->leave($__internal_d29a2d1f8e03c4ed1196b4e269b647b440b23bef91bdab803556d29bb753ef19_prof);

    }

    public function getTemplateName()
    {
        return ":ciudades:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 26,  91 => 20,  86 => 18,  77 => 11,  71 => 10,  62 => 7,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Nueva Ciudad' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_ciudades_index') }}\">Ciudades</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    {{ form_start(form) }}

                    {{ form_widget(form) }}
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    {{ form_end(form) }}

                </div>
            </div>
        </div>
    </div>

{% endblock %}
", ":ciudades:new.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/ciudades/new.html.twig");
    }
}
