<?php

/* AdminBundle:Security:login.html.twig */
class __TwigTemplate_b27e7487f0e06d693ac67e37d94cff9d4465e03624addb73752843845669d549 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18e972cf65a137916541f97a26df2505f534fc74f68ca27ca056c89501a54788 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18e972cf65a137916541f97a26df2505f534fc74f68ca27ca056c89501a54788->enter($__internal_18e972cf65a137916541f97a26df2505f534fc74f68ca27ca056c89501a54788_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Security:login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\" />
    <title>Login Dashboard Daimler Colombia</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />
    <meta content=\"Login del Dash y cms del portal oficial de Daimler Colombia.\" name=\"description\" />
    <meta content=\"Coderthemes\" name=\"author\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />

    <link rel=\"shortcut icon\" href=' ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/favicon.ico"), "html", null, true);
        echo " '>

    <link href=' ";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/bootstrap.min.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/core.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/icons.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/components.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/pages.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/menu.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/responsive.css"), "html", null, true);
        echo " ' rel=\"stylesheet\" type=\"text/css\">

    <script src=' ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/modernizr.min.js"), "html", null, true);
        echo " '></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
    <![endif]-->


</head>
<body>


<div class=\"wrapper-page\">
    <div class=\"panel panel-color panel-primary panel-pages\">
        <div class=\"panel-heading bg-img\">
            <div class=\"bg-overlay\"></div>
            <h3 class=\"text-center m-t-10 text-white\"> Sign In to <strong>Admin to <a class=\"text text-info\" target=\"_blank\" href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Daimler Colombia</a></strong> </h3>
        </div>


        <div class=\"panel-body\">
            ";
        // line 44
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 45
            echo "                <div class=\"alert alert-danger alert-dismissible fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
                    ";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 50
        echo "
            <form class=\"form-horizontal m-t-20\" action=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">

                <div class=\"form-group\">
                    <div class=\"col-xs-12\">
                        <input class=\"form-control input-lg\" type=\"text\" name=\"_username\" value=\"";
        // line 55
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"\" placeholder=\"Username\">
                    </div>
                </div>

                <div class=\"form-group\">
                    <div class=\"col-xs-12\">
                        <input class=\"form-control input-lg\" type=\"password\" name=\"_password\" required=\"\" placeholder=\"Password\">
                    </div>
                </div>

                <div class=\"form-group text-center m-t-40\">
                    <div class=\"col-xs-12\">
                        <button class=\"btn btn-primary btn-lg w-lg waves-effect waves-light\" type=\"submit\">Log In</button>
                    </div>
                </div>
            </form>
            <a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"align-right btn btn-flat waves-effect waves-light btn-xs\">Ir al sitio</a>

        </div>

    </div>
</div>


<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src=' ";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.min.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/bootstrap.min.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/detect.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/fastclick.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.slimscroll.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.blockUI.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/waves.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/wow.min.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.nicescroll.js"), "html", null, true);
        echo " '></script>
<script src=' ";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.scrollTo.min.js"), "html", null, true);
        echo " '></script>

<script src=' ";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.app.js"), "html", null, true);
        echo " '></script>

</body>
</html>";
        
        $__internal_18e972cf65a137916541f97a26df2505f534fc74f68ca27ca056c89501a54788->leave($__internal_18e972cf65a137916541f97a26df2505f534fc74f68ca27ca056c89501a54788_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 95,  190 => 93,  186 => 92,  182 => 91,  178 => 90,  174 => 89,  170 => 88,  166 => 87,  162 => 86,  158 => 85,  154 => 84,  138 => 71,  119 => 55,  112 => 51,  109 => 50,  103 => 47,  99 => 45,  97 => 44,  89 => 39,  68 => 21,  63 => 19,  59 => 18,  55 => 17,  51 => 16,  47 => 15,  43 => 14,  39 => 13,  34 => 11,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\" />
    <title>Login Dashboard Daimler Colombia</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />
    <meta content=\"Login del Dash y cms del portal oficial de Daimler Colombia.\" name=\"description\" />
    <meta content=\"Coderthemes\" name=\"author\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />

    <link rel=\"shortcut icon\" href=' {{ asset(\"assets/favicon.ico\") }} '>

    <link href=' {{ asset(\"moltran/css/bootstrap.min.css\") }} ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' {{ asset(\"moltran/css/core.css\") }} ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' {{ asset(\"moltran/css/icons.css\") }} ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' {{ asset(\"moltran/css/components.css\") }} ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' {{ asset(\"moltran/css/pages.css\") }} ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' {{ asset(\"moltran/css/menu.css\") }} ' rel=\"stylesheet\" type=\"text/css\">
    <link href=' {{ asset(\"moltran/css/responsive.css\") }} ' rel=\"stylesheet\" type=\"text/css\">

    <script src=' {{ asset(\"moltran/js/modernizr.min.js\") }} '></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
    <![endif]-->


</head>
<body>


<div class=\"wrapper-page\">
    <div class=\"panel panel-color panel-primary panel-pages\">
        <div class=\"panel-heading bg-img\">
            <div class=\"bg-overlay\"></div>
            <h3 class=\"text-center m-t-10 text-white\"> Sign In to <strong>Admin to <a class=\"text text-info\" target=\"_blank\" href=\"{{ path('homepage') }}\">Daimler Colombia</a></strong> </h3>
        </div>


        <div class=\"panel-body\">
            {% if error %}
                <div class=\"alert alert-danger alert-dismissible fade in\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
                    {{ error.messageKey|trans(error.messageData, 'security') }}
                </div>
            {% endif %}

            <form class=\"form-horizontal m-t-20\" action=\"{{ path('login') }}\" method=\"post\">

                <div class=\"form-group\">
                    <div class=\"col-xs-12\">
                        <input class=\"form-control input-lg\" type=\"text\" name=\"_username\" value=\"{{ last_username }}\" required=\"\" placeholder=\"Username\">
                    </div>
                </div>

                <div class=\"form-group\">
                    <div class=\"col-xs-12\">
                        <input class=\"form-control input-lg\" type=\"password\" name=\"_password\" required=\"\" placeholder=\"Password\">
                    </div>
                </div>

                <div class=\"form-group text-center m-t-40\">
                    <div class=\"col-xs-12\">
                        <button class=\"btn btn-primary btn-lg w-lg waves-effect waves-light\" type=\"submit\">Log In</button>
                    </div>
                </div>
            </form>
            <a href=\"{{ path('homepage') }}\" class=\"align-right btn btn-flat waves-effect waves-light btn-xs\">Ir al sitio</a>

        </div>

    </div>
</div>


<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src=' {{ asset(\"moltran/js/jquery.min.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/bootstrap.min.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/detect.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/fastclick.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/jquery.slimscroll.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/jquery.blockUI.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/waves.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/wow.min.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/jquery.nicescroll.js\") }} '></script>
<script src=' {{ asset(\"moltran/js/jquery.scrollTo.min.js\") }} '></script>

<script src=' {{ asset(\"moltran/js/jquery.app.js\") }} '></script>

</body>
</html>", "AdminBundle:Security:login.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AdminBundle/Resources/views/Security/login.html.twig");
    }
}
