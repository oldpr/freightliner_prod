<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_66047f95a3d6fdf50cab55a54fe1375093438a204319daf999979ebe077df967 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_181e55d34f487539e10aae3635670170d400a33c76b64bc4f78e3553a94af173 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_181e55d34f487539e10aae3635670170d400a33c76b64bc4f78e3553a94af173->enter($__internal_181e55d34f487539e10aae3635670170d400a33c76b64bc4f78e3553a94af173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_181e55d34f487539e10aae3635670170d400a33c76b64bc4f78e3553a94af173->leave($__internal_181e55d34f487539e10aae3635670170d400a33c76b64bc4f78e3553a94af173_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_95752e1d3b6407ba1916cf395a0c6fb38d44dbe04e2c14e4613effa56cbcb7ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95752e1d3b6407ba1916cf395a0c6fb38d44dbe04e2c14e4613effa56cbcb7ec->enter($__internal_95752e1d3b6407ba1916cf395a0c6fb38d44dbe04e2c14e4613effa56cbcb7ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_95752e1d3b6407ba1916cf395a0c6fb38d44dbe04e2c14e4613effa56cbcb7ec->leave($__internal_95752e1d3b6407ba1916cf395a0c6fb38d44dbe04e2c14e4613effa56cbcb7ec_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ff1bced5096d8a500975432a429049ccc11955fcbe8a482410e6cf280bf69371 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff1bced5096d8a500975432a429049ccc11955fcbe8a482410e6cf280bf69371->enter($__internal_ff1bced5096d8a500975432a429049ccc11955fcbe8a482410e6cf280bf69371_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_ff1bced5096d8a500975432a429049ccc11955fcbe8a482410e6cf280bf69371->leave($__internal_ff1bced5096d8a500975432a429049ccc11955fcbe8a482410e6cf280bf69371_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
