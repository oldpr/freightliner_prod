<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_53b53cf5b12b0541a2ce93df18efbb1c5a35eb7a4d6f0ab1528f2f9420304ff6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c1029f9064bd7a64ddbe0c41a917335d783f68b1da154e61d4bdcaf1003d566 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c1029f9064bd7a64ddbe0c41a917335d783f68b1da154e61d4bdcaf1003d566->enter($__internal_7c1029f9064bd7a64ddbe0c41a917335d783f68b1da154e61d4bdcaf1003d566_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_7c1029f9064bd7a64ddbe0c41a917335d783f68b1da154e61d4bdcaf1003d566->leave($__internal_7c1029f9064bd7a64ddbe0c41a917335d783f68b1da154e61d4bdcaf1003d566_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
