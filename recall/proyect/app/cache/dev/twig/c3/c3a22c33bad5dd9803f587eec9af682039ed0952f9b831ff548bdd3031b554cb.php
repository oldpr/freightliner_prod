<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_07aa1b9f278a14a1cb3eea214040cd88fa95d9fe31f7279f976b1036233037c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a4a7d7176fbcfac5da82b5c96342d09ae04734723b7dfff14a9ae024027c0f4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4a7d7176fbcfac5da82b5c96342d09ae04734723b7dfff14a9ae024027c0f4d->enter($__internal_a4a7d7176fbcfac5da82b5c96342d09ae04734723b7dfff14a9ae024027c0f4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_a4a7d7176fbcfac5da82b5c96342d09ae04734723b7dfff14a9ae024027c0f4d->leave($__internal_a4a7d7176fbcfac5da82b5c96342d09ae04734723b7dfff14a9ae024027c0f4d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
