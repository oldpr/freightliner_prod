<?php

/* :recalls:index.html.twig */
class __TwigTemplate_7bf6bf4985258583cc195ec42080e18f2fcc9f92229c61eb9f2b89f28c7a61de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":recalls:index.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14df48c33b0f8e5c2dda0b1a20393c3927c85d1dae60f6b6965fa7ace1e16627 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14df48c33b0f8e5c2dda0b1a20393c3927c85d1dae60f6b6965fa7ace1e16627->enter($__internal_14df48c33b0f8e5c2dda0b1a20393c3927c85d1dae60f6b6965fa7ace1e16627_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":recalls:index.html.twig"));

        // line 3
        $context["nomPagina"] = "Recalls";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_14df48c33b0f8e5c2dda0b1a20393c3927c85d1dae60f6b6965fa7ace1e16627->leave($__internal_14df48c33b0f8e5c2dda0b1a20393c3927c85d1dae60f6b6965fa7ace1e16627_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_487e0a6243b6377e9968ae12c928139c97fc1c797f40c186a02274f2e92ef415 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_487e0a6243b6377e9968ae12c928139c97fc1c797f40c186a02274f2e92ef415->enter($__internal_487e0a6243b6377e9968ae12c928139c97fc1c797f40c186a02274f2e92ef415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_487e0a6243b6377e9968ae12c928139c97fc1c797f40c186a02274f2e92ef415->leave($__internal_487e0a6243b6377e9968ae12c928139c97fc1c797f40c186a02274f2e92ef415_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_e4ef7f8ae3c6eea3635623fd0c5bb2b88dd45352af2437a84a8ba1b4480dd4b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4ef7f8ae3c6eea3635623fd0c5bb2b88dd45352af2437a84a8ba1b4480dd4b2->enter($__internal_e4ef7f8ae3c6eea3635623fd0c5bb2b88dd45352af2437a84a8ba1b4480dd4b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li class=\"active\">";
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_e4ef7f8ae3c6eea3635623fd0c5bb2b88dd45352af2437a84a8ba1b4480dd4b2->leave($__internal_e4ef7f8ae3c6eea3635623fd0c5bb2b88dd45352af2437a84a8ba1b4480dd4b2_prof);

    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        $__internal_72472c97acc612764662e1522489db65f7cdd86034fd2d8508fb10aeab50c26b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72472c97acc612764662e1522489db65f7cdd86034fd2d8508fb10aeab50c26b->enter($__internal_72472c97acc612764662e1522489db65f7cdd86034fd2d8508fb10aeab50c26b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 10
        echo "
    <!-- container -->
    ";
        // line 12
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 13
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: ";
            // line 15
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 18
        echo "
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Lista <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_new");
        echo "\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["recalls"] ?? $this->getContext($context, "recalls")));
        foreach ($context['_seq'] as $context["_key"] => $context["entidad"]) {
            // line 42
            echo "                                    <tr>
                                        <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "id", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_edit", array("id" => $this->getAttribute($context["entidad"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "codigo", array()), "html", null, true);
            echo "</a></td>
                                        <td><a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_edit", array("id" => $this->getAttribute($context["entidad"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "nombre", array()), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 46
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entidad"], "fecha", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_72472c97acc612764662e1522489db65f7cdd86034fd2d8508fb10aeab50c26b->leave($__internal_72472c97acc612764662e1522489db65f7cdd86034fd2d8508fb10aeab50c26b_prof);

    }

    public function getTemplateName()
    {
        return ":recalls:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 49,  140 => 46,  134 => 45,  128 => 44,  124 => 43,  121 => 42,  117 => 41,  96 => 23,  89 => 18,  83 => 15,  79 => 13,  77 => 12,  73 => 10,  67 => 9,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Recalls' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    <!-- container -->
    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: {{ error }}.
        </div>
    {% endif %}

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Lista <a href=\"{{ path('admin_recalls_new')}}\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>


                                <tbody>
                                {% for entidad in recalls %}
                                    <tr>
                                        <td>{{ entidad.id }}</td>
                                        <td><a href=\"{{ path('admin_recalls_edit', { 'id': entidad.id }) }}\">{{ entidad.codigo }}</a></td>
                                        <td><a href=\"{{ path('admin_recalls_edit', { 'id': entidad.id }) }}\">{{ entidad.nombre }}</a></td>
                                        <td>{{ entidad.fecha|date ('d/m/Y') }}</td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
", ":recalls:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/recalls/index.html.twig");
    }
}
