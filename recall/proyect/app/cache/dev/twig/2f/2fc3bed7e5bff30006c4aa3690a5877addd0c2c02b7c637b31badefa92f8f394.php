<?php

/* :concesionarios:edit.html.twig */
class __TwigTemplate_934e5adb77dfeaaa4275dc1442a594524611f84ffefccdc49c3338dc721e5762 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":concesionarios:edit.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_900f4a5c18df5e2fbfff9fa7a82874d73535d79da034a4e3b4edc5d2566d7b2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_900f4a5c18df5e2fbfff9fa7a82874d73535d79da034a4e3b4edc5d2566d7b2e->enter($__internal_900f4a5c18df5e2fbfff9fa7a82874d73535d79da034a4e3b4edc5d2566d7b2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":concesionarios:edit.html.twig"));

        // line 3
        $context["nomPagina"] = $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "nombre", array());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_900f4a5c18df5e2fbfff9fa7a82874d73535d79da034a4e3b4edc5d2566d7b2e->leave($__internal_900f4a5c18df5e2fbfff9fa7a82874d73535d79da034a4e3b4edc5d2566d7b2e_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_b8c42c3f6c78c2fabaed1cd5dda5f7db428096fdb7eed0e1b662c25ecbc75b4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8c42c3f6c78c2fabaed1cd5dda5f7db428096fdb7eed0e1b662c25ecbc75b4a->enter($__internal_b8c42c3f6c78c2fabaed1cd5dda5f7db428096fdb7eed0e1b662c25ecbc75b4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_b8c42c3f6c78c2fabaed1cd5dda5f7db428096fdb7eed0e1b662c25ecbc75b4a->leave($__internal_b8c42c3f6c78c2fabaed1cd5dda5f7db428096fdb7eed0e1b662c25ecbc75b4a_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_efcfbfbf84c8675e28c96c5be538dabf6a8f6415515de5f6771834ff2131e4a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efcfbfbf84c8675e28c96c5be538dabf6a8f6415515de5f6771834ff2131e4a9->enter($__internal_efcfbfbf84c8675e28c96c5be538dabf6a8f6415515de5f6771834ff2131e4a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_index");
        echo "\">Ciudades</a></li>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_edit", array("id" => $this->getAttribute($this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "ciudad", array()), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["concesionario"] ?? $this->getContext($context, "concesionario")), "ciudad", array()), "html", null, true);
        echo "</a></li>
    <li class=\"active\">";
        // line 8
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_efcfbfbf84c8675e28c96c5be538dabf6a8f6415515de5f6771834ff2131e4a9->leave($__internal_efcfbfbf84c8675e28c96c5be538dabf6a8f6415515de5f6771834ff2131e4a9_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_c28cd7ae7b4358278c1fb27d6a7a134574a56348bff1947cc48bce9c2d96075c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c28cd7ae7b4358278c1fb27d6a7a134574a56348bff1947cc48bce9c2d96075c->enter($__internal_c28cd7ae7b4358278c1fb27d6a7a134574a56348bff1947cc48bce9c2d96075c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "
    ";
        // line 13
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array(), "any", false, true), "get", array(0 => "error"), "method", true, true)) {
            // line 14
            echo "        ";
            $context["error"] = $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "error"), "method");
            // line 15
            echo "    ";
        }
        // line 16
        echo "    ";
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 17
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 19
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 22
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    ";
        // line 26
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start', array("attr" => array("class" => "")));
        echo "
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"submit\" class=\"btn btn-danger waves-effect waves-light pull-right\">Eliminar</button>
                    </h3>
                    ";
        // line 30
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
                </div>
                <div class=\"panel-body\">

                    ";
        // line 34
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
                    ";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    ";
        // line 41
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_c28cd7ae7b4358278c1fb27d6a7a134574a56348bff1947cc48bce9c2d96075c->leave($__internal_c28cd7ae7b4358278c1fb27d6a7a134574a56348bff1947cc48bce9c2d96075c_prof);

    }

    public function getTemplateName()
    {
        return ":concesionarios:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 41,  131 => 35,  127 => 34,  120 => 30,  113 => 26,  107 => 22,  101 => 19,  97 => 17,  94 => 16,  91 => 15,  88 => 14,  86 => 13,  83 => 12,  77 => 11,  68 => 8,  62 => 7,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = concesionario.nombre %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_ciudades_index') }}\">Ciudades</a></li>
    <li><a href=\"{{ path('admin_ciudades_edit',{'id':concesionario.ciudad.id}) }}\">{{ concesionario.ciudad }}</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}

    {% if app.request.get('error') is defined %}
        {% set error = app.request.get('error') %}
    {% endif %}
    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> {{ error }}.
        </div>
    {% endif %}
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    {{ form_start(delete_form, { 'attr': {'class': ''} }) }}
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"submit\" class=\"btn btn-danger waves-effect waves-light pull-right\">Eliminar</button>
                    </h3>
                    {{ form_end(delete_form) }}
                </div>
                <div class=\"panel-body\">

                    {{ form_start(edit_form) }}
                    {{ form_widget(edit_form) }}
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    {{ form_end(edit_form) }}

                </div>
            </div>
        </div>
    </div>
{% endblock %}
", ":concesionarios:edit.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/concesionarios/edit.html.twig");
    }
}
