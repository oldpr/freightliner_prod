<?php

/* AppBundle:Default:concesionarios.html.twig */
class __TwigTemplate_9200c4eb29d4e8305e9c4357db260d5f04d16e4b06b15c037049646eb94437bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:concesionarios.html.twig", 1);
        $this->blocks = array(
            'banerInicial' => array($this, 'block_banerInicial'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7fb0854adcf547b5c4bc8c7b3f93d358f0fcbebafd7ab77326ebcaacfdf9158 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7fb0854adcf547b5c4bc8c7b3f93d358f0fcbebafd7ab77326ebcaacfdf9158->enter($__internal_f7fb0854adcf547b5c4bc8c7b3f93d358f0fcbebafd7ab77326ebcaacfdf9158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Default:concesionarios.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7fb0854adcf547b5c4bc8c7b3f93d358f0fcbebafd7ab77326ebcaacfdf9158->leave($__internal_f7fb0854adcf547b5c4bc8c7b3f93d358f0fcbebafd7ab77326ebcaacfdf9158_prof);

    }

    // line 2
    public function block_banerInicial($context, array $blocks = array())
    {
        $__internal_91876f72e551205e9f6ca9bfb8aa9bd84d7acdf310e6432fa43fdfa0e819f2f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91876f72e551205e9f6ca9bfb8aa9bd84d7acdf310e6432fa43fdfa0e819f2f5->enter($__internal_91876f72e551205e9f6ca9bfb8aa9bd84d7acdf310e6432fa43fdfa0e819f2f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "banerInicial"));

        
        $__internal_91876f72e551205e9f6ca9bfb8aa9bd84d7acdf310e6432fa43fdfa0e819f2f5->leave($__internal_91876f72e551205e9f6ca9bfb8aa9bd84d7acdf310e6432fa43fdfa0e819f2f5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_aa86a3682d997da51893cbabb2c650ef89bae3aeac4392a0f506184cda681c62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa86a3682d997da51893cbabb2c650ef89bae3aeac4392a0f506184cda681c62->enter($__internal_aa86a3682d997da51893cbabb2c650ef89bae3aeac4392a0f506184cda681c62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

    <div class=\"container\">
        <div class=\"inner clientes\" style=\"margin-top:150px\" id=\"\">
            <hr>
            <!--CONTENIDOS-->
            <div class=\"container content-interna\" style=\"margin-top:60px;\">
                <div class=\"row\">
                    <!--MENU LATERAL -->
                    <aside class=\"col-lg-3 col-md-3 col-sm-4 \">
                        <nav class=\"menuLateral\">
                            <ul>
                                ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ciudades"] ?? $this->getContext($context, "ciudades")));
        foreach ($context['_seq'] as $context["_key"] => $context["ciudad"]) {
            // line 17
            echo "                                    <li><a data-id=\"#";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "nombre", array()), "html", null, true);
            echo "</a></li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ciudad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                            </ul>
                        </nav>

                    </aside>
                    <div class=\"col-lg-9 col-md-9 col-sm-8 tabinner concesionarios\">
                        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ciudades"] ?? $this->getContext($context, "ciudades")));
        foreach ($context['_seq'] as $context["_key"] => $context["ciudad"]) {
            // line 25
            echo "                        <div id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "id", array()), "html", null, true);
            echo "\" class=\"contCon\">
                            <div class=\"col-md-12\">
                                <h3>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "nombre", array()), "html", null, true);
            echo "</h3>
                            </div>

                            ";
            // line 30
            $context["cont"] = 0;
            // line 31
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["ciudad"], "concesionarios", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["concesionario"]) {
                // line 32
                echo "                                ";
                $context["cont"] = (($context["cont"] ?? $this->getContext($context, "cont")) + 1);
                // line 33
                echo "                                ";
                if (((($context["cont"] ?? $this->getContext($context, "cont")) % 2) == 1)) {
                    // line 34
                    echo "                                    <div class=\"row capConce\">
                                    <div class=\"col-md-6\">
                                        <b>";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "nombre", array()), "html", null, true);
                    echo "</b><br>
                                        ";
                    // line 37
                    echo $this->getAttribute($context["concesionario"], "descripcion", array());
                    echo "
                                    </div>
                                ";
                } else {
                    // line 40
                    echo "                                    <div class=\"col-md-6\">
                                        <strong>";
                    // line 41
                    echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "nombre", array()), "html", null, true);
                    echo "</strong><br>
                                        ";
                    // line 42
                    echo $this->getAttribute($context["concesionario"], "descripcion", array());
                    echo "
                                    </div>
                                    </div>
                                ";
                }
                // line 46
                echo "
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concesionario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "
                            ";
            // line 49
            if (((($context["cont"] ?? $this->getContext($context, "cont")) % 2) == 1)) {
                // line 50
                echo "                        </div>
                        ";
            }
            // line 52
            echo "                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ciudad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_aa86a3682d997da51893cbabb2c650ef89bae3aeac4392a0f506184cda681c62->leave($__internal_aa86a3682d997da51893cbabb2c650ef89bae3aeac4392a0f506184cda681c62_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Default:concesionarios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 54,  161 => 52,  157 => 50,  155 => 49,  152 => 48,  145 => 46,  138 => 42,  134 => 41,  131 => 40,  125 => 37,  121 => 36,  117 => 34,  114 => 33,  111 => 32,  106 => 31,  104 => 30,  98 => 27,  92 => 25,  88 => 24,  81 => 19,  70 => 17,  66 => 16,  52 => 4,  46 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block banerInicial %}{% endblock %}
{% block body %}


    <div class=\"container\">
        <div class=\"inner clientes\" style=\"margin-top:150px\" id=\"\">
            <hr>
            <!--CONTENIDOS-->
            <div class=\"container content-interna\" style=\"margin-top:60px;\">
                <div class=\"row\">
                    <!--MENU LATERAL -->
                    <aside class=\"col-lg-3 col-md-3 col-sm-4 \">
                        <nav class=\"menuLateral\">
                            <ul>
                                {% for ciudad in ciudades %}
                                    <li><a data-id=\"#{{ ciudad.id }}\">{{ ciudad.nombre }}</a></li>
                                {% endfor %}
                            </ul>
                        </nav>

                    </aside>
                    <div class=\"col-lg-9 col-md-9 col-sm-8 tabinner concesionarios\">
                        {% for ciudad in ciudades %}
                        <div id=\"{{ ciudad.id }}\" class=\"contCon\">
                            <div class=\"col-md-12\">
                                <h3>{{ ciudad.nombre }}</h3>
                            </div>

                            {% set cont = 0 %}
                            {% for concesionario in ciudad.concesionarios %}
                                {% set cont = cont + 1 %}
                                {% if cont % 2 == 1 %}
                                    <div class=\"row capConce\">
                                    <div class=\"col-md-6\">
                                        <b>{{ concesionario.nombre }}</b><br>
                                        {{ concesionario.descripcion|raw }}
                                    </div>
                                {% else %}
                                    <div class=\"col-md-6\">
                                        <strong>{{ concesionario.nombre }}</strong><br>
                                        {{ concesionario.descripcion|raw }}
                                    </div>
                                    </div>
                                {% endif %}

                            {% endfor %}

                            {% if cont % 2 == 1 %}
                        </div>
                        {% endif %}
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>

{% endblock %}", "AppBundle:Default:concesionarios.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/concesionarios.html.twig");
    }
}
