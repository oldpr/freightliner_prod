<?php

/* base_admin.html.twig */
class __TwigTemplate_bf87d4494e761ff7e36d64c34539cf0070fe8cd6432c0d9f388d7ca4fbb5d59a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'css' => array($this, 'block_css'),
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'js' => array($this, 'block_js'),
            'ready' => array($this, 'block_ready'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_08a6524ee4d8bd479ee5942683f007521eabbdcfa4f0366a6f2b0fd465176af5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08a6524ee4d8bd479ee5942683f007521eabbdcfa4f0366a6f2b0fd465176af5->enter($__internal_08a6524ee4d8bd479ee5942683f007521eabbdcfa4f0366a6f2b0fd465176af5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base_admin.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\" />
    <title>Dashboard Daimler Colombia</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />
    <meta content=\"CMS para el proyecto Recoll de Daimler Colombia.\" name=\"description\" />
    <meta content=\"Coderthemes\" name=\"author\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />

    <link rel=\"shortcut icon\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/favicon.ico"), "html", null, true);
        echo "\">

    <!--venobox lightbox-->
    <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/magnific-popup/dist/magnific-popup.css"), "html", null, true);
        echo "\">

    <!-- DataTables -->
    <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/datatables/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

    <!-- Dropzone css -->
    <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/dropzone/dist/dropzone.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

    <!--bootstrap-wysihtml5-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"), "html", null, true);
        echo "\">
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/summernote/dist/summernote.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    ";
        // line 26
        $this->displayBlock('css', $context, $blocks);
        // line 29
        echo "
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/sweetalert/dist/sweetalert.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

    <link href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/core.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/icons.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/components.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/pages.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/css/responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">


    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/modernizr.min.js"), "html", null, true);
        echo "\"></script>
    <script>
        var nuevaImagen = '';
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js') }}/1.3.0/respond.min.js\"></script>
    <![endif]-->


</head>


<body class=\"fixed-left\">

<!-- Begin page -->
<div id=\"wrapper\">

    <!-- Top Bar Start -->
    <div class=\"topbar\">
        <!-- LOGO -->
        <div class=\"topbar-left\">
            <div class=\"text-center\">
                <a href=\"";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_homepage");
        echo "\" class=\"logo\"><i class=\"md md-domain text-primary\" style=\"color: slategrey;\"></i> <span>Dashboard </span></a>
            </div>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class=\"navbar navbar-default\" role=\"navigation\">
            <div class=\"container\">
                <div class=\"\">
                    <div class=\"pull-left\">
                        <button type=\"button\" class=\"button-menu-mobile open-left\">
                            <i class=\"fa fa-bars\"></i>
                        </button>
                        <span class=\"clearfix\"></span>
                    </div>
                    <!--<form class=\"navbar-form pull-left\" role=\"search\">
                        <div class=\"form-group\">
                            <input type=\"text\" class=\"form-control search-bar\" placeholder=\"Type here for search...\">
                        </div>
                        <button type=\"submit\" class=\"btn btn-search\"><i class=\"fa fa-search\"></i></button>
                    </form>-->

                    <ul class=\"nav navbar-nav navbar-right pull-right\">
                        <li class=\"hidden-xs\">
                            <a href=\"#\" id=\"btn-fullscreen\" class=\"waves-effect\"><i class=\"md md-crop-free\"></i></a>
                        </li>
                        <li class=\"hidden-xs\">
                            <a href=\"";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout");
        echo "\" class=\"waves-effect\"><i class=\"md md-exit-to-app\"></i></a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class=\"left side-menu\">
        <div class=\"sidebar-inner slimscrollleft\">
            <div class=\"user-details\">
                <div class=\"pull-left\">
                    <img src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/daimler_logo_claro.png"), "html", null, true);
        echo "\" alt=\"\" class=\"thumb-md img-circle\">
                </div>
                <div class=\"user-info\">
                    <p class=\"text-muted\"><b>";
        // line 112
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array())), "html", null, true);
        echo "</b></p>
                    <p class=\"text-muted m-0\">Rol: ";
        // line 113
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, twig_replace_filter($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "roles", array()), 0, array(), "array"), array("ROLE_" => ""))), "html", null, true);
        echo "</p>
                </div>
            </div>
            <!--- Divider -->
            <div id=\"sidebar-menu\">
                <ul>
                    <li>
                        <a href=\"";
        // line 120
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_homepage");
        echo "\" class=\"waves-effect\"><i class=\"md md-home\"></i><span> Inicio </span></a>
                    </li>

                    ";
        // line 123
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_RECALLS")) {
            // line 124
            echo "                    <li>
                        <a href=\"";
            // line 125
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
            echo "\" class=\"waves-effect\"><i class=\"md md-web\"></i><span> Recall </span></a>
                    </li>

                    <li>
                        <a href=\"";
            // line 129
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_registros");
            echo "\" class=\"waves-effect\"><i class=\"md md-list\"></i><span> Registros </span></a>
                    </li>
                    ";
        }
        // line 132
        echo "
                    ";
        // line 133
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_CONCESIONARIOS")) {
            // line 134
            echo "                    <li>
                        ";
            // line 136
            echo "                    </li>
                    ";
        }
        // line 138
        echo "                </ul>
                <div class=\"clearfix\"></div>
            </div>
            <div class=\"clearfix\"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class=\"content-page\">
        <!-- Start content -->
        <div class=\"content\">
            <div class=\"container\">

                <!-- Page-Title -->
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <h4 class=\"pull-left page-title\">";
        // line 159
        $this->displayBlock('pagina', $context, $blocks);
        echo "</h4>
                        <ol class=\"breadcrumb pull-right\">
                            <li><a href=\"";
        // line 161
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_homepage");
        echo "\">Inicio</a></li>
                            ";
        // line 162
        $this->displayBlock('ubicacion', $context, $blocks);
        // line 164
        echo "                        </ol>
                    </div>
                </div>


            </div>
            ";
        // line 170
        $this->displayBlock('content', $context, $blocks);
        // line 172
        echo "        </div> <!-- content -->

        ";
        // line 174
        $this->displayBlock('footer', $context, $blocks);
        // line 179
        echo "
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/detect.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/fastclick.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.slimscroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.blockUI.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/waves.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/wow.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.scrollTo.min.js"), "html", null, true);
        echo "\"></script>

<!-- Datatables-->
<script src=\"";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/datatables/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/datatables/dataTables.bootstrap.js"), "html", null, true);
        echo "\"></script>


<!-- Datatable init js -->
<script src=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/pages/datatables.init.js"), "html", null, true);
        echo "\"></script>


<script src=\"";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/js/jquery.app.js"), "html", null, true);
        echo "\"></script>



<script type=\"text/javascript\" src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"), "html", null, true);
        echo "\"></script>

<!--form validation init-->
<script src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/summernote/dist/summernote.min.js"), "html", null, true);
        echo "\"></script>

<!-- Page Specific JS Libraries -->
<script src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/dropzone/dist/dropzone.js"), "html", null, true);
        echo "\"></script>

<!-- Galeria -->
<script type=\"text/javascript\" src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/isotope/dist/isotope.pkgd.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"), "html", null, true);
        echo "\"></script>

<!-- Sweet-Alert  -->
<script src=\"";
        // line 232
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/sweetalert/dist/sweetalert.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/pages/jquery.sweet-alert.init.js"), "html", null, true);
        echo "\"></script>


";
        // line 236
        $this->displayBlock('js', $context, $blocks);
        // line 238
        echo "
<script type=\"text/javascript\">
    \$(document).ready(function() {
        \$('#datatable').dataTable();
        \$('.summernote').summernote({
            height: 200,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: true                 // set focus to editable area after initializing summernote
        });
        ";
        // line 250
        $this->displayBlock('ready', $context, $blocks);
        // line 252
        echo "    } );
</script>

</body>
</html>";
        
        $__internal_08a6524ee4d8bd479ee5942683f007521eabbdcfa4f0366a6f2b0fd465176af5->leave($__internal_08a6524ee4d8bd479ee5942683f007521eabbdcfa4f0366a6f2b0fd465176af5_prof);

    }

    // line 26
    public function block_css($context, array $blocks = array())
    {
        $__internal_ca80dcb9438dad3c7c44a326bff59a575de181c746148cce784e72993fa4354a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca80dcb9438dad3c7c44a326bff59a575de181c746148cce784e72993fa4354a->enter($__internal_ca80dcb9438dad3c7c44a326bff59a575de181c746148cce784e72993fa4354a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "css"));

        // line 27
        echo "        <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("moltran/plugins/notifications/notification.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    ";
        
        $__internal_ca80dcb9438dad3c7c44a326bff59a575de181c746148cce784e72993fa4354a->leave($__internal_ca80dcb9438dad3c7c44a326bff59a575de181c746148cce784e72993fa4354a_prof);

    }

    // line 159
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_ba6ea4d951b1dfc92db210fd25f426b3150d2f01fbf5e8df77c47bc4b8d090a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba6ea4d951b1dfc92db210fd25f426b3150d2f01fbf5e8df77c47bc4b8d090a7->enter($__internal_ba6ea4d951b1dfc92db210fd25f426b3150d2f01fbf5e8df77c47bc4b8d090a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        
        $__internal_ba6ea4d951b1dfc92db210fd25f426b3150d2f01fbf5e8df77c47bc4b8d090a7->leave($__internal_ba6ea4d951b1dfc92db210fd25f426b3150d2f01fbf5e8df77c47bc4b8d090a7_prof);

    }

    // line 162
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_465738d404d7faa20f81a7a9863514462a324c567e287266b6ba4f6f315da29d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_465738d404d7faa20f81a7a9863514462a324c567e287266b6ba4f6f315da29d->enter($__internal_465738d404d7faa20f81a7a9863514462a324c567e287266b6ba4f6f315da29d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 163
        echo "                            ";
        
        $__internal_465738d404d7faa20f81a7a9863514462a324c567e287266b6ba4f6f315da29d->leave($__internal_465738d404d7faa20f81a7a9863514462a324c567e287266b6ba4f6f315da29d_prof);

    }

    // line 170
    public function block_content($context, array $blocks = array())
    {
        $__internal_1990293f228cde7603336acb98e167f8d14a25ccc208fbf47c7976bb7706281d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1990293f228cde7603336acb98e167f8d14a25ccc208fbf47c7976bb7706281d->enter($__internal_1990293f228cde7603336acb98e167f8d14a25ccc208fbf47c7976bb7706281d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 171
        echo "            ";
        
        $__internal_1990293f228cde7603336acb98e167f8d14a25ccc208fbf47c7976bb7706281d->leave($__internal_1990293f228cde7603336acb98e167f8d14a25ccc208fbf47c7976bb7706281d_prof);

    }

    // line 174
    public function block_footer($context, array $blocks = array())
    {
        $__internal_477e578ef6b8ebf9ac1bc594374565184e367d5c8547d3fd907b5687b6449649 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_477e578ef6b8ebf9ac1bc594374565184e367d5c8547d3fd907b5687b6449649->enter($__internal_477e578ef6b8ebf9ac1bc594374565184e367d5c8547d3fd907b5687b6449649_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 175
        echo "            <footer class=\"footer text-right\">
                2017 © Daimler Colombia.
            </footer>
        ";
        
        $__internal_477e578ef6b8ebf9ac1bc594374565184e367d5c8547d3fd907b5687b6449649->leave($__internal_477e578ef6b8ebf9ac1bc594374565184e367d5c8547d3fd907b5687b6449649_prof);

    }

    // line 236
    public function block_js($context, array $blocks = array())
    {
        $__internal_6924306c8a24879deb75daa01df300b8b2e112f20fbf3a230a61cacec90bf875 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6924306c8a24879deb75daa01df300b8b2e112f20fbf3a230a61cacec90bf875->enter($__internal_6924306c8a24879deb75daa01df300b8b2e112f20fbf3a230a61cacec90bf875_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        
        $__internal_6924306c8a24879deb75daa01df300b8b2e112f20fbf3a230a61cacec90bf875->leave($__internal_6924306c8a24879deb75daa01df300b8b2e112f20fbf3a230a61cacec90bf875_prof);

    }

    // line 250
    public function block_ready($context, array $blocks = array())
    {
        $__internal_49dab9f62b05df65c250b761ecf7d8654625ad4aa491890a3e0948b587a5983c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49dab9f62b05df65c250b761ecf7d8654625ad4aa491890a3e0948b587a5983c->enter($__internal_49dab9f62b05df65c250b761ecf7d8654625ad4aa491890a3e0948b587a5983c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ready"));

        // line 251
        echo "        ";
        
        $__internal_49dab9f62b05df65c250b761ecf7d8654625ad4aa491890a3e0948b587a5983c->leave($__internal_49dab9f62b05df65c250b761ecf7d8654625ad4aa491890a3e0948b587a5983c_prof);

    }

    public function getTemplateName()
    {
        return "base_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  537 => 251,  531 => 250,  520 => 236,  510 => 175,  504 => 174,  497 => 171,  491 => 170,  484 => 163,  478 => 162,  467 => 159,  457 => 27,  451 => 26,  440 => 252,  438 => 250,  424 => 238,  422 => 236,  416 => 233,  412 => 232,  406 => 229,  402 => 228,  396 => 225,  390 => 222,  384 => 219,  380 => 218,  373 => 214,  367 => 211,  360 => 207,  356 => 206,  350 => 203,  346 => 202,  342 => 201,  338 => 200,  334 => 199,  330 => 198,  326 => 197,  322 => 196,  318 => 195,  314 => 194,  297 => 179,  295 => 174,  291 => 172,  289 => 170,  281 => 164,  279 => 162,  275 => 161,  270 => 159,  247 => 138,  243 => 136,  240 => 134,  238 => 133,  235 => 132,  229 => 129,  222 => 125,  219 => 124,  217 => 123,  211 => 120,  201 => 113,  197 => 112,  191 => 109,  171 => 92,  143 => 67,  114 => 41,  108 => 38,  104 => 37,  100 => 36,  96 => 35,  92 => 34,  88 => 33,  84 => 32,  79 => 30,  76 => 29,  74 => 26,  69 => 24,  65 => 23,  59 => 20,  53 => 17,  47 => 14,  41 => 11,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\" />
    <title>Dashboard Daimler Colombia</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />
    <meta content=\"CMS para el proyecto Recoll de Daimler Colombia.\" name=\"description\" />
    <meta content=\"Coderthemes\" name=\"author\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />

    <link rel=\"shortcut icon\" href=\"{{ asset ('assets/favicon.ico') }}\">

    <!--venobox lightbox-->
    <link rel=\"stylesheet\" href=\"{{ asset ('moltran/plugins/magnific-popup/dist/magnific-popup.css') }}\">

    <!-- DataTables -->
    <link href=\"{{ asset ('moltran/plugins/datatables/jquery.dataTables.min.css') }}\" rel=\"stylesheet\" type=\"text/css\" />

    <!-- Dropzone css -->
    <link href=\"{{ asset ('moltran/plugins/dropzone/dist/dropzone.css') }}\" rel=\"stylesheet\" type=\"text/css\">

    <!--bootstrap-wysihtml5-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset ('moltran/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}\">
    <link href=\"{{ asset ('moltran/plugins/summernote/dist/summernote.css') }}\" rel=\"stylesheet\">

    {% block css %}
        <link href=\"{{ asset('moltran/plugins/notifications/notification.css') }}\" rel=\"stylesheet\">
    {% endblock %}

    <link href=\"{{ asset ('moltran/plugins/sweetalert/dist/sweetalert.css') }}\" rel=\"stylesheet\" type=\"text/css\">

    <link href=\"{{ asset ('moltran/css/bootstrap.min.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset ('moltran/css/core.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset ('moltran/css/icons.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset ('moltran/css/components.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset ('moltran/css/pages.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset ('moltran/css/menu.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset ('moltran/css/responsive.css') }}\" rel=\"stylesheet\" type=\"text/css\">


    <script src=\"{{ asset ('moltran/js/modernizr.min.js') }}\"></script>
    <script>
        var nuevaImagen = '';
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js') }}/1.3.0/respond.min.js\"></script>
    <![endif]-->


</head>


<body class=\"fixed-left\">

<!-- Begin page -->
<div id=\"wrapper\">

    <!-- Top Bar Start -->
    <div class=\"topbar\">
        <!-- LOGO -->
        <div class=\"topbar-left\">
            <div class=\"text-center\">
                <a href=\"{{ path ('admin_homepage') }}\" class=\"logo\"><i class=\"md md-domain text-primary\" style=\"color: slategrey;\"></i> <span>Dashboard </span></a>
            </div>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class=\"navbar navbar-default\" role=\"navigation\">
            <div class=\"container\">
                <div class=\"\">
                    <div class=\"pull-left\">
                        <button type=\"button\" class=\"button-menu-mobile open-left\">
                            <i class=\"fa fa-bars\"></i>
                        </button>
                        <span class=\"clearfix\"></span>
                    </div>
                    <!--<form class=\"navbar-form pull-left\" role=\"search\">
                        <div class=\"form-group\">
                            <input type=\"text\" class=\"form-control search-bar\" placeholder=\"Type here for search...\">
                        </div>
                        <button type=\"submit\" class=\"btn btn-search\"><i class=\"fa fa-search\"></i></button>
                    </form>-->

                    <ul class=\"nav navbar-nav navbar-right pull-right\">
                        <li class=\"hidden-xs\">
                            <a href=\"#\" id=\"btn-fullscreen\" class=\"waves-effect\"><i class=\"md md-crop-free\"></i></a>
                        </li>
                        <li class=\"hidden-xs\">
                            <a href=\"{{ path('logout') }}\" class=\"waves-effect\"><i class=\"md md-exit-to-app\"></i></a>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class=\"left side-menu\">
        <div class=\"sidebar-inner slimscrollleft\">
            <div class=\"user-details\">
                <div class=\"pull-left\">
                    <img src=\"{{ asset ('assets/daimler_logo_claro.png') }}\" alt=\"\" class=\"thumb-md img-circle\">
                </div>
                <div class=\"user-info\">
                    <p class=\"text-muted\"><b>{{ app.user.username|upper }}</b></p>
                    <p class=\"text-muted m-0\">Rol: {{ app.user.roles[0]|replace({'ROLE_':''})|lower }}</p>
                </div>
            </div>
            <!--- Divider -->
            <div id=\"sidebar-menu\">
                <ul>
                    <li>
                        <a href=\"{{ path ('admin_homepage') }}\" class=\"waves-effect\"><i class=\"md md-home\"></i><span> Inicio </span></a>
                    </li>

                    {% if is_granted('ROLE_RECALLS') %}
                    <li>
                        <a href=\"{{ path('admin_recalls_index') }}\" class=\"waves-effect\"><i class=\"md md-web\"></i><span> Recall </span></a>
                    </li>

                    <li>
                        <a href=\"{{ path('admin_registros') }}\" class=\"waves-effect\"><i class=\"md md-list\"></i><span> Registros </span></a>
                    </li>
                    {% endif %}

                    {% if is_granted('ROLE_CONCESIONARIOS') %}
                    <li>
                        {#<a href=\"{{ path('admin_ciudades_index') }}\" class=\"waves-effect\"><i class=\"md md-location-city\"></i><span> Concesionarios </span></a>#}
                    </li>
                    {% endif %}
                </ul>
                <div class=\"clearfix\"></div>
            </div>
            <div class=\"clearfix\"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class=\"content-page\">
        <!-- Start content -->
        <div class=\"content\">
            <div class=\"container\">

                <!-- Page-Title -->
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <h4 class=\"pull-left page-title\">{% block pagina %}{% endblock %}</h4>
                        <ol class=\"breadcrumb pull-right\">
                            <li><a href=\"{{ path ('admin_homepage') }}\">Inicio</a></li>
                            {% block ubicacion %}
                            {% endblock %}
                        </ol>
                    </div>
                </div>


            </div>
            {% block content %}
            {% endblock %}
        </div> <!-- content -->

        {% block footer %}
            <footer class=\"footer text-right\">
                2017 © Daimler Colombia.
            </footer>
        {% endblock %}

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- Main  -->
<script src=\"{{ asset ('moltran/js/jquery.min.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/bootstrap.min.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/detect.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/fastclick.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/jquery.slimscroll.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/jquery.blockUI.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/waves.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/wow.min.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/jquery.nicescroll.js') }}\"></script>
<script src=\"{{ asset ('moltran/js/jquery.scrollTo.min.js') }}\"></script>

<!-- Datatables-->
<script src=\"{{ asset ('moltran/plugins/datatables/jquery.dataTables.min.js') }}\"></script>
<script src=\"{{ asset ('moltran/plugins/datatables/dataTables.bootstrap.js') }}\"></script>


<!-- Datatable init js -->
<script src=\"{{ asset ('moltran/pages/datatables.init.js') }}\"></script>


<script src=\"{{ asset ('moltran/js/jquery.app.js') }}\"></script>



<script type=\"text/javascript\" src=\"{{ asset ('moltran/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset ('moltran/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}\"></script>

<!--form validation init-->
<script src=\"{{ asset ('moltran/plugins/summernote/dist/summernote.min.js') }}\"></script>

<!-- Page Specific JS Libraries -->
<script src=\"{{ asset ('moltran/plugins/dropzone/dist/dropzone.js') }}\"></script>

<!-- Galeria -->
<script type=\"text/javascript\" src=\"{{ asset ('moltran/plugins/isotope/dist/isotope.pkgd.min.js') }}\"></script>
<script type=\"text/javascript\" src=\"{{ asset ('moltran/plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}\"></script>

<!-- Sweet-Alert  -->
<script src=\"{{ asset ('moltran/plugins/sweetalert/dist/sweetalert.min.js') }}\"></script>
<script src=\"{{ asset ('moltran/pages/jquery.sweet-alert.init.js') }}\"></script>


{% block js %}
{% endblock %}

<script type=\"text/javascript\">
    \$(document).ready(function() {
        \$('#datatable').dataTable();
        \$('.summernote').summernote({
            height: 200,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: true                 // set focus to editable area after initializing summernote
        });
        {% block ready %}
        {% endblock %}
    } );
</script>

</body>
</html>", "base_admin.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/base_admin.html.twig");
    }
}
