<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_5809e0b84e5648fefc5a58ba549e82d5ccd156e6f2342d6f256192433ec75f38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66ab1d947dc20a21e6d6b9de9a6aa21d43ff399992ed94a93a97a87770c644dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66ab1d947dc20a21e6d6b9de9a6aa21d43ff399992ed94a93a97a87770c644dc->enter($__internal_66ab1d947dc20a21e6d6b9de9a6aa21d43ff399992ed94a93a97a87770c644dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_66ab1d947dc20a21e6d6b9de9a6aa21d43ff399992ed94a93a97a87770c644dc->leave($__internal_66ab1d947dc20a21e6d6b9de9a6aa21d43ff399992ed94a93a97a87770c644dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
