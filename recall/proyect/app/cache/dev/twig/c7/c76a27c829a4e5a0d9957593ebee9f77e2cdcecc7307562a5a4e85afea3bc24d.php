<?php

/* :ciudades:show.html.twig */
class __TwigTemplate_4edc7d7de28c85ed9b561054062ae600088f0f19a087c62b0421cf6abad4f0cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":ciudades:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21ab8b9a93a86193ee70a087cc4b774905113b155bdc4eb84f235f1b57783cdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21ab8b9a93a86193ee70a087cc4b774905113b155bdc4eb84f235f1b57783cdf->enter($__internal_21ab8b9a93a86193ee70a087cc4b774905113b155bdc4eb84f235f1b57783cdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":ciudades:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_21ab8b9a93a86193ee70a087cc4b774905113b155bdc4eb84f235f1b57783cdf->leave($__internal_21ab8b9a93a86193ee70a087cc4b774905113b155bdc4eb84f235f1b57783cdf_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_79bd0a39397470c536a838e9528882e4f7b36eaea5a43c112f7a7f930c5cc094 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79bd0a39397470c536a838e9528882e4f7b36eaea5a43c112f7a7f930c5cc094->enter($__internal_79bd0a39397470c536a838e9528882e4f7b36eaea5a43c112f7a7f930c5cc094_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Ciudade</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ciudade"] ?? $this->getContext($context, "ciudade")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ciudade"] ?? $this->getContext($context, "ciudade")), "nombre", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["ciudade"] ?? $this->getContext($context, "ciudade")), "descripcion", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_edit", array("id" => $this->getAttribute(($context["ciudade"] ?? $this->getContext($context, "ciudade")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 31
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 33
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_79bd0a39397470c536a838e9528882e4f7b36eaea5a43c112f7a7f930c5cc094->leave($__internal_79bd0a39397470c536a838e9528882e4f7b36eaea5a43c112f7a7f930c5cc094_prof);

    }

    public function getTemplateName()
    {
        return ":ciudades:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 33,  84 => 31,  78 => 28,  72 => 25,  62 => 18,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Ciudade</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ ciudade.id }}</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>{{ ciudade.nombre }}</td>
            </tr>
            <tr>
                <th>Descripcion</th>
                <td>{{ ciudade.descripcion }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('admin_ciudades_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('admin_ciudades_edit', { 'id': ciudade.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":ciudades:show.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/ciudades/show.html.twig");
    }
}
