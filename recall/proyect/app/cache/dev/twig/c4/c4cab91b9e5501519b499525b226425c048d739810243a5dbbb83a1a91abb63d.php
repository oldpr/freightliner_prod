<?php

/* AdminBundle:Default:subirCodigosRecall.html.twig */
class __TwigTemplate_2a1b153264c6cf91ecb1ff64b5c054d883f5b05a8ae7287f5c0ff6cc61c07884 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:subirCodigosRecall.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78d71d5c347a45ac2f0914d2bf6d18226e0bd10448ca397c66aea620f4394b17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78d71d5c347a45ac2f0914d2bf6d18226e0bd10448ca397c66aea620f4394b17->enter($__internal_78d71d5c347a45ac2f0914d2bf6d18226e0bd10448ca397c66aea620f4394b17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Default:subirCodigosRecall.html.twig"));

        // line 3
        $context["nomPagina"] = (("Subir códigos a Recoll \"" . $this->getAttribute(($context["entidad"] ?? $this->getContext($context, "entidad")), "nombre", array())) . "\"");
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78d71d5c347a45ac2f0914d2bf6d18226e0bd10448ca397c66aea620f4394b17->leave($__internal_78d71d5c347a45ac2f0914d2bf6d18226e0bd10448ca397c66aea620f4394b17_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_9a88c6bf384c71bff841eb5bd5ffba8d1d293e5954816c44cd1d3812d850def9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a88c6bf384c71bff841eb5bd5ffba8d1d293e5954816c44cd1d3812d850def9->enter($__internal_9a88c6bf384c71bff841eb5bd5ffba8d1d293e5954816c44cd1d3812d850def9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_9a88c6bf384c71bff841eb5bd5ffba8d1d293e5954816c44cd1d3812d850def9->leave($__internal_9a88c6bf384c71bff841eb5bd5ffba8d1d293e5954816c44cd1d3812d850def9_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_33e64ee6ffe4eb6c476717e59001431eff9d0bfb853b22f8580ff0c472bc381e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33e64ee6ffe4eb6c476717e59001431eff9d0bfb853b22f8580ff0c472bc381e->enter($__internal_33e64ee6ffe4eb6c476717e59001431eff9d0bfb853b22f8580ff0c472bc381e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Recalls</a></li>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_edit", array("id" => $this->getAttribute(($context["entidad"] ?? $this->getContext($context, "entidad")), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["entidad"] ?? $this->getContext($context, "entidad")), "nombre", array()), "html", null, true);
        echo "</a></li>
    <li class=\"active\">";
        // line 8
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_33e64ee6ffe4eb6c476717e59001431eff9d0bfb853b22f8580ff0c472bc381e->leave($__internal_33e64ee6ffe4eb6c476717e59001431eff9d0bfb853b22f8580ff0c472bc381e_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_6ba4c531970217f89882a8cb4d4fb91e1aaea0eebbb99e54dac7267b0434c874 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ba4c531970217f89882a8cb4d4fb91e1aaea0eebbb99e54dac7267b0434c874->enter($__internal_6ba4c531970217f89882a8cb4d4fb91e1aaea0eebbb99e54dac7267b0434c874_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    ";
        if (array_key_exists("aux", $context)) {
            // line 13
            echo "        ";
            echo twig_escape_filter($this->env, ($context["aux"] ?? $this->getContext($context, "aux")), "html", null, true);
            echo "
    ";
        }
        // line 15
        echo "    <!-- container -->
    ";
        // line 16
        if ((array_key_exists("error", $context) && (($context["error"] ?? $this->getContext($context, "error")) != null))) {
            // line 17
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: ";
            // line 19
            echo twig_escape_filter($this->env, ($context["error"] ?? $this->getContext($context, "error")), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 22
        echo "    <div class=\"alert alert-warning \">
        <b>Advertencia!</b> Tenga en cuenta que al cargar un nuevo archivo los codigos VIN actuales se borraran de forma permanente.
    </div>
    ";
        // line 25
        if (( !array_key_exists("success", $context) || (($context["success"] ?? $this->getContext($context, "success")) == false))) {
            // line 26
            echo "        <div class=\"row\">
            <!-- Basic example -->
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\"><h3 class=\"panel-title\">Paso 1 - Cargue el archivo CSV o TXT</h3></div>
                    <div class=\"panel-body\">
                        <form method=\"post\" enctype=\"multipart/form-data\" action=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subirCodigosRecall", array("idRecall" => $this->getAttribute(($context["entidad"] ?? $this->getContext($context, "entidad")), "id", array()))), "html", null, true);
            echo "\"  role=\"form\">
                            <div class=\"form-group\">
                                <!-- MAX_FILE_SIZE debe preceder al campo de entrada del fichero -->
                                <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"20000\" />
                                <label for=\"archivo\">Ingrese el archivo</label>
                                <input type=\"file\" name=\"fichero_usuario\" accept=\"text/*, .csv\"  class=\"form-control\" id=\"archivo\" required>
                            </div>
                            <div class=\"form-group\">
                                <div class=\"radio radio-info\">
                                    <input name=\"caracter\" value=\",\" id=\"radio1\" type=\"radio\" checked>
                                    <label for=\"radio1\">
                                        CSV separado por comas ( , )
                                    </label>
                                    <br>
                                    <input name=\"caracter\" value=\";\" id=\"radio2\" type=\"radio\">
                                    <label for=\"radio2\">
                                        CSV separado por punto y coma ( ; )
                                    </label>
                                </div>
                            </div>
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Cargar CSV</button>
                        </form>
                    </div><!-- panel-body -->
                </div> <!-- panel -->
            </div> <!-- col-->
        </div>
    ";
        }
        // line 59
        echo "    ";
        if ((array_key_exists("paso2", $context) && ($context["paso2"] ?? $this->getContext($context, "paso2")))) {
            // line 60
            echo "        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\">Paso 2 - Verifique la información</h3>
                    </div>
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                                <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>Codigo</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["codigos"] ?? $this->getContext($context, "codigos")));
            foreach ($context['_seq'] as $context["_key"] => $context["codigo"]) {
                if ((($context["codigos"] ?? $this->getContext($context, "codigos")) != null)) {
                    // line 79
                    echo "                                        <tr>
                                                <td>";
                    // line 80
                    echo twig_escape_filter($this->env, $context["codigo"], "html", null, true);
                    echo "</td>
                                        </tr>
                                    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['codigo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- End Row -->

        ";
            // line 94
            if (( !array_key_exists("success", $context) || (($context["success"] ?? $this->getContext($context, "success")) == false))) {
                // line 95
                echo "            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">Paso 3 - Actualice los códigos</h3>
                        </div>
                        <form  method=\"post\" action=\"";
                // line 101
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subirCodigosRecall", array("idRecall" => $this->getAttribute(($context["entidad"] ?? $this->getContext($context, "entidad")), "id", array()))), "html", null, true);
                echo "\">
                            <input type=\"hidden\" name=\"codigos\" value=\"";
                // line 102
                echo twig_escape_filter($this->env, twig_jsonencode_filter(($context["codigos"] ?? $this->getContext($context, "codigos"))), "html", null, true);
                echo "\">
                            <div class=\"panel-body\">
                                <button type=\"submit\" class=\"btn btn-primary waves-effect waves-light\">Actualizar Códigos</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        ";
            }
            // line 111
            echo "    ";
        }
        // line 112
        echo "
";
        
        $__internal_6ba4c531970217f89882a8cb4d4fb91e1aaea0eebbb99e54dac7267b0434c874->leave($__internal_6ba4c531970217f89882a8cb4d4fb91e1aaea0eebbb99e54dac7267b0434c874_prof);

    }

    // line 115
    public function block_js($context, array $blocks = array())
    {
        $__internal_d4ca69c06c8f44cee2dd978b54d79c4e04a7b6ef59eebf39f438cbedf7143f88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4ca69c06c8f44cee2dd978b54d79c4e04a7b6ef59eebf39f438cbedf7143f88->enter($__internal_d4ca69c06c8f44cee2dd978b54d79c4e04a7b6ef59eebf39f438cbedf7143f88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js"));

        
        $__internal_d4ca69c06c8f44cee2dd978b54d79c4e04a7b6ef59eebf39f438cbedf7143f88->leave($__internal_d4ca69c06c8f44cee2dd978b54d79c4e04a7b6ef59eebf39f438cbedf7143f88_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:subirCodigosRecall.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 115,  236 => 112,  233 => 111,  221 => 102,  217 => 101,  209 => 95,  207 => 94,  194 => 83,  184 => 80,  181 => 79,  176 => 78,  156 => 60,  153 => 59,  123 => 32,  115 => 26,  113 => 25,  108 => 22,  102 => 19,  98 => 17,  96 => 16,  93 => 15,  87 => 13,  84 => 12,  78 => 11,  69 => 8,  63 => 7,  58 => 6,  52 => 5,  40 => 4,  33 => 1,  31 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Subir códigos a Recoll \"'~entidad.nombre~'\"' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li><a href=\"{{ path('admin_recalls_index') }}\">Recalls</a></li>
    <li><a href=\"{{ path('admin_recalls_edit', { 'id': entidad.id }) }}\">{{ entidad.nombre }}</a></li>
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}
    {% if aux is defined %}
        {{ aux }}
    {% endif %}
    <!-- container -->
    {% if error is defined and error != null %}
        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: {{ error }}.
        </div>
    {% endif %}
    <div class=\"alert alert-warning \">
        <b>Advertencia!</b> Tenga en cuenta que al cargar un nuevo archivo los codigos VIN actuales se borraran de forma permanente.
    </div>
    {% if success is not defined or success == false %}
        <div class=\"row\">
            <!-- Basic example -->
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\"><h3 class=\"panel-title\">Paso 1 - Cargue el archivo CSV o TXT</h3></div>
                    <div class=\"panel-body\">
                        <form method=\"post\" enctype=\"multipart/form-data\" action=\"{{ path('subirCodigosRecall', {'idRecall':entidad.id}) }}\"  role=\"form\">
                            <div class=\"form-group\">
                                <!-- MAX_FILE_SIZE debe preceder al campo de entrada del fichero -->
                                <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"20000\" />
                                <label for=\"archivo\">Ingrese el archivo</label>
                                <input type=\"file\" name=\"fichero_usuario\" accept=\"text/*, .csv\"  class=\"form-control\" id=\"archivo\" required>
                            </div>
                            <div class=\"form-group\">
                                <div class=\"radio radio-info\">
                                    <input name=\"caracter\" value=\",\" id=\"radio1\" type=\"radio\" checked>
                                    <label for=\"radio1\">
                                        CSV separado por comas ( , )
                                    </label>
                                    <br>
                                    <input name=\"caracter\" value=\";\" id=\"radio2\" type=\"radio\">
                                    <label for=\"radio2\">
                                        CSV separado por punto y coma ( ; )
                                    </label>
                                </div>
                            </div>
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Cargar CSV</button>
                        </form>
                    </div><!-- panel-body -->
                </div> <!-- panel -->
            </div> <!-- col-->
        </div>
    {% endif %}
    {% if paso2 is defined and paso2 %}
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\">Paso 2 - Verifique la información</h3>
                    </div>
                    <div class=\"panel-body\">
                        <div class=\"row\">
                            <div class=\"col-md-12 col-sm-12 col-xs-12\">
                                <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                    <thead>
                                    <tr>
                                        <th>Codigo</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    {% for codigo in codigos if codigos != null %}
                                        <tr>
                                                <td>{{ codigo }}</td>
                                        </tr>
                                    {% endfor %}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- End Row -->

        {% if success is not defined or success == false %}
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title\">Paso 3 - Actualice los códigos</h3>
                        </div>
                        <form  method=\"post\" action=\"{{ path('subirCodigosRecall', {'idRecall':entidad.id}) }}\">
                            <input type=\"hidden\" name=\"codigos\" value=\"{{ codigos|json_encode() }}\">
                            <div class=\"panel-body\">
                                <button type=\"submit\" class=\"btn btn-primary waves-effect waves-light\">Actualizar Códigos</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        {% endif %}
    {% endif %}

{% endblock %}

{% block js %}
{% endblock %}", "AdminBundle:Default:subirCodigosRecall.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/subirCodigosRecall.html.twig");
    }
}
