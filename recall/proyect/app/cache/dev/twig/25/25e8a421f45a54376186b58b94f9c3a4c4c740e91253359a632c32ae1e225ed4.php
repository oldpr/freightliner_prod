<?php

/* AdminBundle:Default:registros.html.twig */
class __TwigTemplate_dcdd0d71bca822b8048bc5d6de5ce4acd2908de13121c846af3c5fa5e94d9371 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:registros.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_341103343e40c45c0fb0b8d74fe40073a892286117653ac2fcca8d37b75b13ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_341103343e40c45c0fb0b8d74fe40073a892286117653ac2fcca8d37b75b13ba->enter($__internal_341103343e40c45c0fb0b8d74fe40073a892286117653ac2fcca8d37b75b13ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AdminBundle:Default:registros.html.twig"));

        // line 3
        $context["nomPagina"] = "Registros";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_341103343e40c45c0fb0b8d74fe40073a892286117653ac2fcca8d37b75b13ba->leave($__internal_341103343e40c45c0fb0b8d74fe40073a892286117653ac2fcca8d37b75b13ba_prof);

    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        $__internal_84cecbdfbed35c317c8bc7b7711100330fff93e402d3a0df83dd6844f1525cdc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84cecbdfbed35c317c8bc7b7711100330fff93e402d3a0df83dd6844f1525cdc->enter($__internal_84cecbdfbed35c317c8bc7b7711100330fff93e402d3a0df83dd6844f1525cdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pagina"));

        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        
        $__internal_84cecbdfbed35c317c8bc7b7711100330fff93e402d3a0df83dd6844f1525cdc->leave($__internal_84cecbdfbed35c317c8bc7b7711100330fff93e402d3a0df83dd6844f1525cdc_prof);

    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        $__internal_5cbc344c84023e2564785014f38ef41da8ad44f3f790ddcd5a6d1af4ab9883fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5cbc344c84023e2564785014f38ef41da8ad44f3f790ddcd5a6d1af4ab9883fe->enter($__internal_5cbc344c84023e2564785014f38ef41da8ad44f3f790ddcd5a6d1af4ab9883fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "ubicacion"));

        // line 6
        echo "    <li class=\"active\">";
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? $this->getContext($context, "nomPagina")), "html", null, true);
        echo "</li>
";
        
        $__internal_5cbc344c84023e2564785014f38ef41da8ad44f3f790ddcd5a6d1af4ab9883fe->leave($__internal_5cbc344c84023e2564785014f38ef41da8ad44f3f790ddcd5a6d1af4ab9883fe_prof);

    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        $__internal_4c6a0e04692266a51b05a3a10bbd4a148bf25ee80375b24eadca8edb53bd6b1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c6a0e04692266a51b05a3a10bbd4a148bf25ee80375b24eadca8edb53bd6b1d->enter($__internal_4c6a0e04692266a51b05a3a10bbd4a148bf25ee80375b24eadca8edb53bd6b1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 10
        echo "    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Registros</h3>
                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Codigo VIN</th>
                                    <th>Fecha</th>
                                    <th>Nombres</th>
                                    <th>Correo</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["registros"] ?? $this->getContext($context, "registros")));
        foreach ($context['_seq'] as $context["_key"] => $context["registro"]) {
            // line 34
            echo "                                    <tr>
                                        <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["registro"], "id", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["registro"], "codigo", array()), "codigo", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["registro"], "fecha", array())), "html", null, true);
            echo "</td>
                                        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["registro"], "nombres", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["registro"], "correo", array()), "html", null, true);
            echo "</td>
                                        <td>
                                            ";
            // line 41
            if ((((($this->getAttribute($context["registro"], "nombres", array()) != "") || ($this->getAttribute($context["registro"], "apellidos", array()) != "")) || ($this->getAttribute($context["registro"], "telefono", array()) != "")) || ($this->getAttribute($context["registro"], "correo", array()) != ""))) {
                // line 42
                echo "                                                <a class=\"btn btn-primary\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_registro", array("idRegistro" => $this->getAttribute($context["registro"], "id", array()))), "html", null, true);
                echo "\">Ver Formulario</a>
                                            ";
            } else {
                // line 44
                echo "                                                No envio Formulario
                                            ";
            }
            // line 46
            echo "                                        </td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['registro'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- End Row -->
";
        
        $__internal_4c6a0e04692266a51b05a3a10bbd4a148bf25ee80375b24eadca8edb53bd6b1d->leave($__internal_4c6a0e04692266a51b05a3a10bbd4a148bf25ee80375b24eadca8edb53bd6b1d_prof);

    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:registros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 49,  138 => 46,  134 => 44,  128 => 42,  126 => 41,  121 => 39,  117 => 38,  113 => 37,  109 => 36,  105 => 35,  102 => 34,  98 => 33,  73 => 10,  67 => 9,  57 => 6,  51 => 5,  39 => 4,  32 => 1,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_admin.html.twig' %}
{# TODO Cabiar nomPagina #}
{% set nomPagina = 'Registros' %}
{% block pagina %}{{ nomPagina }}{% endblock %}
{% block ubicacion %}
    <li class=\"active\">{{ nomPagina }}</li>
{% endblock %}

{% block content %}
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Registros</h3>
                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Codigo VIN</th>
                                    <th>Fecha</th>
                                    <th>Nombres</th>
                                    <th>Correo</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>


                                <tbody>
                                {% for registro in registros %}
                                    <tr>
                                        <td>{{ registro.id }}</td>
                                        <td>{{ registro.codigo.codigo }}</td>
                                        <td>{{ registro.fecha|date }}</td>
                                        <td>{{ registro.nombres }}</td>
                                        <td>{{ registro.correo }}</td>
                                        <td>
                                            {% if registro.nombres != '' or registro.apellidos != '' or registro.telefono != '' or registro.correo != '' %}
                                                <a class=\"btn btn-primary\" href=\"{{ path('admin_registro', {idRegistro:registro.id}) }}\">Ver Formulario</a>
                                            {% else %}
                                                No envio Formulario
                                            {% endif %}
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- End Row -->
{% endblock %}", "AdminBundle:Default:registros.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/registros.html.twig");
    }
}
