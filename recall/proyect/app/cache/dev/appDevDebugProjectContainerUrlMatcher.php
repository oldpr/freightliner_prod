<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            // admin_homepage
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_homepage');
                }

                return array (  '_controller' => 'AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'admin_homepage',);
            }

            if (0 === strpos($pathinfo, '/admin/log')) {
                // login
                if ($pathinfo === '/admin/login') {
                    return array (  '_controller' => 'AdminBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
                }

                // logout
                if ($pathinfo === '/admin/logout') {
                    return array('_route' => 'logout');
                }

            }

            if (0 === strpos($pathinfo, '/admin/re')) {
                // subirCodigosRecall
                if (0 === strpos($pathinfo, '/admin/recall') && preg_match('#^/admin/recall/(?P<idRecall>[^/]++)/codigos$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'subirCodigosRecall')), array (  '_controller' => 'AdminBundle\\Controller\\DefaultController::subirCodigosRecallAction',));
                }

                if (0 === strpos($pathinfo, '/admin/registro')) {
                    // admin_registros
                    if ($pathinfo === '/admin/registros') {
                        return array (  '_controller' => 'AdminBundle\\Controller\\DefaultController::registrosAction',  '_route' => 'admin_registros',);
                    }

                    // admin_registro
                    if (preg_match('#^/admin/registro/(?P<idRegistro>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_registro')), array (  '_controller' => 'AdminBundle\\Controller\\DefaultController::registroAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin/recalls')) {
                    // admin_recalls_index
                    if (rtrim($pathinfo, '/') === '/admin/recalls') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_recalls_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admin_recalls_index');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\RecallsController::indexAction',  '_route' => 'admin_recalls_index',);
                    }
                    not_admin_recalls_index:

                    // admin_recalls_show
                    if (preg_match('#^/admin/recalls/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_recalls_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_recalls_show')), array (  '_controller' => 'AppBundle\\Controller\\RecallsController::showAction',));
                    }
                    not_admin_recalls_show:

                    // admin_recalls_new
                    if ($pathinfo === '/admin/recalls/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_recalls_new;
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\RecallsController::newAction',  '_route' => 'admin_recalls_new',);
                    }
                    not_admin_recalls_new:

                    // admin_recalls_edit
                    if (preg_match('#^/admin/recalls/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_recalls_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_recalls_edit')), array (  '_controller' => 'AppBundle\\Controller\\RecallsController::editAction',));
                    }
                    not_admin_recalls_edit:

                    // admin_recalls_delete
                    if (preg_match('#^/admin/recalls/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admin_recalls_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_recalls_delete')), array (  '_controller' => 'AppBundle\\Controller\\RecallsController::deleteAction',));
                    }
                    not_admin_recalls_delete:

                }

            }

            if (0 === strpos($pathinfo, '/admin/c')) {
                if (0 === strpos($pathinfo, '/admin/ciudades')) {
                    // admin_ciudades_index
                    if (rtrim($pathinfo, '/') === '/admin/ciudades') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_ciudades_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admin_ciudades_index');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\CiudadesController::indexAction',  '_route' => 'admin_ciudades_index',);
                    }
                    not_admin_ciudades_index:

                    // admin_ciudades_show
                    if (preg_match('#^/admin/ciudades/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_ciudades_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ciudades_show')), array (  '_controller' => 'AppBundle\\Controller\\CiudadesController::showAction',));
                    }
                    not_admin_ciudades_show:

                    // admin_ciudades_new
                    if ($pathinfo === '/admin/ciudades/new') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_ciudades_new;
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\CiudadesController::newAction',  '_route' => 'admin_ciudades_new',);
                    }
                    not_admin_ciudades_new:

                    // admin_ciudades_edit
                    if (preg_match('#^/admin/ciudades/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_ciudades_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ciudades_edit')), array (  '_controller' => 'AppBundle\\Controller\\CiudadesController::editAction',));
                    }
                    not_admin_ciudades_edit:

                    // admin_ciudades_delete
                    if (preg_match('#^/admin/ciudades/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admin_ciudades_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_ciudades_delete')), array (  '_controller' => 'AppBundle\\Controller\\CiudadesController::deleteAction',));
                    }
                    not_admin_ciudades_delete:

                }

                if (0 === strpos($pathinfo, '/admin/concesionarios')) {
                    // admin_concesionarios_index
                    if (rtrim($pathinfo, '/') === '/admin/concesionarios') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_concesionarios_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'admin_concesionarios_index');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\ConcesionariosController::indexAction',  '_route' => 'admin_concesionarios_index',);
                    }
                    not_admin_concesionarios_index:

                    // admin_concesionarios_show
                    if (preg_match('#^/admin/concesionarios/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_concesionarios_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_concesionarios_show')), array (  '_controller' => 'AppBundle\\Controller\\ConcesionariosController::showAction',));
                    }
                    not_admin_concesionarios_show:

                    // admin_concesionarios_new
                    if (preg_match('#^/admin/concesionarios/(?P<id>[^/]++)/new$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_concesionarios_new;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_concesionarios_new')), array (  '_controller' => 'AppBundle\\Controller\\ConcesionariosController::newAction',));
                    }
                    not_admin_concesionarios_new:

                    // admin_concesionarios_edit
                    if (preg_match('#^/admin/concesionarios/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_concesionarios_edit;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_concesionarios_edit')), array (  '_controller' => 'AppBundle\\Controller\\ConcesionariosController::editAction',));
                    }
                    not_admin_concesionarios_edit:

                    // admin_concesionarios_delete
                    if (preg_match('#^/admin/concesionarios/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_admin_concesionarios_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_concesionarios_delete')), array (  '_controller' => 'AppBundle\\Controller\\ConcesionariosController::deleteAction',));
                    }
                    not_admin_concesionarios_delete:

                }

            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_homepage;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }
        not_homepage:

        // queEsCodigoVin
        if ($pathinfo === '/que-es-un-codigo-vin-o-fin') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_queEsCodigoVin;
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::queEsCodigoVinAction',  '_route' => 'queEsCodigoVin',);
        }
        not_queEsCodigoVin:

        // buscarCodigo
        if ($pathinfo === '/codigo/busqueda') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD', 'POST'))) {
                $allow = array_merge($allow, array('GET', 'HEAD', 'POST'));
                goto not_buscarCodigo;
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::buscarCodigoAction',  '_route' => 'buscarCodigo',);
        }
        not_buscarCodigo:

        // guardarRegistro
        if ($pathinfo === '/registro/datos-enviados-con-exito') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_guardarRegistro;
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::guardarRegistroAction',  '_route' => 'guardarRegistro',);
        }
        not_guardarRegistro:

        // concesionarios
        if ($pathinfo === '/concesionarios') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_concesionarios;
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::concesionariosAction',  '_route' => 'concesionarios',);
        }
        not_concesionarios:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
