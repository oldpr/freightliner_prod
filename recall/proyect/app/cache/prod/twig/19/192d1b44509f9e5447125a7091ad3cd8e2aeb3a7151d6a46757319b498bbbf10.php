<?php

/* AppBundle:Default:datosEnviados.html.twig */
class __TwigTemplate_b3c1213b909df494b9230a4dae0eb77dbbca6a1fbff8d910bcb9fc9b9120d608 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:datosEnviados.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>

    <div class=\"content\">
        <div class=\"form-contact\">
            <h2>EL CÓDIGO APLICA</h2>
            <p>";
        // line 11
        echo $this->getAttribute($this->getAttribute(($context["codigo"] ?? null), "recall", array()), "informacion", array());
        echo "</p>

            <div class=\"default-box\">
                <a class=\"boton boton-medio\" id=\"red\">red de concesionarios</a>
                <a href=\"http://freightliner.com.co\" class=\"boton boton-medio boton-negro\">IR AL SITIO WEB</a>
            </div>
        </div>
        <div class=\"content info-concesionarios\" id=\"concesionarios\">
            <p>Visite nuestra Red de Concesionarios a nivel nacional y conozca todos los servicios y beneficios que tenemos para usted. Seleccione la ciudad donde se encuentra y descubra toda la información que necesita del centro de servicio más cercano.</p>
            <form class=\"form-select pull-left\">
                <select id=\"selCiudad\">
                    <option selected value=\"Barranquilla\">Barranquilla</option>
                    <option value=\"Bogota\">Bogotá</option>
                    <option value=\"Bucaramanga\">Bucaramanga</option>
                    <option value=\"Cali\">Cali</option>
                    <option value=\"Cucuta\">Cúcuta</option>
                    <option value=\"Duitama\">Duitama</option>
                    <option value=\"Ibague\">Ibagué</option>
                    <option value=\"Medellin\">Medellín</option>
                    <option value=\"Neiva\">Neiva</option>
                    <option value=\"Pereira\">Pereira</option>
                </select>
            </form>
            <ul class=\"ciudades\" id=\"Barranquilla\">
                <li>
                    <h2>Barranquilla</h2>
                    <div>
                        <p><b>Alemana Automotriz</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Vía  40 #77B 59</p>
                        <p><b>Tel:</b>(5) 3773159</p>
                    </div>
                </li>
            </ul>
            <ul class=\"ciudades\" id=\"Bogota\">
                <li>
                    <h2>Bogotá</h2>
                    <div>
                        <p><b>Motorysa</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Calle 80 costado sur Km 1, despues del puente de guadua</p>
                        <p><b>Tel:</b>(1) 7437242</p>
                    </div>
                    <div>
                        <p><b>Tramicón S.A.</b></p>
                        <p>Servicio posventa y distribuidor oficial de repuestos,</p>
                        <p>Calle 19 No. 68-75</p>
                        <p><b>Tel:</b>(1) 2609923</p>
                    </div>
                    <div>
                        <p><b>Impardiesel S.A</b></p>
                        <p>Distribuidor oficial de repuestos</p>
                        <p>Avenida Centenario No. 90-70</p>
                        <p><b>Tel:</b>(1) 4217963</p>
                    </div>
                </li>
            </ul>

            <ul class=\"ciudades\" id=\"Bucaramanga\">
                <li>
                    <h2>Bucaramanga</h2>
                    <div>
                        <p><b>Motoreste Motors S.A.</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Autopista Florida Blanca No. 91-55</p>
                        <p><b>Tel:</b>(7) 6360160</p>
                    </div>
                </li>
            </ul>

            <ul class=\"ciudades\" id=\"Cali\">
                <li>
                    <h2>Cali</h2>
                    <div>
                        <p><b>Andina Motors </b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Carrera 8 No. 33-16</p>
                        <p><b>Tel:</b>(2) 4852727</p>
                    </div>
                    <div>
                        <p><b>Andina Motors </b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Calle 15 Cra 34 Esquina, Autopista Cali-Yumbo</p>
                        <p><b>Tel:</b>(2) 4852727</p>
                    </div>
                </li>
            </ul>

            <ul class=\"ciudades\" id=\"Cucuta\">
                <li>
                    <h2>Cúcuta</h2>
                    <div>
                        <p><b>Motoreste Motors S.A.</b></p>
                        <p>Venta de vehículos</p>
                        <p>Avenida Libertadores 2 - 100</p>
                        <p><b>Tel:</b>3114626076</p>
                    </div>
                    <div>
                        <p><b>Orient Trucks</b></p>
                        <p>Distribuidor oficial de repuestos</p>
                        <p>Avenida 3 No. 8-19</p>
                        <p><b>Tel:</b>(7) 5831065</p>
                    </div>
                </li>
            </ul>
            <ul class=\"ciudades\" id=\"Duitama\">
                <li>
                    <h2>Duitama</h2>
                    <div>
                        <p><b>Motorysa</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Municipio de Duitama vereda San Lorenzo KM 4 Vía Paipa Duitama</p>
                        <p><b>Tel:</b>3223495959</p>
                    </div>
                    <div>
                        <p><b>Tractocamiones de las Americas</b></p>
                        <p>Distribuidor oficial de repuestos</p>
                        <p>Autopista Central de Norte 600 Mts. Vía Duitama-Paipa</p>
                        <p><b>Tel:</b>(8) 5831065</p>
                    </div>
                    <div>
                        <p><b>Luciano & Chaparro</b></p>
                        <p>Distribuidor oficial de repuestos</p>
                        <p>Carrera 18 No. 12-73</p>
                        <p><b>Tel:</b>(8) 7602715</p>
                    </div>
                </li>
            </ul>
            <ul class=\"ciudades\" id=\"Ibague\">
                <li>
                    <h2>Ibagué</h2>
                    <div>
                        <p><b>Motorysa</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Carrera 48 sur No. 83-15 Kilómetro 4 Vía Picaleña</p>
                        <p><b>Tel:</b>(8) 2771828</p>
                    </div>
                </li>
            </ul>
            <ul class=\"ciudades\" id=\"Medellin\">
                <li>
                    <h2>Medellín</h2>
                    <div>
                        <p><b>Alemautos</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Carrera 50 No. 79 Sur-30 (La Estrella)</p>
                        <p><b>Tel:</b>(4) 4442369</p>
                    </div>
                </li>
            </ul>
            <ul class=\"ciudades\" id=\"Pereira\">
                <li>
                    <h2>Pereira</h2>
                    <div>
                        <p><b>Andes Motors</b></p>
                        <p>Venta, servicio posventa y distribuidor oficial de repuestos</p>
                        <p>Av. 30 de Agosto No. 103-81</p>
                        <p><b>Tel:</b>(6) 3247575</p>
                    </div>
                </li>
            </ul>

            <ul class=\"ciudades\" id=\"Neiva\">
                <li>
                    <h2>Neiva</h2>
                    <div>
                        <p><b>Impardiesel S.A </b></p>
                        <p>Distribuidor autorizado de Repuestos</p>
                        <p>Calle 2da sur No. 7-30 </p>
                        <p><b>Tel:</b>(8) 8631602</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>

";
    }

    // line 188
    public function block_javascripts($context, array $blocks = array())
    {
        // line 189
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        var concesionarios = \$('#concesionarios');
        concesionarios.addClass('hidden');
        \$('#red').click(function () {
            concesionarios.removeClass('hidden');
        });
        var ciudades = {
            Barranquilla:\$('#Barranquilla'),
            Bogota:\$('#Bogota'),
            Bucaramanga:\$('#Bucaramanga'),
            Cali:\$('#Cali'),
            Cucuta:\$('#Cucuta'),
            Duitama:\$('#Duitama'),
            Ibague:\$('#Ibague'),
            Medellin:\$('#Medellin'),
            Pereira:\$('#Pereira'),
            Neiva:\$('#Neiva')
        };

        \$.each(ciudades, function (key, value) {
            value.addClass('hidden');
        });

        var ciudadAnt = null;

        function activarCiudad(nomCiudad) {
            ciudadAnt = ciudadAnt===null?nomCiudad:ciudadAnt;
            ciudades[ciudadAnt].addClass('hidden');
            ciudades[nomCiudad].removeClass('hidden');
            ciudadAnt = nomCiudad;
        }

        var selCiudad = \$('select#selCiudad');

        activarCiudad(selCiudad.val());

        selCiudad.change(function () {
            activarCiudad(selCiudad.val());
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:datosEnviados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 189,  222 => 188,  41 => 11,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:datosEnviados.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/datosEnviados.html.twig");
    }
}
