<?php

/* :recalls:new.html.twig */
class __TwigTemplate_c384750ff3795d0ea01350c89380f8005b8171621cea1a3edeb4789de570951b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":recalls:new.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = "Recall Nuevo";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Recalls</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">


                    ";
        // line 18
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_start', array("attr" => array("class" => "form")));
        echo "

                    ";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "imagen", array()), 'row');
        echo "

                    ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["form"] ?? null), 'widget', array("attr" => array("class" => "form-group")));
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Crear</button>
                        </div>
                    </div>
                    ";
        // line 28
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["form"] ?? null), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return ":recalls:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 28,  75 => 22,  70 => 20,  65 => 18,  56 => 11,  53 => 10,  47 => 7,  42 => 6,  39 => 5,  33 => 4,  29 => 1,  27 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":recalls:new.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/recalls/new.html.twig");
    }
}
