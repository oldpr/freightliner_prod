<?php

/* AppBundle:Default:buscarCodigo.html.twig */
class __TwigTemplate_1b0e0bd4fa33df2ddd2ab087a3bf4901e6b596f8ce50551a7c3ea0c96939b3f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:buscarCodigo.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>
    ";
        // line 7
        if (array_key_exists("codigos", $context)) {
            // line 8
            echo "
        <div class=\"content\">
            <div class=\"form-contact\">
                <!--<h2>EL CÓDIGO APLICA</h2>-->
                <h2>LLAMADO A SEGURIDAD FREIGHTLINER CASCADIA PRODUCIDOS ENTRE 2007 Y 2018</h2>
                <!--<p>El código <span>-->
                <p>Daimler Colombia  S. A informa  que, para su vehículo FREIGHTLINER CASCADIA, la fábrica ha lanzado una campaña de seguridad. Esta refiere a que, después de varias aplicaciones de freno duro, el interruptor de presión de la luz de freno puede no activarse con una aplicación ligera del pedal de freno. Tales luces no se iluminan cuando los frenos de servicio se están aplicando, por lo cual no indicaría a otros automovilistas la intención del conductor de reducir la velocidad.</p>";
            // line 12
            //echo twig_escape_filter($this->env, ($context["codigo"] ?? null), "html", null, true);
            //echo "</span>, aplica para la campaña de seguridad <span>";
            //echo twig_replace_filter($this->getAttribute($this->getAttribute($this->getAttribute(($context["codigos"] ?? null), 0, array(), "array"), "recall", array()), "descripcion", array()), array("<p>" => "", "</p>" => ""));
            //echo "</span>, ingrese sus datos de contacto para obtener más información.</p>
            //    <form action=\"";
            // line 13
            //echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("guardarRegistro");
            //echo "\" method=\"post\">
            //        <input type=\"hidden\" name=\"codigo\" value=\"";
            // line 14
            //echo twig_escape_filter($this->env, ($context["codigo"] ?? null), "html", null, true);
            //echo "\">
            //        <input type=\"text\" name=\"nombres\" placeholder=\"nombres\" required>
            //        <input type=\"text\" name=\"apellidos\" placeholder=\"apellidos\" required>
            //        <input type=\"text\" name=\"correo\" placeholder=\"correo electrónico\" required>
            //        <input type=\"text\" name=\"telefono\" placeholder=\"teléfono\" required>
            //        <input type=\"hidden\" name=\"misRegistros\" value=\"";
            // line 19
            //echo twig_escape_filter($this->env, twig_jsonencode_filter(($context["misRegistros"] ?? null)), "html", null, true);
            //echo "\" >
            //        <input type=\"submit\" value=\"Ver información\" class=\"boton\">
            //    </form>
            echo "
                <p>Encuentre <a href='https://www.freightliner.com.co/concesionarios.php' >aquí</a> nuestra red de concesionarios autorizados para comunicarse con el más cercano a su ubicación y programar la cita.</p>
            </div>

        </div>

    ";
        } else {
            // line 27
            echo "        <div class=\"content\">
            <div class=\"form-contact\">
                <h2>LLAMADOS DE SEGURIDAD</h2>
                <p>El código <span>";
            // line 30
            echo twig_escape_filter($this->env, ($context["codigo"] ?? null), "html", null, true);
            echo "</span>, no aplica para alguna de las campañas de seguridad vigentes de Freightliner Colombia Rectifique el código o intente nuevamente.</p>
                <form action=\"";
            // line 31
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("buscarCodigo");
            echo "\" method=\"post\" class=\"two-buttons\">
                    <input class=\"red\" type=\"text\" name=\"codigo\" placeholder=\"Ingrese su número VIN o FIN\">
                    <input type=\"submit\" value=\"volver a buscar\" class=\"boton\">
                    <a href=\"http://freightliner.com.co\" class=\"boton boton-medio boton-negro\">IR AL SITIO WEB</a>
                    <a class=\"que-es\" role=\"button\" data-toggle=\"popover\" data-trigger=\"focus\" title=\"Dismissible popover\" data-content=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"><b>¿Qué es el código VIN o FIN?</b></a>
                </form>
            </div>
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:buscarCodigo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 31,  78 => 30,  73 => 27,  62 => 19,  54 => 14,  50 => 13,  44 => 12,  38 => 8,  36 => 7,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:buscarCodigo.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/buscarCodigo.html.twig");
    }
}
