<?php

/* :ciudades:index.html.twig */
class __TwigTemplate_6a4f4ecacf6efd42aca4fbd5853999a2757026f4b1a5b2cb92c439f47d6f69b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":ciudades:index.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = "Ciudades";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li class=\"active\">";
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
    <!-- container -->
    ";
        // line 12
        if ((array_key_exists("error", $context) && (($context["error"] ?? null) != null))) {
            // line 13
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            ERROR: ";
            // line 15
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 18
        echo "
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Lista <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_new");
        echo "\" class=\"btn btn-success pull-right\">Nuevo <span class=\"md  md-add\"></span></a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ciudades"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["entidad"]) {
            // line 40
            echo "                                    <tr>
                                        <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "id", array()), "html", null, true);
            echo "</td>
                                        <td><a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_edit", array("id" => $this->getAttribute($context["entidad"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "nombre", array()), "html", null, true);
            echo "</a></td>
                                    </tr>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return ":ciudades:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 45,  105 => 42,  101 => 41,  98 => 40,  94 => 39,  75 => 23,  68 => 18,  62 => 15,  58 => 13,  56 => 12,  52 => 10,  49 => 9,  42 => 6,  39 => 5,  33 => 4,  29 => 1,  27 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":ciudades:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/ciudades/index.html.twig");
    }
}
