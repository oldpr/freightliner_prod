<?php

/* :recalls:edit.html.twig */
class __TwigTemplate_5a36f419e0c923b2b2e99609629e9d72f9c2c295c42f59b122d362f9a59c7fb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":recalls:edit.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = $this->getAttribute(($context["recall"] ?? null), "nombre", array());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_recalls_index");
        echo "\">Recalls</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "
    ";
        // line 12
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array(), "any", false, true), "get", array(0 => "error"), "method", true, true)) {
            // line 13
            echo "        ";
            $context["error"] = $this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array()), "get", array(0 => "error"), "method");
            // line 14
            echo "    ";
        }
        // line 15
        echo "    ";
        if ((array_key_exists("error", $context) && (($context["error"] ?? null) != null))) {
            // line 16
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 18
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 21
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    ";
        // line 25
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? null), 'form_start', array("attr" => array("class" => "", "id" => "form_delete")));
        echo "
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"button\" class=\"btn btn-danger waves-effect waves-light btn-sm pull-right\" id=\"\" onclick=\"eliminar()\">Eliminar</button>
                    </h3>
                    ";
        // line 29
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? null), 'form_end');
        echo "
                </div>
                <div class=\"panel-body\">


                    ";
        // line 34
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? null), 'form_start', array("attr" => array("class" => "form")));
        echo "

                    <label class=\"control-label\">Imagen</label>
                    <img src=\"";
        // line 37
        echo twig_escape_filter($this->env, ($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/images/") . $this->getAttribute(($context["recall"] ?? null), "imagen", array())), "html", null, true);
        echo "\" class=\"responsive-img\" alt=\"Imagen\" style=\"max-width: 100%; max-height: 250px\">
                    ";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_form"] ?? null), "imagen", array()), 'row', array("label" => "Cambiar Imagen"));
        echo "

                    ";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute(($context["edit_form"] ?? null), "linkPdf", array()), 'row', array("label" => "Cambiar Archivo"));
        echo "
                    <div class=\"col-md-12 control-label panel panel-default\">
                        <div class=\"panel-heading\">
                            <a href=\"";
        // line 43
        echo twig_escape_filter($this->env, ($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("uploads/docs/") . $this->getAttribute(($context["recall"] ?? null), "linkPdf", array())), "html", null, true);
        echo "\" class=\"panel-title text-info\" target=\"_blank\">Ver Archivo Actual</a>
                        </div>
                    </div>

                    ";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["edit_form"] ?? null), 'widget', array("attr" => array("class" => "form-group")));
        echo "

                    <input type=\"hidden\" name=\"img\" value=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? null), "imagen", array()), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"pdf\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute(($context["recall"] ?? null), "linkPdf", array()), "html", null, true);
        echo "\">
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    ";
        // line 56
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? null), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Códigos VIN <a href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("subirCodigosRecall", array("idRecall" => $this->getAttribute(($context["recall"] ?? null), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-success pull-right\"><span class=\"md  md-file-upload\"></span> Subir Códigos VIN</a></h3>

                </div>
                <div class=\"panel-body\">
                    <div class=\"row\">
                        <div class=\"col-md-12 col-sm-12 col-xs-12\">
                            <table id=\"datatable\" class=\"table table-striped table-bordered\">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>


                                <tbody>
                                ";
        // line 84
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["recall"] ?? null), "codigos", array()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["entidad"]) {
            // line 85
            echo "                                    <tr>
                                        <td>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "id", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["entidad"], "codigo", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 88
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entidad"], "fecha", array()), "d/m/Y"), "html", null, true);
            echo "</td>
                                    </tr>
                                ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 91
            echo "                                    <div class=\"alert alert-info\">
                                        No se han subido los VIN de este Recall.
                                    </div>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entidad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 105
    public function block_js($context, array $blocks = array())
    {
        // line 106
        echo "    <script type=\"text/javascript\">
        function eliminar() {
            swal({
                    title: \"Desea eliminar este elemento?\",
                    text: \"Si se elimina, no se podrá recuperar!\",
                    type: \"warning\",
                    showCancelButton: true,
                    confirmButtonColor: \"#DD6B55\",
                    confirmButtonText: \"Sí, Eliminar!\",
                    closeOnConfirm: false
                },
                function () {
                    document.getElementById(\"form_delete\").submit();
                    swal(\"Eliminado!\", \"Este elemento se eliminó.\", \"success\");
                });
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return ":recalls:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 106,  228 => 105,  215 => 95,  206 => 91,  198 => 88,  194 => 87,  190 => 86,  187 => 85,  182 => 84,  162 => 67,  148 => 56,  139 => 50,  135 => 49,  130 => 47,  123 => 43,  117 => 40,  112 => 38,  108 => 37,  102 => 34,  94 => 29,  87 => 25,  81 => 21,  75 => 18,  71 => 16,  68 => 15,  65 => 14,  62 => 13,  60 => 12,  57 => 11,  54 => 10,  48 => 7,  43 => 6,  40 => 5,  34 => 4,  30 => 1,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":recalls:edit.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/recalls/edit.html.twig");
    }
}
