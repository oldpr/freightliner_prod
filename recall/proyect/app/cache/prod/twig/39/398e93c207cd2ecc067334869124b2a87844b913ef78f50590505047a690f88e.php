<?php

/* AppBundle:Default:concesionarios.html.twig */
class __TwigTemplate_9d83d9efe72684b6d2a22693283c40c48ff0891b43db52f5a7dc7266ea1e69a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:concesionarios.html.twig", 1);
        $this->blocks = array(
            'banerInicial' => array($this, 'block_banerInicial'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_banerInicial($context, array $blocks = array())
    {
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "

    <div class=\"container\">
        <div class=\"inner clientes\" style=\"margin-top:150px\" id=\"\">
            <hr>
            <!--CONTENIDOS-->
            <div class=\"container content-interna\" style=\"margin-top:60px;\">
                <div class=\"row\">
                    <!--MENU LATERAL -->
                    <aside class=\"col-lg-3 col-md-3 col-sm-4 \">
                        <nav class=\"menuLateral\">
                            <ul>
                                ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ciudades"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["ciudad"]) {
            // line 17
            echo "                                    <li><a data-id=\"#";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "nombre", array()), "html", null, true);
            echo "</a></li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ciudad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                            </ul>
                        </nav>

                    </aside>
                    <div class=\"col-lg-9 col-md-9 col-sm-8 tabinner concesionarios\">
                        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ciudades"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["ciudad"]) {
            // line 25
            echo "                        <div id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "id", array()), "html", null, true);
            echo "\" class=\"contCon\">
                            <div class=\"col-md-12\">
                                <h3>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["ciudad"], "nombre", array()), "html", null, true);
            echo "</h3>
                            </div>

                            ";
            // line 30
            $context["cont"] = 0;
            // line 31
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["ciudad"], "concesionarios", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["concesionario"]) {
                // line 32
                echo "                                ";
                $context["cont"] = (($context["cont"] ?? null) + 1);
                // line 33
                echo "                                ";
                if (((($context["cont"] ?? null) % 2) == 1)) {
                    // line 34
                    echo "                                    <div class=\"row capConce\">
                                    <div class=\"col-md-6\">
                                        <b>";
                    // line 36
                    echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "nombre", array()), "html", null, true);
                    echo "</b><br>
                                        ";
                    // line 37
                    echo $this->getAttribute($context["concesionario"], "descripcion", array());
                    echo "
                                    </div>
                                ";
                } else {
                    // line 40
                    echo "                                    <div class=\"col-md-6\">
                                        <strong>";
                    // line 41
                    echo twig_escape_filter($this->env, $this->getAttribute($context["concesionario"], "nombre", array()), "html", null, true);
                    echo "</strong><br>
                                        ";
                    // line 42
                    echo $this->getAttribute($context["concesionario"], "descripcion", array());
                    echo "
                                    </div>
                                    </div>
                                ";
                }
                // line 46
                echo "
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['concesionario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "
                            ";
            // line 49
            if (((($context["cont"] ?? null) % 2) == 1)) {
                // line 50
                echo "                        </div>
                        ";
            }
            // line 52
            echo "                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ciudad'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:concesionarios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 54,  146 => 52,  142 => 50,  140 => 49,  137 => 48,  130 => 46,  123 => 42,  119 => 41,  116 => 40,  110 => 37,  106 => 36,  102 => 34,  99 => 33,  96 => 32,  91 => 31,  89 => 30,  83 => 27,  77 => 25,  73 => 24,  66 => 19,  55 => 17,  51 => 16,  37 => 4,  34 => 3,  29 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:concesionarios.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/concesionarios.html.twig");
    }
}
