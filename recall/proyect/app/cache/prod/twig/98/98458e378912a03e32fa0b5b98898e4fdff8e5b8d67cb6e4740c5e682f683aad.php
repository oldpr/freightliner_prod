<?php

/* AdminBundle:Default:registro.html.twig */
class __TwigTemplate_9615a95cebc571d39e2fce5f0d7217d895a4735ff530bff20b31d0ff2aff348a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", "AdminBundle:Default:registro.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = ((("VIN: " . $this->getAttribute($this->getAttribute(($context["registro"] ?? null), "codigo", array()), "codigo", array())) . " - fecha: ") . twig_date_format_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "fecha", array())));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_registros");
        echo "\">Registros</a></li>
    <li class=\"active\">";
        // line 7
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "
    ";
        // line 12
        if ((array_key_exists("error", $context) && (($context["error"] ?? null) != null))) {
            // line 13
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 15
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 18
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-body\">

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Id</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "id", array()), "html", null, true);
        echo "</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Fecha</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 30
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "fecha", array())), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Nombres</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "nombres", array()), "html", null, true);
        echo "</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Apellidos</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "apellidos", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">Correo</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "correo", array()), "html", null, true);
        echo "</p>
                        </div>
                        <label class=\"col-sm-2 control-label\">Telefono</label>
                        <div class=\"col-md-4\">
                            <p class=\"form-control-static\">";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute(($context["registro"] ?? null), "telefono", array()), "html", null, true);
        echo "</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Default:registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 52,  115 => 48,  105 => 41,  98 => 37,  88 => 30,  81 => 26,  71 => 18,  65 => 15,  61 => 13,  59 => 12,  56 => 11,  53 => 10,  47 => 7,  42 => 6,  39 => 5,  33 => 4,  29 => 1,  27 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AdminBundle:Default:registro.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AdminBundle/Resources/views/Default/registro.html.twig");
    }
}
