<?php

/* AppBundle:Default:index.html.twig */
class __TwigTemplate_f5c38053ec8d4950972017762bded314a0494ef5cb9ada39a2d18fc98f1c9b58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"slider-info\">
        <h1>Campaña de seguridad</h1>
    </div>

    <div class=\"content\">
        <div class=\"form-contact\">
            <h2>LLAMADOS DE SEGURIDAD</h2>
            <p>Bienvenido a la sección especializada de Freightliner Colombia donde se le informará si su vehículo aplica para alguna de las campañas de seguridad vigentes.</p>
            <form action=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("buscarCodigo");
        echo "\" method=\"post\">
                <input type=\"text\" name=\"codigo\" placeholder=\"Ingrese su número VIN o FIN\">
                <input type=\"submit\" value=\"Buscar\" class=\"boton\">
                <a ";
        // line 15
        echo " class=\"que-es\" role=\"button\" data-toggle=\"popover\" data-trigger=\"focus\" title=\"Dismissible popover\" data-content=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\"><b>¿Qué es el código VIN o FIN?</b></a>
            </form>
        </div>

    </div>
";
    }

    public function getTemplateName()
    {
        return "AppBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 15,  41 => 12,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Default:index.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/src/AppBundle/Resources/views/Default/index.html.twig");
    }
}
