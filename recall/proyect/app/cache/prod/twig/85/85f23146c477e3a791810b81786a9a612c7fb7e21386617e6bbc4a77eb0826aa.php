<?php

/* base.html.twig */
class __TwigTemplate_d65520687ac67fb6f7f9438f525bac341dff5afcfcac69787ee7d1a899e0d35c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'metas' => array($this, 'block_metas'),
            'links' => array($this, 'block_links'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'modals' => array($this, 'block_modals'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"es\">
<head>
    ";
        // line 4
        $this->displayBlock('metas', $context, $blocks);
        // line 10
        echo "    ";
        $this->displayBlock('links', $context, $blocks);
        // line 16
        echo "    <link href=\"https://fonts.googleapis.com/css?family=Rajdhani:500,600,700\" rel=\"stylesheet\">
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"), "html", null, true);
        echo "\"></script>

    <title>Freightliner Colombia - Bienvenido al Sitio oficial</title>
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../img/favicon/favicon-16x16.png"), "html", null, true);
        echo "\">

</head>
";
        // line 23
        if (array_key_exists("classBody", $context)) {
            // line 24
            echo "<body class=\"";
            echo twig_escape_filter($this->env, ($context["classBody"] ?? null), "html", null, true);
            echo "\">
";
        } else {
            // line 26
            echo "<body>
";
        }
        // line 28
        echo "<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-47958253-1\"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-47958253-1');
</script>
<!--[if lt IE 8]>
<p class=\"browserupgrade\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
";
        // line 40
        $this->displayBlock('header', $context, $blocks);
        // line 183
        $this->displayBlock('content', $context, $blocks);
        // line 184
        $this->displayBlock('footer', $context, $blocks);
        // line 231
        $this->displayBlock('modals', $context, $blocks);
        // line 232
        $this->displayBlock('javascripts', $context, $blocks);
        // line 241
        echo "<script type=\"text/javascript\">
    var correoNewsletter = \$('#correoNewsletter');
    var submitNewsletter = \$('#submitNewsletter');
    var error = \"Ha habido un error al procesar la solicitud<br>por favor vuelva a intentarlo\";
    \$('form#newsletter').submit(function(event) {
        var correoN = correoNewsletter.val();
        var labelMsjNewsletter = \$('#labelMsjNewsletter');
        submitNewsletter.val('ENVIANDO...');
        \$.ajax({
            type        : 'POST',
            url         : './libs/addNewsletter.php',
            data        : {correo:correoN},
            success: function(respose){
                console.log(respose);
                respose = JSON.parse(respose);
                if (respose.code==200) {
                    correoNewsletter.val(\"\");
                    submitNewsletter.val('SUSCRITO');
                    labelMsjNewsletter.html('');
                }else{
                    console.log(respose.error);
                    labelMsjNewsletter.html(respose.msj);
                    submitNewsletter.val('SUSCRIBIRME');
                }
                labelMsjNewsletter.show(250);
            },
            error: function (response) {
                console.log(response);
                labelMsjNewsletter.html(error);
                labelMsjNewsletter.show(250);
                submitNewsletter.val('SUSCRIBIRME');
            }

        });
        event.preventDefault();
    });
</script>
</body>
</html>";
    }

    // line 4
    public function block_metas($context, array $blocks = array())
    {
        // line 5
        echo "        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <meta name=\"description\" content=\"\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    ";
    }

    // line 10
    public function block_links($context, array $blocks = array())
    {
        // line 11
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../css/bootstrap-theme.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../css/main.css"), "html", null, true);
        echo "\">

    ";
    }

    // line 40
    public function block_header($context, array $blocks = array())
    {
        // line 41
        echo "    <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
        <div class=\"contenedor\">
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"freightliner\" href=\"http://freightliner.com.co/index.php\"></a>
                <div class=\"info-header hidden-xs\">
                    <p>Servicio al cliente: <b>+57 1 742 6850</b></p>
                    <!-- <a href=\"mailto:info@freightliner.com.co\">info@freightliner.com.co</a> -->
                </div>
                <a class=\"visible-xs btn-productos\">Productos</a>
            </div>
            <div id=\"navbar\" class=\"navbar-collapse collapse\">
                <ul>
                    ";
        // line 59
        if ( !array_key_exists("active", $context)) {
            // line 60
            echo "                        ";
            $context["active"] = 0;
            // line 61
            echo "                    ";
        }
        // line 62
        echo "                    <li><a href=\"http://freightliner.com.co/index.php\" class=\"";
        if ((($context["active"] ?? null) == 1)) {
            echo "active";
        }
        echo "\">Inicio</a></li>
                    <li class=\"expandible\"><a class=\"";
        // line 63
        if ((($context["active"] ?? null) == 2)) {
            echo "active";
        }
        echo "\" id=\"productos\">Productos</a></li>
                    <li><a href=\"http://freightliner.com.co/posventa.php\" class=\"";
        // line 64
        if ((($context["active"] ?? null) == 3)) {
            echo "active";
        }
        echo "\">Posventa</a></li>
                    <li><a href=\"http://freightliner.com.co/organizacion.php\" class=\"";
        // line 65
        if ((($context["active"] ?? null) == 4)) {
            echo "active";
        }
        echo "\">¿por qué freightliner?</a></li>
                    <li><a href=\"http://freightliner.com.co/timeline.php\" class=\"";
        // line 66
        if ((($context["active"] ?? null) == 5)) {
            echo "active";
        }
        echo "\">75 años de innovación</a></li>

                    <li><a href=\"http://freightliner.com.co/concesionarios.php\" class=\"";
        // line 68
        if ((($context["active"] ?? null) == 8)) {
            echo "active";
        }
        echo "\">Concesionarios</a></li>
                    <li><a href=\"http://freightliner.com.co/noticias.php\" class=\"";
        // line 69
        if ((($context["active"] ?? null) == 6)) {
            echo "active";
        }
        echo "\">Noticias</a></li>
                    <!-- <li><a href=\"http://freightliner.com.co/\" class=\"";
        // line 70
        if ((($context["active"] ?? null) == 7)) {
            echo "active";
        }
        echo "\">Recall</a></li> -->
                    <li><a href=\"http://freightliner.com.co/contacto.php\" class=\"";
        // line 71
        if ((($context["active"] ?? null) == 9)) {
            echo "active";
        }
        echo "\">Contacto</a></li>
                </ul>
                <div class=\"redes\">
                    <a class=\"facebook\" target=\"_blank\" href=\"https://www.facebook.com/FreightlinerCol/\"></a>
                    <a class=\"instagram\" target=\"_blank\" href=\"https://www.instagram.com/freightlinercol/\"></a>
                    <!-- <a class=\"youtube\" href=\"http://freightliner.com.co/\"></a> -->
                </div>
            </div><!--/.navbar-collapse -->
        </div>
        <div class=\"menu-expandido\">
            <ul class=\"tipo\">

                <li>
                    <a style=\"background-image: url(../img/opc-carga.jpg);\"><p>Movimiento en carretera</p></a>
                    <ul class=\"opciones\">
                        <li>
                            <a>CASCADIA</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-Cascadia.jpg);\"></div>
                                <p class=\"det-nombre\">CASCADIA</p>
                                <p class=\"det-motor\">Detroit DD15</span></p>
                                <p class=\"det-potencia\">475HP</span></p>
                                <p class=\"det-precio\"><span>\$</span>323.200.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/cascadia-dd15.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/cascadia.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 MINIMULA</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-Minimula.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 MINIMULA</p>
                                <p class=\"det-pbv\">5.321<span>kg</span></p>
                                <p class=\"det-combinado\">40.500kg</p>
                                <p class=\"det-precio\"><span>\$</span>297.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-minimula.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m2106-minimula.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 4x2 LARGO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Camion-4X2.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 4x2 LARGO</p>
                                <p class=\"det-pbv\">5.620<span>kg</span></p>
                                <p class=\"det-distancia\">6.121mm</p>
                                <p class=\"det-precio\"><span>\$</span>283.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-4x2-largo.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m21064x2-camion-largo.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 106 6x4 LARGO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Camion-6X4.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 6x4 LARGO</p>
                                <p class=\"det-pbv\">7.661<span>kg</span></p>
                                <p class=\"det-distancia\">6.400mm</p>
                                <p class=\"det-precio\"><span>\$</span>330.000.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2106-6x4-largo.php\">Conozca más</a>
                                    <a target=\"_blank\"  href=\"http://freightliner.com.co/pdf/m2106-6x4-camion-largo.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>

                <li>
                    <a style=\"background-image: url(../img/opc-construccion.jpg);\"><p>Movimiento en todo terreno</p></a>
                    <ul class=\"opciones\">
                        <li>
                            <a>M2 106 VOLCO 6x4</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-106-Volco.jpg);\"></div>
                                <p class=\"det-nombre\">M2 106 VOLCO 6x4</p>
                                <p class=\"det-vol-vac\">9.504<span>kg</span></p>
                                <p class=\"det-capacidad\">14mts<span>3</span></p>
                                <p class=\"det-precio\"><span>\$</span>266.800.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2-volco.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/m2106-6X4-volco.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a>M2 112 VOLCO</a>
                            <div class=\"detalles\">
                                <div class=\"det-image\" style=\"background-image: url(../img/assets/megamenu/Foto-megamenu-M2-112-Volco.jpg);\"></div>
                                <p class=\"det-nombre\">M2 112 VOLCO</p>
                                <p class=\"det-motor\">MBE 4000</span></p>
                                <p class=\"det-potencia\">350HP</span></p>
                                <p class=\"det-precio-volco-hoper\"><span>\$</span>296.500.000</p>
                                <div class=\"det-btn\">
                                    <a href=\"http://freightliner.com.co/m2-112-volco.php\">Conozca más</a>
                                    <a target=\"_blank\" href=\"http://freightliner.com.co/pdf/M2112-volco.pdf\">Ficha técnica</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <div class=\"overlay\"></div>
";
    }

    // line 183
    public function block_content($context, array $blocks = array())
    {
    }

    // line 184
    public function block_footer($context, array $blocks = array())
    {
        // line 185
        echo "    <footer>
        <div class=\"content\">
            <div class=\"col-md-8\">
                <a class=\"freightliner\" href=\"http://freightliner.com.co/index.php\"></a>
                <div class=\"boletin\">
                    <h3>Boletín virtual</h3>
                    <p>Suscríbase y entérese de las últimas noticias, novedades y ofertas de la marca.</p>
                    <form id=\"newsletter\">
                        <input type=\"email\" name=\"mail-boletin\" id=\"correoNewsletter\" placeholder=\"Escriba su correo electrónico\" required>
                        <input class=\"boton\" id=\"submitNewsletter\" type=\"submit\" value=\"suscribirme\">
                        <p id=\"labelMsjNewsletter\" class=\"msj-form\"></p>
                    </form>
                </div>
            </div>
            <div class=\"col-md-4\">
                <ul>
                    <li><a href=\"http://freightliner.com.co/index.php\">Inicio</a></li>
                    <li><a href=\"http://freightliner.com.co/organizacion.php\">Por qué Freightliner</a></li>
                    <li><a href=\"http://freightliner.com.co/posventa.php\">Posventa</a></li>
                    <li><a href=\"http://freightliner.com.co/noticias.php\">Noticias</a></li>
                    <li><a target=\"_blank\" href=\"http://daimler.com.co/freightliner.php\">Garantía</a></li>

                </ul>
                <ul>
                    <!-- <li><a href=\"http://freightliner.com.co/\">Sala de prensa</a></li> -->
                    <li><a href=\"http://freightliner.com.co/recall.php\">Campaña de seguridad</a></li>
                    <li><a href=\"http://freightliner.com.co/concesionarios.php\">Concesionarios</a></li>
                    <li><a href=\"http://freightliner.com.co/contacto.php\">Contacto</a></li>
                    <li><a href=\"http://freightliner.com.co/terminos-condiciones.php\">Términos y condiciones</a></li>
                </ul>
                <div class=\"redes\">
                    <a class=\"facebook\" target=\"_blank\" href=\"https://www.facebook.com/FreightlinerCol/\"></a>
                    <a class=\"instagram\" target=\"_blank\" href=\"https://www.instagram.com/freightlinercol/\"></a>
                    <!-- <a class=\"youtube\" href=\"http://freightliner.com.co/\"></a> -->
                </div>
            </div>
        </div>

        <hr>

        <div class=\"content legales\">
            <a class=\"daimler\" target=\"_blank\" href=\"http://daimler.com.co/\"></a>
            <p>Todos los derechos reservados - Daimler Colombia S.A. 2017 - <a href=\"http://freightliner.com.co/pdf/terminos-y-condiciones.pdf\" target=\"_blank\">Términos y condiciones</a></p>
        </div>
    </footer>
";
    }

    // line 231
    public function block_modals($context, array $blocks = array())
    {
    }

    // line 232
    public function block_javascripts($context, array $blocks = array())
    {
        // line 233
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
    <script>window.jQuery || document.write('<script src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/vendor/jquery-1.11.2.min.js"), "html", null, true);
        echo "\"><\\/script>')</script>

    <script src=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/vendor/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
        // line 238
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("../js/main.js"), "html", null, true);
        echo "\"></script>

";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 238,  431 => 236,  426 => 234,  423 => 233,  420 => 232,  415 => 231,  366 => 185,  363 => 184,  358 => 183,  240 => 71,  234 => 70,  228 => 69,  222 => 68,  215 => 66,  209 => 65,  203 => 64,  197 => 63,  190 => 62,  187 => 61,  184 => 60,  182 => 59,  162 => 41,  159 => 40,  152 => 13,  148 => 12,  143 => 11,  140 => 10,  132 => 5,  129 => 4,  87 => 241,  85 => 232,  83 => 231,  81 => 184,  79 => 183,  77 => 40,  63 => 28,  59 => 26,  53 => 24,  51 => 23,  45 => 20,  39 => 17,  36 => 16,  33 => 10,  31 => 4,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/base.html.twig");
    }
}
