<?php

/* :concesionarios:edit.html.twig */
class __TwigTemplate_b1a4d2844fbbd997e06b5ed85ffc907cb87f2a0744c458dd0808ea0fb4412a0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base_admin.html.twig", ":concesionarios:edit.html.twig", 1);
        $this->blocks = array(
            'pagina' => array($this, 'block_pagina'),
            'ubicacion' => array($this, 'block_ubicacion'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["nomPagina"] = $this->getAttribute(($context["concesionario"] ?? null), "nombre", array());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_pagina($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
    }

    // line 5
    public function block_ubicacion($context, array $blocks = array())
    {
        // line 6
        echo "    <li><a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_index");
        echo "\">Ciudades</a></li>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_ciudades_edit", array("id" => $this->getAttribute($this->getAttribute(($context["concesionario"] ?? null), "ciudad", array()), "id", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["concesionario"] ?? null), "ciudad", array()), "html", null, true);
        echo "</a></li>
    <li class=\"active\">";
        // line 8
        echo twig_escape_filter($this->env, ($context["nomPagina"] ?? null), "html", null, true);
        echo "</li>
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "
    ";
        // line 13
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array(), "any", false, true), "get", array(0 => "error"), "method", true, true)) {
            // line 14
            echo "        ";
            $context["error"] = $this->getAttribute($this->getAttribute(($context["app"] ?? null), "request", array()), "get", array(0 => "error"), "method");
            // line 15
            echo "    ";
        }
        // line 16
        echo "    ";
        if ((array_key_exists("error", $context) && (($context["error"] ?? null) != null))) {
            // line 17
            echo "        <div class=\"alert alert-danger alert-dismissible fade in\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>
            <b>ERROR:</b> ";
            // line 19
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo ".
        </div>
    ";
        }
        // line 22
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    ";
        // line 26
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? null), 'form_start', array("attr" => array("class" => "")));
        echo "
                    <h3 class=\"panel-title\">Elementos Editables
                        <button type=\"submit\" class=\"btn btn-danger waves-effect waves-light pull-right\">Eliminar</button>
                    </h3>
                    ";
        // line 30
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["delete_form"] ?? null), 'form_end');
        echo "
                </div>
                <div class=\"panel-body\">

                    ";
        // line 34
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? null), 'form_start');
        echo "
                    ";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock(($context["edit_form"] ?? null), 'widget');
        echo "
                    <div class=\"form-group m-b-0\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-info waves-effect waves-light\">Editar</button>
                        </div>
                    </div>
                    ";
        // line 41
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock(($context["edit_form"] ?? null), 'form_end');
        echo "

                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return ":concesionarios:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 41,  110 => 35,  106 => 34,  99 => 30,  92 => 26,  86 => 22,  80 => 19,  76 => 17,  73 => 16,  70 => 15,  67 => 14,  65 => 13,  62 => 12,  59 => 11,  53 => 8,  47 => 7,  42 => 6,  39 => 5,  33 => 4,  29 => 1,  27 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":concesionarios:edit.html.twig", "/home/120818.cloudwaysapps.com/mxymcvtjvk/public_html/recall/proyect/app/Resources/views/concesionarios/edit.html.twig");
    }
}
