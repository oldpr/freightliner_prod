<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Freightliner en la Feria Expoconstrucción | Freightliner Colombia';
$metaDescripcion = 'Freightliner hizo presencia en la décimo cuarta edición de la Feria de Expoconstrucción y Expodiseño 2017';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/Freightliner-en-la-XIV-Feria-Internacional-Expoconstruccion-y-Expodiseno-2017.php';
$ogTitle = 'Freightliner en la Feria Expoconstrucción | Freightliner Colombia';
$ogDescription = 'Freightliner hizo presencia en la décimo cuarta edición de la Feria de Expoconstrucción y Expodiseño 2017';
$ogImage = 'http://freightliner.com.co/tags/expoconstruccion.jpg';
$twitterTitle = 'Freightliner en la Feria Expoconstrucción | Freightliner Colombia';
$twitterDescription = 'Freightliner hizo presencia en la décimo cuarta edición de la Feria de Expoconstrucción y Expodiseño 2017';
$twitterImage = 'http://freightliner.com.co/tags/expoconstruccion.jpg';


/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '03.11. 2017';
$imagen = 'img/assets/detalle-noticias/4Noticia-photo.jpg';

$titulo = 'Freightliner en la XIV  Feria Internacional  Expoconstrucción y Expodiseño  2017';

// --Contenido dividido por parrafos
$contenido[0] = 'El sector de la construcción es una de las industrias que más actividades integra en diferentes esferas económicas y sociales del país. Por ello, la importancia de generar espacios idóneos donde se promueva el fortalecimiento del sector de la construcción e infraestructura. Debido a lo anterior, el Centro Internacional de Negocios y Exposiciones de Bogotá, Corferias junto con la Cámara Colombiana de la Construcción (Camacol) abrieron sus puertas del 16 al 21 de mayo para dar inicio a la décimo cuarta edición de Expoconstrucción y Expodiseño 2017, una feria especializada de carácter internacional que versión tras versión se consolida como el escenario que promueve el desarrollo y crecimiento de los sectores de la construcción, arquitectura, infraestructura y diseño en la región, el evento cuenta con un aproximado de 600 expositores presentes en los 26.000 m2 de exhibición.
';
$contenido[1] = 'La marca de Camiones Freightliner le apuesta a este tipo de encuentros y en esta versión participó con el portafolio de volquetas Freightliner. 
';
$contenido[2] = '“Esta es la segunda vez que participamos en esta feria. Además de ser un escenario óptimo para una vitrina comercial exitosa, lo que buscamos es fortalecer la presencia de nuestras marcas. Con Freightliner, llegamos con la Volqueta M2 112 Volco Hardox, que cuenta con un acero de mayor resistencia para brindar a los constructores más horas efectivas de disponibilidad del vehículo y reducir los costos de mantenimiento del mismo”. Así lo afirmó Alexander Peña, Director de Camiones Daimler. 
';
$contenido[3] = 'Durante 6 días, Corferias se convirtió en el punto de encuentro del 50% de los sectores de la economía, lo que se traduce en 174 líneas productivas, desde donde la construcción demanda productos y servicios. 
';
$contenido[4] = '<strong>Freightliner M2 112 Volco Hardox</strong>
';
$contenido[5] = 'La volqueta M2 112 dobletroque es la propuesta de Freightliner para participar en todos los proyectos de infraestructura y construcción que se están llevando a cabo y que vendrán a corto plazo en el país. Su bastidor y suspensión reforzados, junto a su tren motriz de gran potencia, la convierten en la opción perfecta para el trabajo extremo.
';
$contenido[6] = 'Su motor Detroit MBE 4000, ofrece un torque máximo disponible desde muy bajas revoluciones. Esto se traduce en una menor velocidad de rotación del motor para condiciones de máxima fuerza, como por ejemplo arranques en pendientes pronunciadas con carga. Al trabajar a menores rotaciones en condición de máximo esfuerzo, se logra un menor desgaste del motor y de los demás componentes del tren motriz como el embrague, caja de cambios, lo que finalmente conlleva a menores costos en la operación.  Adicionalmente, el rango del torque inicia a las 1.000 rpm constante,  hasta 1.500 rpm, lo que permite contar con un margen amplio de revoluciones de motor, facilitando la operación para el conductor bajo condiciones de máximo esfuerzo y reduciendo el consumo de combustible.
';
$contenido[7] = 'Gracias a su diseño y construcción, el motor de 13 litros resulta ser muy liviano y confiable, su construcción sencilla y modular permite intervenciones rápidas, lo que se percibe en un menor tiempo de permanencia en el taller y por ende, un aumento en la disponibilidad del vehículo. 
';
$contenido[8] = 'Su cabina aerodinámica fabricada en aluminio, ofrece una estructura fuerte, liviana y resistente al óxido. Cuenta con suspensión de aire, paquete de aislamiento acústico y térmico. De igual manera, cuenta con un capó inclinado, en complemento con su parabrisas amplio,que permite mejor visibilidad desde el puesto del conductor y menores riesgos de accidentes, así como menos estrés para quien lo opera.
';
$contenido[9] = 'El bomper cromado seccionado en tres piezas de la M2 112, está diseñado para ahorrar tiempo y costos de mantenimiento, permitiendo hacer sólo reparaciones parciales si es necesario. Es decir, cuando se presenta una colisión, se cambia la sección afectada sin necesidad de cambiar todo el componente. Lo que significa menos tiempo y costos de reparación.
';
$contenido[10] = 'La M2 112, cuenta con dos tanques de 70 Galones cada uno, brindando al dueño una gran capacidad de almacenamiento, brindando de esta forma mayor autonomía y por ende menores detenciones para recargar combustible durante la operación.
';
$contenido[11] = 'Uno de los grandes atributos y diferenciales de esta referencia en el segmento de volquetas de gran capacidad, son sus Sistemas de Seguridad ATC Y ABS. El control automático de tracción (ATC) es un sistema que ayuda a mejorar la tracción cuando los vehículos están sobre superficies resbalosas, reduciendo la velocidad de giro de la rueda impulsora que está patinando, transmitiendo la fuerza a aquellas ruedas de los ejes de tracción que ofrecen mejor agarre. Por su parte, el Sistema Antibloqueo de Frenos ABS mejora el control del vehículo en maniobras de detención y ayuda a mantener el control del vehículo en superficies con poca tracción como piedras sueltas, arena, barro, zonas húmedas y tierra suelta.
';
$contenido[12] = '<strong>Características Volco Hardox 450</strong>
';
$contenido[13] = 'La M2 112 llega al mercado con uno de los volcos más resistentes para los trabajos extremos. 
';
$contenido[14] = '•	Fabricado con acero Hardox 450 de alta resistencia a la abrasión y al impacto.<br>
•	Ideal para grandes y exigentes operaciones de carga de materiales duros y abrasivos.<br>
•	Resistente al desgaste y a los ataques químicos severos, no absorbe la humedad. Ideal para transportistas que manejan carga de materiales con temperaturas superiores a los 120 grados, situación que no afecta el acero de la carrocería.<br>
•	Especial para aplicaciones en Minería Pesada, transporte de materiales duros y abrasivos, procesos de carga del volco a gran altura (impactos). <br>
•	La escalera con su barandilla facilita la subida para la inspección de la carga en el volco. Los escalones tienen un diseño Antideslizante.<br>
•	Volco tipo góndola con piso plano para evitar que se pegue el material, brindando un desprendimiento y deslizamiento de la carga más práctico para procesos de descarga más seguros y rápidos.';


/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));