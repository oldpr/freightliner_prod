<?php
require_once './app.php';
$classBody = 'producto';
/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$metaDescripcion = 'Un vehículo que ofrece diversas opciones de configuración, con el fin de garantizar y satisfacer las especificaciones para su negocio.';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/m2106-minimula.php';
$ogTitle = 'M2 106 minimula | Freightliner Colombia';
$ogDescription = 'Un vehículo que ofrece diversas opciones de configuración, con el fin de garantizar y satisfacer las especificaciones para su negocio.';
$ogImage = 'http://freightliner.com.co/tags/minimula.jpg';
$twitterTitle = 'M2 106 Minimula | Camiones Freightliner Colombia';
$twitterDescription = 'Un vehículo que ofrece diversas opciones de configuración, con el fin de garantizar y satisfacer las especificaciones para su negocio.';
$twitterImage = 'http://freightliner.com.co/tags/minimula.jpg';


$title = 'M2 106 Minimula | Camiones Freightliner Colombia';
/**** SECCIÓN BANNER PRINCIPAL ****/
$imgBanner = 'img/assets/productos/m2-106-minimula/1-Minimula-Billboard.jpg';
$prodcutoNombre = 'M2 106 MINIMULA ISL';
$productoPrecio = '278.400.000';
/**** SECCIÓN DESCRIPCIÓN ****/
$imgLogo = 'img/assets/productos/m2-106-minimula/Logo-m2-106.jpg';
$descTitulo = 'La minimula ideal para su trabajo';
$descTexto = 'El M2 106 4X2 Tracto Euro 5 ha sido concebido para el demandante trabajo en carretera. Su poderoso motor Cummins le da la vitalidad y el desempeño que se requiere en las exigentes rutas colombianas.';
/**** SECCION ESPECIFICACIONES ****/
// --ITEM 1
$imgItem1 = 'img/icon-eficiencia.jpg';
$tituloItem1 = 'MÁXIMA CAPACIDAD DE CARGA, MENOR PESO EN VACÍO';
$subtituloItem1 = 'Eficiencia';
// --ITEM 2
$imgItem2 = 'img/icon-seguridad.jpg';
$tituloItem2 = 'CABINA = VISIBILIDAD Y CONFORT';
$subtituloItem2 = 'Seguridad';
// --ITEM 3
$imgItem3 = 'img/icon-calidad.png';
$tituloItem3 = ' CABINA LIVIANA, LIBRE DE CORROSIÓN';
$subtituloItem3 = 'Calidad';
// --ITEM 4
$imgItem4 = '';
$tituloItem4 = '';
$subtituloItem4 = '';
// --LINKS
$linkFichaTecnica = 'pdf/m2106-minimula.pdf';
/**** SECCIÓN IMÁGENES ****/
$linkImg1 = 'img/assets/productos/m2-106-minimula/Minimula-photo-1.jpg';
$linkImg2 = 'img/assets/productos/m2-106-minimula/Minimula-photo-2.jpg';
$linkImg3 = 'img/assets/productos/m2-106-minimula/Minimula-photo-3.jpg';
/**** SECCIÓN DETALLES ****/
// --DETALLE 1
$imgDetalle1 = 'img/assets/productos/m2-106-minimula/Minimula-Trenmotriz-Photo.jpg';
$tituloDetalle1 = 'tren motriz';
$textoDetalle1 = 'El M2 106 4x2 tracto Euro 5 cuenta con una caja Eaton FRO14210C, de 10 marchas. Su amplio rango de relaciones permite contar con la fuerza para avanzar con carga en condiciones de pendiente extrema. Sus ejes Meritor le brindan la posibilidad de llegar hasta las 17 toneladas de PBV, lo que le permite contar con el máximo de capacidad de carga admisible en las carreteras colombianas. Su motor Cummins ISL de 8.9 Lt cuenta con un sistema de pos-tratamiento de gases de escape (SCR) que logra reducir las emisiones contaminantes, incluso más allá de lo requerido por las normas colombianas y sin sacrificar la eficiencia del motor, ni degradar el aceite de motor con cenizas y mayores temperaturas de trabajo. Esto significa un menor consumo de combustible, menores costos de mantenimiento preventivo y mayor disponibilidad del vehículo. <br>
Por otro lado, cuenta con un muy eficiente freno de motor Jacobs, cuya operación aumenta la seguridad en el trabajo del vehículo al momento de reducir y controlar la velocidad y sin tener que sobre exigir los frenos de servicio del camión.';
// --DETALLE 2
$imgDetalle2 = 'img/assets/productos/m2-106-minimula/Minimula-Productividad-photo.jpg';
$tituloDetalle2 = 'Máxima productividad del Conductor';
$textoDetalle2 = 'El M2 106 4x2 tracto ha sido creado para lograr que las largas jornadas de trabajo del operador sean más agradables, para ello el M2 106 cuenta con una cabina de suspensión neumática, vidrio panorámico de amplia visibilidad, tablero y controles ergonómicos, además de aire acondicionado de fábrica que facilitan la maniobrabilidad en carretera, reducen el agotamiento y aumentan la productividad del operador.';
// --DETALLE 3
$imgDetalle3 = 'img/assets/productos/m2-106-minimula/Minimula-desempeno-photo.jpg';
$tituloDetalle3 = 'Máximo desempeño del Camión';
$textoDetalle3 = 'El M2 106 4x2 Tracto es resistente,  concebido en una plataforma liviana, con bastidor reforzado y cabina fabricada en aluminio, logra maximizar la capacidad de carga dentro de los límites de peso establecidos en las normas colombianas, aumentando su eficiencia y productividad.';
/*************** FIN DEL CONTENIDO PARA EDITAR ******************/
echo $twig->render('producto.html.twig', array(
    'active' => 2,
    'classBody' => $classBody,
    'title' => $title,
    'imgBanner' => $imgBanner,
    'prodcutoNombre' => $prodcutoNombre,
    'productoPrecio' => $productoPrecio,
    'imgLogo' => $imgLogo,
    'descTitulo' => $descTitulo,
    'descTexto' => $descTexto,
    'imgItem1' => $imgItem1,
    'tituloItem1' => $tituloItem1,
    'subtituloItem1' => $subtituloItem1,
    'imgItem2' => $imgItem2,
    'tituloItem2' => $tituloItem2,
    'subtituloItem2' => $subtituloItem2,
    'imgItem3' => $imgItem3,
    'tituloItem3' => $tituloItem3,
    'subtituloItem3' => $subtituloItem3,
    'imgItem4' => $imgItem4,
    'tituloItem4' => $tituloItem4,
    'subtituloItem4' => $subtituloItem4,
    'linkFichaTecnica' => $linkFichaTecnica,
    'linkImg1' => $linkImg1,
    'linkImg2' => $linkImg2,
    'linkImg3' => $linkImg3,
    'imgDetalle1' => $imgDetalle1,
    'tituloDetalle1' => $tituloDetalle1,
    'textoDetalle1' => $textoDetalle1,
    'imgDetalle2' => $imgDetalle2,
    'tituloDetalle2' => $tituloDetalle2,
    'textoDetalle2' => $textoDetalle2,
    'imgDetalle3' => $imgDetalle3,
    'tituloDetalle3' => $tituloDetalle3,
    'textoDetalle3' => $textoDetalle3,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));
