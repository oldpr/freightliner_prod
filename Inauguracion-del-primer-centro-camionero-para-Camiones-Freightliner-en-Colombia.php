<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Primer centro camionero Freightliner en Colombia | Freightliner Colombia';
$metaDescripcion = 'Revive la inauguración del primer centro camionero Freightliner en Colombia';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/Inauguracion-del-primer-centro-camionero-para-Camiones-Freightliner-en-Colombia.php';
$ogTitle = 'Primer centro camionero Freightliner en Colombia | Freightliner Colombia';
$ogDescription = 'Revive la inauguración del primer centro camionero Freightliner en Colombia';
$ogImage = 'http://freightliner.com.co/tags/primer-centro-camionero.jpg';
$twitterTitle = 'Primer centro camionero Freightliner en Colombia | Freightliner Colombia';
$twitterDescription = 'Revive la inauguración del primer centro camionero Freightliner en Colombia';
$twitterImage = 'http://freightliner.com.co/tags/primer-centro-camionero.jpg';



/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '03.11. 2017';
$imagen = 'img/assets/detalle-noticias/3Noticia-photo.jpg';

$titulo = 'Inauguración del primer centro camionero para Camiones Freightliner en Colombia';

// --Contenido dividido por parrafos
$contenido[0] = 'El primer centro especializado para Camiones Freightliner se encuentra ubicado en la calle 80 a las afueras de la ciudad de Bogotá, inició su operación en enero de 2017, está construido en un terreno de 4.500m2 y contó con una inversión de aproximadamente de 7.500 millones de pesos.  
';
$contenido[1] = '“Con la apertura de esta nueva vitrina, estamos seguros que los clientes y amigos de las marcas Freightliner encontrarán en nuestras instalaciones, personal y servicio, de alta confiabilidad y respaldo” así lo afirmó Juan Felipe Villegas, Gerente en Motorysa. 
';
$contenido[2] = 'Esta nueva sede cuenta con el más completo portafolio de productos de la línea de camiones americanos Freightliner, atendiendo las necesidades de aquellos sectores tradicionales como el de la construcción, infraestructura, entre otros con las referencias FL M2 106, FL M2 112. De igual manera, para el sector de transporte de carga, Freightliner continúa ofreciendo productos en el segmento de Tractocamiones, como la  Columbia y Cascadia. 
';
$contenido[3] = 'Con un área de aproximadamente 4.500 m²,  el concesionario cuenta con servicio de venta y posventa con disponibilidad inmediata para la atención a éstos requerimientos. Dentro de las facilidades se destaca el servicio de taller con varias plazas de trabajo para servicio posventa, almacén de repuestos, vitrina de exhibición y venta de la marca Freightliner. 
';
$contenido[4] = 'El nuevo concesionario ubicado en la calle 80 costado sur, kilómetro 1 después del puente de Guaduas, inició su operación en enero de 2017, pero su lanzamiento oficial fue el jueves 23 de marzo. 
';

/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));