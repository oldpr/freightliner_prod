<?php
require_once './app.php';
$classBody = 'noticia';

$contenido = array();

/*************** CONTENIDO PARA EDITAR ******************/

/**** METATAGS ****/
$title = 'Si de poderío se trata, Freightliner lo demuestra con la M2 112. | Freightliner Colombia';
$metaDescripcion = 'Si de poderío se trata, Freightliner lo demuestra con la M2 112, la Volqueta diseñada para el trabajo extremo que se quedó con la mayor participación de mercado en el segmento de camiones americanos al cierre Feb de 2018. ';
$metaKeywords = 'camiones, Freightliner, volcos, volquetas, tractos, tractocamiones, Bogotá, Colombia, venta de camiones, vehículos comerciales, carga pesada, vehículos para construcción, vehículos mineros, vehículos terrenos difíciles';
$ogUrl = 'http://freightliner.com.co/Si-de-poderio-se%20trata-Freightliner-lo-demuestra-con-la-M2-112.php';
$ogTitle = 'Freightliner en el plan de infraestructura vial | Freightliner Colombia';
$ogDescription = 'Si de poderío se trata, Freightliner lo demuestra con la M2 112, la Volqueta diseñada para el trabajo extremo que se quedó con la mayor participación de mercado en el segmento de camiones americanos al cierre Feb de 2018. ';
$ogImage = 'http://freightliner.com.co/tags/alemautos.jpg';
$twitterTitle = 'Freightliner en el plan de infraestructura vial | Freightliner Colombia';
$twitterDescription = 'Si de poderío se trata, Freightliner lo demuestra con la M2 112, la Volqueta diseñada para el trabajo extremo que se quedó con la mayor participación de mercado en el segmento de camiones americanos al cierre Feb de 2018. ';
$twitterImage = 'http://freightliner.com.co/tags/alemautos.jpg';



/**** NOTICIA ****/
$categoria = 'Freightliner';
$fecha = '12.03. 2018';
$imagen = 'img/assets/detalle-noticias/represa.jpg';

$titulo = 'Si de poderío se trata, Freightliner lo demuestra con la M2 112, la Volqueta diseñada para el trabajo extremo que se quedó con la mayor participación de mercado en el segmento de camiones americanos al cierre Feb de 2018. ';

// --Contenido dividido por parrafos
$contenido[0] = 'Freightliner Colombia, consolida su participación de mercado en el segmento de Camiones Americanos de más de 17 Ton PBV, logrando un 44% SOM al cierre de Febrero de 2018. Hoy sabemos que solo la credibilidad y la confianza que han depositado nuestros clientes y socios de negocio en el portafolio de   Volcos M2 106  y M2 112 es lo  que nos permite seguir avanzando en la construcción de nuestro país.
';
$contenido[1] = 'Ya son más los clientes que eligieron  la resistencia y capacidad técnica de carga de nuestros Volcos M2 112, diseñados para trabajar en condiciones de operación con altas pendientes y sobre esfuerzos, es el caso de nuestras volquetas M2 112 volcó que  trabajan actualmente en la represa Hidrotuango, donde diariamente son puesta a prueba, y es que la M2 112 Volco, con su poderoso motor Detroit MBE 4000 EPA 98, entrega el mejor  rendimiento  en condiciones extremas de carga y terreno.  Sus ejes Meritor de gran capacidad de carga, su suspensión reforzada y sus robustos herrajes le permiten soportar las exigentes condiciones de carga a las que se somete. Sus ejes cuentan también con bloqueos de diferencial transversales y longitudinal, lo que le da al operador la posibilidad de contar con la máxima tracción disponible para tener la capacidad de maniobrar en vías en mal estado, reduciendo así  el riesgo de quedar bloqueado cuando el camino está  lleno de lodo. Para aumentar la eficiencia en la operación, esta volqueta cuenta con sistemas electrónicos ABS  (anti-bloqueo de frenos) y ATC (Control de tracción)  que le brindan al conductor las mejores condiciones de respuesta posibles a la volqueta en fases de detención o de fuerza en la operación. Por su gran equipamiento y su increíble respuesta para el trabajo extremo nuestra Volqueta M2 112 es la ideal para lograr el máximo desempeño y rentabilidad en obras de infraestructura y explotación de agregados del territorio nacional.

';
$contenido[2] = 'Freightliner, la fuerza líder norteamericana que es capaz de mover a todo el país. Somos #FreighlinerNegociosEnMovimiento. Somos #VolcosFreightliner #WorkSmart.

';




/*************** FIN DEL CONTENIDO PARA EDITAR ******************/

echo $twig->render('noticia.html.twig', array(
    'active' => 6,
    'classBody' => $classBody,
    'title' => $title,
    'titulo' => $titulo,
    'categoria' => $categoria,
    'fecha' => $fecha,
    'imagen' => $imagen,
    'contenido' => $contenido,
    'metaDescripcion'=> $metaDescripcion,
    'metaKeywords'=> $metaKeywords,
    'ogUrl'=> $ogUrl,
    'ogTitle'=> $ogTitle,
    'ogDescription'=> $ogDescription,
    'ogImage'=> $ogImage,
    'twitterTitle'=> $twitterTitle,
    'twitterDescription'=> $twitterDescription,
    'twitterImage'=> $twitterImage,
));